import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

refHQTheme(context) {
  return ThemeData(
   fontFamily: 'OpenSans',
      /*textTheme: GoogleFonts.solwayTextTheme(
        Theme.of(context).textTheme,
      ),*/
      /*primaryTextTheme: TextTheme(

      ),*/
    brightness: Brightness.dark,
    primarySwatch: Colors.grey,
    accentColor: Color(0xffb82419),
    canvasColor: answerScreenBackground,
    buttonColor: buttonBackground
  );
}

const Color correctAnswerColor = Color(0xff569f2b);
const Color answerScreenBackground = Color(0xff212121);
const Color buttonBackground = Color(0xff3f3f3f);
const Color lightButtonColor = Color(0xFF757575);
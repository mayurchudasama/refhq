import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ref_hq/data_models/ruleset.dart';
import 'package:ref_hq/data_models/sports.dart';
import 'package:ref_hq/data_models/user_subscription.dart';

class StateContainer extends StatefulWidget {
  final Widget child;
  final List<Sports> sports;
  final List<Ruleset> ruleSet;
  final UserSubscription userSubscription;
  final Map<String, dynamic> selSport;
  final Map<String, dynamic> selRuleset;
  final String subscriptionType;

  StateContainer({
    @required this.child,
    this.sports,
    this.ruleSet,
    this.userSubscription,
    this.selSport,
    this.selRuleset,
    this.subscriptionType
  });

  static StateContainerState of(BuildContext context) {
    /*return (context.inheritFromWidgetOfExactType(_InheritedStateContainer)
    as _InheritedStateContainer)
        .data;*/

    return context.dependOnInheritedWidgetOfExactType<_InheritedStateContainer>().data;
  }

  @override
  StateContainerState createState() => new StateContainerState();
}

class StateContainerState extends State<StateContainer> {
  List<Sports> sports;
  List<Ruleset> ruleSet;
  Map<String, dynamic> selSport;
  Map<String, dynamic> selRuleset;
  UserSubscription userSubscription;
  String subscriptionType;

  @override
  Widget build(BuildContext context) {
    return new _InheritedStateContainer(
      data: this,
      child: widget.child,
    );
  }

  void updateSubscriptionType(mSubscriptionType) {
    // setState(() {
      this.subscriptionType = mSubscriptionType;
    // });
  }

  void updateSelectedSport(selectedSport) {
    setState(() {
      this.selSport = selectedSport;
    });
  }

  void updateSelectedRuleset(selectedRuleset) {
    setState(() {
      this.selRuleset = selectedRuleset;
    });
  }

  void updateSportsInfo(sportsInfo) {
    setState(() {
      this.sports = sportsInfo;
    });
  }

  void updateRulesetInfo(rulesetInfo) {
    setState(() {
      this.ruleSet = rulesetInfo;
    });
  }

  void updateSubscription(rulesetInfo) {
    setState(() {
      this.ruleSet = rulesetInfo;
    });
  }
}

class _InheritedStateContainer extends InheritedWidget {
  final StateContainerState data;

  _InheritedStateContainer({
    Key key,
    @required this.data,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InheritedStateContainer old) => true;
}
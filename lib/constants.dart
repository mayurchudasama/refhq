const users = "users";
const pregame_folder = "pregame_folder";
const pregame_slides = "pregame_slides";
const rules_new = "rules_new";
const newsletter = "newsletter";
const key = "key";
const value = "value";
const firstName = "firstName";
const purchaseDetail = "purchaseDetail";
const purchasedData = "purchasedData";
const lastName = "lastName";
const homeTown = "homeTown";
const currentStreak = "currentStreak";
// const totalQuestionAttended = "totalQuestionAttended";
const attendedQuestions = "attendedQuestions";
const streak = "streak";
const rules = 'rules';
const rules_category = 'rules_category';
const title = 'title';
const documentLink = 'documentLink';
const userId = 'userId';
const questions = "questions";
const savedQuestions = 'savedQuestions';
const sports = 'sports';
const subscriptionType = 'subscriptionType';
const enable = 'enable';
const ruleset = 'ruleset';
const timestamp = 'timestamp';
const description = 'description';
const sport_ruleset_streak = 'sport_ruleset_streak';

//Question class
const answer1 = 'ANSWER 1';
const answer2 = 'ANSWER 2';
const answer3 = 'ANSWER 3';
const answer4 = 'ANSWER 4';
const correct = 'CORRECT';
const flag = 'FLAG';
const question = 'QUESTION';
const rule = 'rule';
const QID = 'QID';
const SPORT = 'SPORT';
const RULESET = 'RULESET';



const int CHOOSE_SPORT = -1;
const int CHOOSE_RULESET = -1;
const int REQUEST_ANOTHER_SPORT = -2;
const int REQUEST_ANOTHER_RULESET = -2;
const int COLLAPSE_SPORT = -3;
const int COLLAPSE_RULE_SET = -3;

const int VALUE_BASKETBALL = 0;
const int VALUE_NFHC_2020_21 = 2;
const List<String> RECIPIENTS = ["refhqsports@gmail.com"];

enum AnswerButton { One, Two, Three, Four }
enum ViewMode { Review, Quiz, Selected, ReviewFlagged }
enum SelectedAnswer { Answer1, Answer2, Answer3, Answer4 }
enum QuizType { All, Rulewise, Flagged }
enum FlagType { Flagged, UnFlagged, AttemptingFlag }

const String str_choose_sport = "Choose Sport";
const String str_collapse_sport = "collapse_sport";
const String str_request_another_sport = "(Request Another Sport)";
const String str_choose_rule_set = "Choose Rule Set";
const String str_collapse_rule_set = "collapse_rule_set";
const String str_request_another_rule_set = "(Request Another Rule Set)";

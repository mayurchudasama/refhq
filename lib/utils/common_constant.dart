import 'package:flutter/material.dart';

class CommonConstant {
  static const double borderDropdown = 1.0;
  static OutlineInputBorder signupTextfieldOutlineBorder = OutlineInputBorder(
    borderSide: BorderSide(
      width: 0,
      style: BorderStyle.none,
    ),
    gapPadding: 5,
    borderRadius: const BorderRadius.all(
      const Radius.circular(roundedCornerBorderRadius),
    ),
  );
  static const double roundedCornerBorderRadius = 10.0;
  static RoundedRectangleBorder roundedRectangleBorder = RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(roundedCornerBorderRadius),
      side: BorderSide(width: 0, style: BorderStyle.none));

  static Widget heightWidget = SizedBox(
    height: 12,
  );

  static EdgeInsetsGeometry textFieldContentPadding = EdgeInsets.symmetric(
  vertical: 4.0, horizontal: 12);
}

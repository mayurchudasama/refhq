import 'package:flutter/material.dart';

class MyColors{
  static Color colorConvert(String color) {
    color = color.replaceAll("#", "");
    if (color.length == 6) {
      return Color(int.parse("0xFF"+color));
    } else if (color.length == 8) {
      return Color(int.parse("0x"+color));
    }
  }

//  static Color get backgroundColor => MyColors.colorConvert("#ededed");
  static Color get backgroundColor => MyColors.colorConvert("#B1383D");
  static Color get white => Colors.white;
  static Color get borderColor => MyColors.colorConvert("#baaa9d");
  static Color get hintColor => MyColors.colorConvert("#c2c2c2");
  static Color get splashBackgroundColor => MyColors.colorConvert("#4B4C50");
  static Color get sponsorGradientColor3 => MyColors.colorConvert("#8F3036");
  static Color get sponsorGradientColor2 => MyColors.colorConvert("#A62D34");
  static Color get sponsorGradientColor1 => MyColors.colorConvert("#DC2128");
  static Color get sponsorButtonBackground => MyColors.colorConvert("#FDFEFF");
  static Color get sponsorButtonTextColor => MyColors.colorConvert("#2E328B");
  static Color get bottomNavigationBarColor => Colors.white;
  static Color get bottomNavigationBarBGColor => Colors.grey;
  static Color get btnGoColor => MyColors.colorConvert("#4C4F51");
  static Color get btnGoTextColor => MyColors.colorConvert("#A6A6A6");
  static Color get dropdownBackgroudColor => MyColors.colorConvert("#4C4F51");
  static Color get pregameButtonColor => MyColors.colorConvert('#383838');
  static Color get pregameSlidesCardButtonColor => MyColors.colorConvert('#707070');
  static Color get pregameNotesListButtonColor => MyColors.colorConvert('#383838');
  static Color get color1 => MyColors.colorConvert('#B02319');
  static Color get color2 => MyColors.colorConvert('#C2453F');
  static Color get color3 => MyColors.colorConvert('#D16D6B');
}
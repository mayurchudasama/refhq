import 'package:flutter/material.dart';
import 'package:ref_hq/utils/my_colors.dart';

class AppStyles{
  static TextStyle errorStyle = TextStyle(
    color: MyColors.backgroundColor
  );
  static OutlineInputBorder focusBorder = OutlineInputBorder(
    borderSide: BorderSide(color: MyColors.backgroundColor),
  );
  static TextStyle drawerMenuTextStyle = TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: MyColors.backgroundColor);
}
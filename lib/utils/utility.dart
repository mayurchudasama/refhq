import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:firebase_database/firebase_database.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:ref_hq/constants.dart';

class Utility {
  static List<String> kProductIdsIOS = <String>[
    'com.refhqsports.refhq.sub',
    'monthly_single_ruleset_3.99',
    'monthly_single_sport_5.99',
    'monthly_all_access_9.99',
  ];
  static List<String> kProductIdsAndroid = <String>[
    'monthly_3.99',
    'monthly_single_ruleset_3.99',
    'monthly_single_sport_5.99',
    'monthly_all_access_9.99',
  ];

  static List<String> productIds = Platform.isIOS?kProductIdsIOS:kProductIdsAndroid;

  static Map<String,dynamic> mapFromPurchaseDetail(PurchaseDetails purchaseDetails){
    Map<String, dynamic> mapPurchaseDetails = Map();
    mapPurchaseDetails['productID'] = purchaseDetails.productID;
    mapPurchaseDetails['purchaseID'] = purchaseDetails.purchaseID;
    mapPurchaseDetails['transactionDate'] = purchaseDetails.transactionDate;

    Map<String, dynamic> verificationData = Map();
    verificationData['serverVerificationData'] = purchaseDetails.verificationData.serverVerificationData;
    verificationData['localVerificationData'] = purchaseDetails.verificationData.localVerificationData;

    mapPurchaseDetails['verificationData'] = verificationData;

    debugPrint("mapPurchaseDetails: $mapPurchaseDetails");
    debugPrint("Nirav changes");
//    if (purchaseDetails.billingClientPurchase.originalJson != null) {
//      mapPurchaseDetails['billingClientPurchase'] = json.decode(purchaseDetails.billingClientPurchase.originalJson);
//    }
//    mapPurchaseDetails['billingClientPurchase'] = json.decode(purchaseDetails.billingClientPurchase.originalJson);

    return mapPurchaseDetails;
  }

  static void cacheAppData() {
    debugPrint("initializing cache..!!");
    FirebaseDatabase.instance.reference().child(prefix0.newsletter).once();
    FirebaseDatabase.instance.reference().child(prefix0.pregame_slides).once();
    FirebaseDatabase.instance.reference().child(users).once();
    FirebaseDatabase.instance.reference().child(prefix0.rules_new).once();
    debugPrint("cache initialized..!!");
  }
}

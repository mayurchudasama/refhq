import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter_mailer/flutter_mailer.dart';
import 'package:ref_hq/constants.dart';
import 'package:ref_hq/utils/my_toast.dart';

class EmailSender{

  static Future<void> requestAnotherRuleSet(selectedItemRuleSet) async {
    await send(subject: selectedItemRuleSet.name);
  }
  static Future<void> contactRefHQSupport() async {
    await send(subject: "Contact RefHQ Support");
  }
  static Future<void> requestAnotherSport(selectedItemSport) async {
    await send(subject: selectedItemSport.name);
  }
  static Future<void> requestAdditionalSportsOrRules() async {
    await send(subject: "Request Aditional Sports or Rules");
  }

  static Future<void> send({String subject}) async {
    if (Platform.isIOS) {
      final bool canSend = await FlutterMailer.canSendMail();
      if (!canSend) {
        MyToast.showToast('No email app available');
        return;
      }
    }

    // Platform messages may fail, so we use a try/catch PlatformException.
    final MailOptions mailOptions = MailOptions(
      body: '',
      subject: subject,
      recipients: RECIPIENTS,
      isHTML: true,
      // bccRecipients: ['other@example.com'],
      // ccRecipients: <String>['third@example.com'],
      // attachments: attachment,
    );

    String platformResponse;

    try {
      await FlutterMailer.send(mailOptions);
      platformResponse = 'success';
    } on PlatformException catch (error) {
      platformResponse = error.toString();
      print(error);
      MyToast.showToast(platformResponse);
    } catch (error) {
      platformResponse = error.toString();
      MyToast.showToast(platformResponse);
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
  }
}
enum FormType { login, register }

class EmailValidator {
  static String validate(String value) {
    return value.isEmpty ? "Email can't be empty" : null;
  }
}

class PasswordValidator {
  static String validate(String value) {
    return value.isEmpty ? "Password can't be empty" : null;
  }
}

class FirstNameValidator {
  static String validate(String value) {
    return value.isEmpty ? "First name can't be empty" : null;
  }
}

class LastNameValidator {
  static String validate(String value) {
    return value.isEmpty ? "Last name can't be empty" : null;
  }
}

class HometownValidator {
  static String validate(String value) {
    return value.isEmpty ? "Hometown can't be empty" : null;
  }
}
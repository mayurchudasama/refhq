class Ruleset {
  int value;
  String name;
  int sports;
  bool enable;

  Ruleset(this.value, this.name, this.sports,this.enable);

  static Map<String,dynamic> toJson(Ruleset item){
    return {
      "value":item.value,
      "name":item.name,
      "sports":item.sports,
      "enable":item.enable,
    };
  }

  Ruleset.fromMap(Map<String,dynamic> map):this.value = map['value'], this.name = map['name'], this.sports = map['sports'],this.enable = map['enable'];

  Ruleset.fromMapDynamic(Map<dynamic,dynamic> map):this.value = map['value'], this.name = map['name'], this.sports = map['sports'],this.enable = map['enable'];
}
import 'package:flutter/material.dart';

class Categories {
  int id;
  String title;

  Categories({this.title, this.id});

  factory Categories.fromJSON(Map<dynamic, dynamic> parsedJSON) {
    return Categories(id: parsedJSON['ID'],
        title: parsedJSON['Title']);
  }
}

class CategoryList {
  List<Categories> categoryList;

  CategoryList({this.categoryList});

  /*factory CategoryList.fromJSON(Map<dynamic, dynamic> json) {
    return CategoryList(
        categoryList: parseCategories(json)
    );
  }*/
  factory CategoryList.fromList(List<dynamic> json) {
    return CategoryList(
        categoryList: parseCategories(json)
    );
  }

  static List<Categories> parseCategories(List<dynamic> rulesJSON) {
    // var rList = rulesJSON[rules_new] as List;
    // var rList = rulesJSON['categories'] as List;
    debugPrint("category List : ${rulesJSON}");

    List<Categories> catList = rulesJSON.map((data) => Categories.fromJSON(data)).toList();
    return catList;
  }
}
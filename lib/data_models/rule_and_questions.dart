import 'package:ref_hq/constants.dart';
import 'package:ref_hq/constants.dart' as constants;

class Rules {
  dynamic questions;
  String title;

  Rules({this.title, this.questions});

  factory Rules.fromJSON(Map<dynamic, dynamic> parsedJSON) {
    return Rules(questions: parsedJSON[constants.questions],
        title: parsedJSON[constants.title]);
  }
}

class RulesList {
  List<Rules> rulesList;

  RulesList({this.rulesList});

  factory RulesList.fromJSON(Map<dynamic, dynamic> json) {
    return RulesList(
        rulesList: parseRules(json)
    );
  }

  static List<Rules> parseRules(rulesJSON) {
    // var rList = rulesJSON[rules_new] as List;
    var rList = rulesJSON[rules_category] as List;
    List<Rules> ruleList = rList.map((data) => Rules.fromJSON(data)).toList();
    return ruleList;
  }
}

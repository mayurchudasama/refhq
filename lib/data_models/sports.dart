class Sports {
  int value;
  String name;
  bool enable;

  Sports(this.value, this.name,this.enable);

  static Map<String,dynamic> toJson(Sports item){
    return {
      "value":item.value,
      "name":item.name,
      "enable":item.enable
    };
  }
  Sports.fromMap(Map<dynamic,dynamic> map):this.value = map['value'], this.name = map['name'], this.enable = map['enable'];

  Sports.fromMapDynamic(Map<dynamic,dynamic> map):this.value = map['value'], this.name = map['name'], this.enable = map['enable'];
}
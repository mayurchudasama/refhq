import 'package:ref_hq/constants.dart' as constants;

class Question {
  String answer1;
  String answer2;
  String answer3;
  String answer4;
  int correct;
  int qid;
  int sport;
  int ruleset;
  int flag;
  String question;
  String rule;
  constants.ViewMode viewMode = constants.ViewMode.Quiz;
  constants.SelectedAnswer selectedAnswer;
  String selectedAnswerString;
  List<String> answersList = List();
  bool wasPreviousQuestionCorrect = false;
  constants.FlagType flagType = constants.FlagType.UnFlagged;
  String key;

  Question(
      {this.answer1,
      this.answer2,
      this.answer3,
      this.answer4,
      this.correct,
      this.flag,
      this.question,
        this.rule,
        this.qid,
        this.sport,
        this.ruleset,
        this.key});

  factory Question.fromJSON(Map<String, dynamic> parsedJSON) {
    return Question(
      answer1: parsedJSON[constants.answer1].toString(),
      answer2: parsedJSON[constants.answer2].toString(),
      answer3: parsedJSON[constants.answer3].toString(),
      answer4: parsedJSON[constants.answer4].toString(),
      correct: parsedJSON[constants.correct],
      flag: parsedJSON[constants.flag],
      question: parsedJSON[constants.question],
      rule: parsedJSON[constants.rule],
      qid: parsedJSON[constants.QID],
      sport: parsedJSON[constants.SPORT],
      ruleset: parsedJSON[constants.RULESET],
    );
  }
  factory Question.fromDynamicJSON(Map<dynamic, dynamic> parsedJSON) {
    return Question(
      answer1: parsedJSON[constants.answer1].toString(),
      answer2: parsedJSON[constants.answer2].toString(),
      answer3: parsedJSON[constants.answer3].toString(),
      answer4: parsedJSON[constants.answer4].toString(),
      correct: parsedJSON[constants.correct],
      flag: parsedJSON[constants.flag],
      question: parsedJSON[constants.question],
      rule: parsedJSON[constants.rule],
      qid: parsedJSON[constants.QID],
      sport: parsedJSON[constants.SPORT],
      ruleset: parsedJSON[constants.RULESET],
    );
  }

  factory Question.flaggedFromJSON(String key,
      Map<String, dynamic> parsedJSON) {
    return Question(
      answer1: parsedJSON[constants.answer1].toString(),
      answer2: parsedJSON[constants.answer2].toString(),
      answer3: parsedJSON[constants.answer3].toString(),
      answer4: parsedJSON[constants.answer4].toString(),
      correct: parsedJSON[constants.correct],
      flag: parsedJSON[constants.flag] is String?int.parse(parsedJSON[constants.flag]):parsedJSON[constants.flag],
      question: parsedJSON[constants.question],
      rule: parsedJSON[constants.rule],
      qid: parsedJSON[constants.QID],
      sport: parsedJSON[constants.SPORT],
      ruleset: parsedJSON[constants.RULESET],
      key: key,
    );
  }

  ///To be used only when Flagging question
  toJson() {
    return {
      constants.answer1: answer1,
      constants.answer2: answer2,
      constants.answer3: answer3,
      constants.answer4: answer4,
      constants.correct: correct,
      //The original question format stores this as int.
      constants.flag: flag,
      constants.question: question,
      constants.rule: rule,
      constants.QID: qid,
      constants.SPORT: sport,
      constants.RULESET: ruleset,
    };
  }
}

class QuestionList {
  List<Question> questionList;

  QuestionList({this.questionList});

  factory QuestionList.fromJSON(List<dynamic> json) {
    return QuestionList(questionList: parseQuestions(json));
  }

  static List<Question> parseQuestions(List<dynamic> list) {
    List<Question> questionList = list
        .map((data) => Question.fromJSON(Map<String, dynamic>.from(data)))
        .toList();
    return questionList;
  }
}

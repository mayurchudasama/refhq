import 'dart:convert';

import 'package:ref_hq/data_models/ruleset.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:ref_hq/data_models/sports.dart';
import 'package:ref_hq/data_models/subscription_type.dart';
import 'package:ref_hq/utils/utility.dart';

class UserSubscription{
  SubscriptionType subscriptionType;
  Sports sports;
  Ruleset ruleset;
  UserSubscription({this.subscriptionType,this.sports,this.ruleset});


  factory UserSubscription.fromJson(Map<String,dynamic> mapUserSubscription){
    SubscriptionType sType = getSubscriptionTypeEnum(mapUserSubscription[prefix0.subscriptionType]);
    Sports sports = Sports.fromMap(mapUserSubscription[prefix0.sports]);
    Ruleset ruleset = Ruleset.fromMap(mapUserSubscription[prefix0.ruleset]);

    return UserSubscription(subscriptionType: sType,sports: sports,ruleset: ruleset);

  }
  static Map<String,dynamic> toJson(UserSubscription userSubscription){
    Map<String,dynamic> mapSubscription=Map();
    mapSubscription[prefix0.subscriptionType] = getSubscriptionTypeString(userSubscription.subscriptionType);
    mapSubscription[prefix0.sports] = json.encode(Sports.toJson(userSubscription.sports));
    mapSubscription[prefix0.ruleset] = json.encode(Ruleset.toJson(userSubscription.ruleset));
    return mapSubscription;
  }
  static String getSubscriptionTypeString(SubscriptionType subscriptionType){
    switch(subscriptionType){
      case SubscriptionType.NONE:
        return '';
      case SubscriptionType.SINGLE_RULESET:
        return Utility.productIds[1];
      case SubscriptionType.SINGLE_SPORT:
        return Utility.productIds[2];
      case SubscriptionType.ALL_ACCESS:
        return Utility.productIds[3];
    }
    return '';
  }
  static SubscriptionType getSubscriptionTypeEnum(String mSubType){

    if(mSubType == Utility.productIds[0]){
      return SubscriptionType.ALL_ACCESS;
    } else if(mSubType == Utility.productIds[1]){
      return SubscriptionType.SINGLE_RULESET;
    } else if(mSubType == Utility.productIds[2]){
      return SubscriptionType.SINGLE_SPORT;
    } else if(mSubType == Utility.productIds[3]){
      return SubscriptionType.ALL_ACCESS;
    }
   /* int mIntType = int.parse(mSubType);
    if(mIntType == 0){
      return SubscriptionType.NONE;
    }else if(mIntType == 1){
      return SubscriptionType.SINGLE_RULESET;
    }else if(mIntType == 2){
      return SubscriptionType.SINGLE_SPORT;
    }else if(mIntType == 3){
      return SubscriptionType.ALL_ACCESS;
    }*/
    return SubscriptionType.NONE;
  }
}
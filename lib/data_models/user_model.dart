
import 'package:ref_hq/constants.dart' as prefix0;
class UserModel {
  String key;
  String currentStreak;
  String firstName;
  String lastName;
  int streak;
  String homeTown;
  dynamic savedQuestions;
  dynamic sports;
  dynamic ruleset;
  dynamic sport_ruleset_streak;
  String subscriptionType;


  UserModel(
      {this.currentStreak,
      this.firstName,
      this.lastName,
      this.streak,
      this.homeTown,
      this.savedQuestions,
      this.sports,
      this.ruleset,
      this.subscriptionType,
      this.sport_ruleset_streak});

  UserModel.fromSnapshot(String key, Map user)
      : key = key,
        currentStreak = user['currentStreak'],
        firstName = user['firstName'],
        lastName = user['lastName'],
        homeTown = user['homeTown'],
        streak = (user["streak"] is String)
            ? int.parse(user['streak'])
            : user["streak"],
        savedQuestions = user['savedQuestions'],
        sports = user[prefix0.sports],
        ruleset = user[prefix0.ruleset],
        subscriptionType = user[prefix0.subscriptionType],
        sport_ruleset_streak = user[prefix0.sport_ruleset_streak] ;

  toJson() {
    return {
      'currentStreak': currentStreak,
      'firstName': firstName,
      'lastName': lastName,
      'homeTown': homeTown,
      'savedQuestions': savedQuestions,
      'streak': streak.toString(),
      prefix0.sports: sports.toString(),
      prefix0.ruleset: ruleset.toString(),
      prefix0.subscriptionType:subscriptionType,
      prefix0.sport_ruleset_streak:sport_ruleset_streak
    };
  }
}

import 'dart:math' as math;

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class AudioPlayerScreen extends StatefulWidget {
  final String rule;

  AudioPlayerScreen({Key key, @required this.rule}) : super(key: key);

  @override
  _AudioPlayerState createState() => _AudioPlayerState();
}

class _AudioPlayerState extends State<AudioPlayerScreen> {
  AudioPlayer advancedPlayer;
  AudioCache cache;
  Icon playOrPause;

  Duration _duration = new Duration();
  Duration _position = new Duration();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.rule),
        ),
        body: Column(
          children: <Widget>[
            Flexible(
              child: FractionallySizedBox(
                heightFactor: 0.4,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                      child: Text(
                        '10',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.bold),
                      ),
                    ),
                    IconButton(
                      padding: EdgeInsets.all(0),
                      icon: Icon(
                        Icons.replay,
                        size: 48,
                      ),
                      onPressed: () {
                        setState(() {
                          int replayTo = _position.inSeconds.toInt() - 10;
                          if (replayTo < 0) {
                            seekToSecond(0);
                          } else {
                            seekToSecond(replayTo);
                          }
                        });
                      },
                    ),
                  ],
                ),
                Flexible(
                    child: FractionallySizedBox(
                      widthFactor: 0.3,
                    )),
                IconButton(
                  //Play / Pause Icon.
                  padding: EdgeInsets.all(0),
                  alignment: Alignment.center,
                  icon: Center(child: setPlayPauseIcon()),
                  onPressed: () {
                    playPauseAction();
                  },
                ),
                Flexible(
                    child: FractionallySizedBox(
                      widthFactor: 0.3,
                    )),
                Stack(
                  //Forward Button Stack
                  alignment: Alignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                      child: Text(
                        '15',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.bold),
                      ),
                    ),
                    IconButton(
                      padding: EdgeInsets.all(0),
                      icon: Transform(
                        alignment: Alignment.center,
                        transform: Matrix4.rotationY(math.pi),
                        child: Icon(
                          Icons.replay,
                          size: 48,
                        ),
                      ),
                      onPressed: () {
                        setState(() {
                          int fastForwardTo = _position.inSeconds.toInt() + 15;
                          if (fastForwardTo > _duration.inSeconds) {
                            seekToSecond(_duration.inSeconds);
                          } else {
                            seekToSecond(fastForwardTo);
                          }
                        });
                      },
                    ),
                  ],
                )
              ],
            ),
            Slider(
              activeColor: Colors.white,
              value: _position.inSeconds.toDouble(),
              min: 0.0,
              max: _duration.inSeconds.toDouble(),
              onChanged: (double value) {
                setState(() {
                  seekToSecond(value.toInt());
                  value = value;
                });
              },
            ),
            ConstrainedBox(
              constraints: BoxConstraints(
                  minWidth: MediaQuery
                      .of(context)
                      .size
                      .width, maxHeight: 16),
              child: Stack(
                alignment: Alignment.topCenter,
                children: <Widget>[
                  Positioned(
                    left: 24,
                    child: StreamBuilder(
                        stream: advancedPlayer.onAudioPositionChanged,
                        builder: (context, snapshot) {
                          if (snapshot.data == null) return Text('00:00');
                          {
                            Duration duration = snapshot.data;
                            var label =
                                '${duration.inMinutes.toString().padLeft(
                                2, '0')}:${snapshot.data.inSeconds.remainder(60)
                                .toString()
                                .padLeft(2, '0')}';
                            return Text(label.toString());
                          }
                        }),
                  ),
                  Positioned(
                    right: 24,
                    child: StreamBuilder(
                        stream: advancedPlayer.onDurationChanged,
                        builder: (context, snapshot) {
                          if (snapshot.data == null) return Text('00:00');
                          {
                            Duration duration = snapshot.data;
                            var label =
                                '${duration.inMinutes.toString().padLeft(
                                2, '0')}:${duration.inSeconds.remainder(60)
                                .toString()
                                .padLeft(2, '0')}';
                            return Text(label.toString());
                          }
                        }),
                  ),
                ],
              ),
            )
          ],
        ));
  }

  @override
  void initState() {
    super.initState();
    initPlayer();
    setStreamListeners();
  }

  play() async {
    cache.play('audios/${widget.rule}.mp3');
  }

  void initPlayer() {
    advancedPlayer = AudioPlayer();
    cache = AudioCache(fixedPlayer: advancedPlayer);
    cache.load('audios/${widget.rule}.mp3');
  }

  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);
    advancedPlayer.seek(newDuration);
  }

  @override
  void dispose() {
    advancedPlayer.stop();
    advancedPlayer.release();
    super.dispose();
  }

  setPlayPauseIcon() {
    var state = advancedPlayer.state;
    if (state == AudioPlayerState.PLAYING) {
      setState(() {
        playOrPause = Icon(Icons.pause_circle_outline, size: 48);
      });
    } else {
      playOrPause = Icon(Icons.play_circle_outline, size: 48);
    }

    return playOrPause;
  }

  void playPauseAction() {
    AudioPlayerState state = advancedPlayer.state;
    switch (state) {
      case AudioPlayerState.STOPPED:
        play();
        break;
      case AudioPlayerState.PLAYING:
        advancedPlayer.pause();
        setState(() {});
        break;
      case AudioPlayerState.PAUSED:
        advancedPlayer.resume();
        break;
      case AudioPlayerState.COMPLETED:
        setState(() {
          play();
        });
        break;
      default:
        setState(() {
          play();
        });
    }
  }

  void setStreamListeners() {
    advancedPlayer.onAudioPositionChanged.listen((onData) {
      _position = onData;
      if (_position != null) {
        setState(() {});
      }
    }, onError: (error) {
      print(error);
    }, cancelOnError: false);

    advancedPlayer.onDurationChanged.listen((onData) {
      _duration = onData;
    }, onError: (error) {
      print(error);
    }, cancelOnError: false);
  }
}

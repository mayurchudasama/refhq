import 'package:flutter/material.dart';

bottomLogo() {
  return Padding(
    padding: const EdgeInsets.fromLTRB(48, 0, 48, 0),
    child: Image.asset('assets/images/logo.png'),
  );
}

void displaySnackBar(String s, BuildContext context) {
  final snackBar = SnackBar(
    content: Text(
      s,
      style: TextStyle(color: Colors.white),
    ),
    backgroundColor: Theme
        .of(context)
        .primaryColor,
  );

  Scaffold.of(context).showSnackBar(snackBar);
}

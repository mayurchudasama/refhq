import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:ref_hq/audio_player.dart';

class AudioRulebook extends StatefulWidget {
  @override
  _AudioRulebookState createState() => _AudioRulebookState();
}

class _AudioRulebookState extends State<AudioRulebook> {
  AudioPlayer advancedPlayer;
  AudioCache cache;

  final List<String> audioRuleList = [
    'RULE 1 - Court and Equipment',
    'RULE 2 - Officials and Their Duties',
    'RULE 3 - Players and Substitutes',
    'RULE 4 - Definitions',
    'RULE 5 - Scoring and Timing',
    'RULE 6 - Live Ball and Dead Ball',
    'RULE 7 - Out of Bounds and the Throw-In',
    'RULE 8 - Free Throw',
    'RULE 9 - Violations and Penalties',
    'RULE 10 - Fouls and Penalties',
    'RULE 11 - Instant Replay',
  ];

  Duration _duration = new Duration();
  Duration _position = new Duration();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('2019-20 Audio Rulebook'),
        ),
        body: ListView.separated(
            itemCount: audioRuleList.length,
            itemBuilder: (BuildContext context, int index) {
              return FlatButton(
                padding: EdgeInsets.all(0),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) =>
                          AudioPlayerScreen(rule: audioRuleList[index])));
                },
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                      minWidth: MediaQuery
                          .of(context)
                          .size
                          .width),
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text('${audioRuleList[index]}'),
                      ),
                      Positioned(
                        right: 0,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Icon(Icons.play_arrow),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          separatorBuilder: (context, index) => Divider(),
        ));
  }
}

class API{
//  http://work-updates.com/salon/public/api/get-categories
  /*static const String BASE = "https://reqres.in/";
  static const String LOGIN_USER = BASE+"api/login";*/

  static const String BASE = "http://work-updates.com/ihc/public/";
  static const String GET_CATEGORIES = BASE+"api/get-categories";
  static const String GET_TOP = BASE+"api/get-categories/top";
  static const String GET_CATEGORY_LIST = BASE+"api/v1/category-list";
  static const String GET_DEGREE_LIST = BASE+"api/v1/degree-list";
  static const String GET_SERVICE_LIST = BASE+"api/v1/service-list";
  static const String GET_BANNER = BASE+"api/get-categories/future";
  static const String GET_DOCTORS = BASE+"api/get-doctors/{type}/{type_id}";
  static const String GET_PATIENTS = BASE+"api/doctor-patient-list/{doctor_id}";
  static const String GET_CONVERSATION = BASE+"api/get-doctors";
  static const String GET_MY_DOCTORS = BASE+"api/my-doctors/{user_id}";
  static const String GET_MY_APPOINTMENTS = BASE+"api/my-appointments/{user_id}";
  static const String GET_SINGLE_DOCTORS = BASE+"api/single-doctors/{id_doctors}";
  static const String POST_VALIDATE_DOCTOR = BASE+"api/v1/doctor_validate";
  static const String POST_UPCOMING_APPOINTMENT_DOCTOR = BASE+"api/v1/upcoming-appointme-doctor";
  static const String POST_DOCTORS_NEAR_YOU = BASE+"api/get-doctors-by-distance";
  static const String POST_DOCTOR_CREATE = BASE+"api/v1/doctor_create";
  static const String POST_DOCTOR_UPDATE = BASE+"api/v1/doctor-update";
  static const String verifyPurchase = "https://androidpublisher.googleapis.com/androidpublisher/v3/applications/{packageName}/purchases/subscriptions/{subscriptionId}/tokens/{token}";

/*

  http://work-updates.com/ihc/public/api/get-doctors/category/7


  http://work-updates.com/ihc/public/api/get-categories


  GET_TOP : http://work-updates.com/ihc/public/api/get-categories/top

  GET_FUTURE : http://work-updates.com/ihc/public/api/get-categories/future
*/


/*  this is the final api for home page

  top category mate apde top vadi user karsu
  banner mate future vadi

  for the category*/

}

import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:ref_hq/api/api.dart';

class ApiRequest {
  static Future<Map<String, dynamic>> fetchCategories() async {
    var client = http.Client();
    String url = API.verifyPurchase;
    // {packageName}/purchases/subscriptions/{subscriptionId}/tokens/{token}
    url = url.replaceAll('{packageName}', 'com.refhq.app');
    url = url.replaceAll('{subscriptionId}', 'monthly_single_ruleset_3.99');
    url = url.replaceAll('{token}', 'iklikhkkcboklhedjhmiekil.AO-J1OyIhQYJ3x3tkW7i2Wn1Of0h58afnomn_rVu7y9yDyBhtprqASXF-vH3RMfMPfNQ0J0gwEPHDpUuGp7Xy0ypJWxd6QqMWHcG4_RL8ojtIo05L3uNmq_d8U1_GJwdhRPG3TnKvb48');
    final response = await client.get(url);
    debugPrint("App-API : verifyPurchase : ${url}");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }
/*
  static Future<Map<String, dynamic>> getPatientList({String doctorId}) async {
    String api_str = API.GET_PATIENTS.replaceAll('{doctor_id}', '5'*//*doctorId*//*);
    debugPrint("App-API : GET_PATIENTS : ${api_str}");
    var client = http.Client();
    final response = await client.get(api_str);

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }

  static Future<Map<String, dynamic>> getDoctors({String type, int type_id}) async {
    String api_str = API.GET_DOCTORS.replaceAll('{type}', type);
    api_str = api_str.replaceAll('{type_id}', "${type_id.toString()}");
    debugPrint("App-API : GET_DOCTORS : ${api_str}");
    var client = http.Client();
    final response = await client.get(api_str);

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }

  static Future<Map<String, dynamic>> getConversation() async {
    String api_str = API.GET_CONVERSATION;
    debugPrint("App-API : GET_CONVERSATION : ${api_str}");
    var client = http.Client();
    final response = await client.get(api_str);

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }

  static Future<Map<String, dynamic>> getMyDoctors({String userID}) async {
    String api_str = API.GET_MY_DOCTORS.replaceAll('{user_id}', userID);
    debugPrint("App-API : GET_MY_DOCTORS : ${api_str}");
    var client = http.Client();
    final response = await client.get(api_str);

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get my doctors');
    }
  }

  static Future<Map<String, dynamic>> getMyAppointments({String userID}) async {
    String api_str = API.GET_MY_APPOINTMENTS.replaceAll('{user_id}', userID);
    debugPrint("App-API : GET_MY_APPOINTMENTS : ${api_str}");
    var client = http.Client();
    final response = await client.get(api_str);

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get my appointments');
    }
  }

  static Future<Map<String, dynamic>> getSingleDoctors({String id_doctors}) async {
    String api_str = API.GET_SINGLE_DOCTORS.replaceAll('{id_doctors}', id_doctors);
    debugPrint("App-API : GET_SINGLE_DOCTORS : ${api_str}");
    var client = http.Client();
    final response = await client.get(api_str);

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get single doctors');
    }
  }


  static Future<Map<String, dynamic>> postDoctorsNearYou({String lat, String lng, String radius, String limit, String page}) async {
    String api_str = API.POST_DOCTORS_NEAR_YOU;
    debugPrint("App-API : POST_DOCTORS_NEAR_YOU : ${api_str}");

    Map<String, String> params = Map();
    params['lat'] = lat;
    params['lng'] = lng;
    params['radius'] = radius;
    params['limit'] = limit;
    params['page'] = page;

    print("API postDoctorsNearYou Params : ${params.toString()}");
//    FormData
    try {
      var client = http.Client();
      final response = await client.post(api_str, headers: <String, String>{"Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"}, body: json.encode(params));
      if (response.statusCode == 202 || response.statusCode == 201) {
        Map<String, dynamic> mMap = jsonDecode(response.body.toString());
        debugPrint("Result from API POST_DOCTORS_NEAR_YOU : ${mMap}");
//      Map<dynamic,dynamic> mapResult = Map<String, dynamic>.from(json.decode(response.body));
        return mMap;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to validate customer');
      }
    } on DioError catch(e){
      throw Exception(e.response);
    }
    catch (e) {
      throw Exception(e.toString());
    }
  }

//  static String mID= "987645551";
  static Future<Map<String,dynamic>> postValidateDoctor({String id_firebase}) async {
    String api_str = API.POST_VALIDATE_DOCTOR;
    debugPrint("App-API : POST_VALIDATE_DOCTOR : ${api_str}");
    Map<String, String> params = Map();
    params['id_firebase'] = *//*mID*//*id_firebase;
//    FormData
    print("API postValidateDoctor params : ${params.toString()}");

    var client = http.Client();
    final response = await client.post(api_str, headers: <String, String>{"Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"}, body: jsonEncode(params));
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);
      print("result : ${map}");

      return Future.value(map);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to validate customer');
    }
  }
  static Future<Map<String, dynamic>> postDoctorCreate({
      @required String fullname,
      String email,
      String password,
      String id_firebase,
      String mobile,
      String state,
      String city,
      String address,
      String zipcode,
      File image,
      List<String> category,
    List<String> degree,
    List<String> services,
    String fees,
    String description,
    String lat,
    String lng,
    }) async {
    debugPrint("5.1");
    String api_str = API.POST_DOCTOR_CREATE;
    debugPrint("App-API : POST_DOCTOR_CREATE : ${api_str}");
    debugPrint("5.2");
    String strCat = category.join(',');
    String strService = services.join(',');
    String strDegree = degree.join(',');


    Map<String,dynamic> params = {
      "fullname": fullname,
      "email": email,
      "mobile": mobile,
      "category":strCat,
      "degree":strDegree,
      "services":strService,
      "fees":fees,
      "id_firebase": *//*mID*//*id_firebase,
      "state": state,
      "city": city,
      "address": address,
      "zipcode": zipcode,
      "lat":lat,
      "lng":lng,
      "base_image": image!=null? await MultipartFile.fromFile(image.path,filename: basename(image.path) ):null,
      // "cover_image": image!=null? await MultipartFile.fromFile(image.path,filename: basename(image.path) ):null,
      "description": description,
    };
    debugPrint("App-API : POST_DOCTOR_CREATE : Params : ${params}");
    FormData formData = FormData.fromMap(params);

//    FormData
    try {
      debugPrint("1");
    Response response = await Dio().post(api_str, data: formData);
      debugPrint("2");
      debugPrint("dio only response : ${response}");
      debugPrint("dio toString response : ${response.toString()}");
      debugPrint("dio response : ${response.data}");
      debugPrint("dio status code : ${response.statusCode}");
      if (response.statusCode == 201) {
        final jsonData = json.decode(response.toString());
        var map=Map<String, dynamic>.from(jsonData);
        return map;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to validate customer');
      }
    } on DioError catch(e){
        throw Exception(e.response);
    }catch (e) {
      debugPrint("3");
      throw Exception("Mayur ${e.toString()}");
    }
  }

  static Future<Map<String, dynamic>> postDoctorUpdate({
    @required String id,
    @required String fullname,
    String email,
    String mobile,
    List<String> category,
    List<String> degree,
    List<String> service,
    String state,
    String city,
    String address,
    String zipcode,
    File image,
    File cover_image,
    String fees,
    String description,
    String lat,
    String lng
  }) async {
    String api_str = API.POST_DOCTOR_UPDATE;
    debugPrint("API-App POST_DOCTOR_UPDATE : ${api_str}");

    String strCat = category.join(',');
    String strService = service.join(',');
    String strDegree = degree.join(',');
    Map<String,dynamic> mapFormData = Map();
    mapFormData['id'] = *//*mID*//*id;
    mapFormData['fullname'] = fullname;
    mapFormData['email'] = email;
    mapFormData['mobile'] = mobile;
    mapFormData['category'] = strCat;
    mapFormData['degree'] = strDegree;
    mapFormData['services'] = strService;
    mapFormData['state'] = state;
    mapFormData['city'] = city;
    mapFormData['address'] = address;
    mapFormData['zipcode'] = zipcode;
    mapFormData['fees'] = fees;
    mapFormData["lat"] = lat;
    mapFormData["lng"] = lng;
    mapFormData["description"] = description;
    mapFormData['base_image'] = image!=null? await MultipartFile.fromFile(image.path,filename: basename(image.path) ):null;
    mapFormData['cover_image'] = cover_image!=null? await MultipartFile.fromFile(cover_image.path,filename: basename(cover_image.path) ):null;
    debugPrint("API-App POST_DOCTOR_UPDATE : PARAMS : ${mapFormData}");

    FormData formData = FormData.fromMap(mapFormData);

//    FormData
    try {
      Response response = await Dio().post(api_str, data: formData);
      debugPrint("dio only response : ${response}");
      debugPrint("dio status code : ${response.statusCode}");
      if (response.statusCode == 201) {
        final jsonData = json.decode(response.toString());
        var map=Map<String, dynamic>.from(jsonData);
        return map;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to validate customer');
      }
    } on DioError catch(e){
      throw Exception(e.response);
    }catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<Map<String, dynamic>> getTop() async {
    var client = http.Client();
    final response = await client.get(API.GET_TOP);
    debugPrint("API-App GET_TOP : ${API.GET_TOP}");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }

  static Future<Map<String, dynamic>> getCategoryList() async {
    var client = http.Client();
    final response = await client.get(API.GET_CATEGORY_LIST);
    debugPrint("API-App GET_CATEGORY_LIST : ${API.GET_CATEGORY_LIST}");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }
  static Future<Map<String, dynamic>> getDegreeList() async {
    var client = http.Client();
    final response = await client.get(API.GET_DEGREE_LIST);
    debugPrint("API-App GET_DEGREE_LIST : ${API.GET_DEGREE_LIST}");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }
  static Future<Map<String, dynamic>> getServiceList() async {
    var client = http.Client();
    final response = await client.get(API.GET_SERVICE_LIST);
    debugPrint("API-App GET_SERVICE_LIST : ${API.GET_SERVICE_LIST}");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }
  static Future<Map<String, dynamic>> getBanner() async {
    var client = http.Client();
    final response = await client.get(API.GET_BANNER);
    debugPrint("API-App GET_BANNER : ${API.GET_BANNER}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }

  static Future<Map<String,dynamic>> postUpcomingAppointmentDoctor({String date_time,String doctor_id}) async {
    String api_str = API.POST_UPCOMING_APPOINTMENT_DOCTOR;
    debugPrint("App-API : POST_UPCOMING_APPOINTMENT : ${api_str}");
    Map<String, String> params = Map();
    params['date_time'] = date_time;
    params['doctor_id'] = doctor_id;
//    FormData
    print("API POST_UPCOMING_APPOINTMENT params : ${params.toString()}");

    var client = http.Client();
    final response = await client.post(api_str, headers: <String, String>{"Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"}, body: jsonEncode(params));
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);
      print("result : ${map}");

      return Future.value(map);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to validate customer');
    }
  }*/
}

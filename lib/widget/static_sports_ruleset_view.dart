import 'package:flutter/material.dart';
import 'package:ref_hq/utils/common_constant.dart';
import 'package:ref_hq/utils/my_colors.dart';

class StaticSportsRuleSetView extends StatelessWidget {
  final Map<dynamic, dynamic> mapSportOrRuleset;
  double height;
  double fontSize;
  StaticSportsRuleSetView({this.mapSportOrRuleset,this.height,this.fontSize});
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      width: 300,
      padding: EdgeInsets.only(left: 10, right: 10),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(
              width: CommonConstant.borderDropdown, color: Colors.black),
          borderRadius: BorderRadius.circular(10),
          color: MyColors.dropdownBackgroudColor),
      height: this.height,
      child: Container(
        // color: Colors.grey,
        child: Text(
          this.mapSportOrRuleset['name'],
          style: TextStyle(
              color: Colors.white,
              fontSize: this.fontSize /*, decoration: TextDecoration.underline*/),
        ),
      ),
    );
  }
}

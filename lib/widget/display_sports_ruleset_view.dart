import 'package:flutter/material.dart';
import 'package:ref_hq/constants.dart';
import 'package:ref_hq/utils/common_constant.dart';
import 'package:ref_hq/utils/my_colors.dart';

class DisplaySportsRuleSetView extends StatelessWidget {
  final Map<dynamic, dynamic> mapSport;
  final Map<dynamic, dynamic> mapRuleSet;
  double minHeight;
  DisplaySportsRuleSetView({this.mapSport,this.mapRuleSet,this.minHeight=40});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      alignment: Alignment.center,
      // padding: EdgeInsets.only(left: 10, right: 10),
      /*decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(
              width: CommonConstant.borderDropdown, color: Colors.black),
          borderRadius: BorderRadius.circular(10),
          color: MyColors.dropdownBackgroudColor),
      constraints: BoxConstraints(
          minHeight: this.minHeight
      ),*/
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            this.mapSport['name'],
            style: TextStyle(
                color: Colors.white,
                fontSize: 18 /*, decoration: TextDecoration.underline*/),
          ),
          Text(' - '),
          Text(
            this.mapRuleSet['name'],
            style: TextStyle(
                color: Colors.white,
                fontSize: 18 /*, decoration: TextDecoration.underline*/),
          ),
          this.mapRuleSet[sports] == 0?Container(
              width: 70,
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 5,bottom: 5),
              child:
              Image.asset('assets/images/iaab_logo.png')):Container()
        ],
      ),
    );
  }
}

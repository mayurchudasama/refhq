import 'package:flutter/material.dart';

class StatusBarHeight extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(height: MediaQuery.of(context).padding.top,);
  }
}

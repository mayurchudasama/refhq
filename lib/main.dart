import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:package_info/package_info.dart';
import 'package:ref_hq/auth.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:ref_hq/constants.dart';
import 'package:ref_hq/data_models/ruleset.dart';
import 'package:ref_hq/data_models/sports.dart';
import 'package:ref_hq/data_models/user_model.dart';
import 'package:ref_hq/home/home_screen.dart';
import 'package:ref_hq/loginorsignup/user_signin.dart';
import 'package:ref_hq/state_container/state_container.dart';
import 'package:ref_hq/utils/app_preferences.dart';
import 'package:ref_hq/utils/common_constant.dart';
import 'package:ref_hq/utils/email_sender.dart';
import 'package:ref_hq/utils/my_colors.dart';
import 'package:ref_hq/utils/my_log.dart';
import 'package:ref_hq/utils/my_toast.dart';
import 'package:ref_hq/utils/utility.dart';

import 'consumable_store.dart';
import 'misc_widgets.dart';
import 'ref_theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  InAppPurchaseConnection.enablePendingPurchases();
  await FirebaseDatabase.instance.setPersistenceEnabled(true);
  // await FirebaseDatabase.instance.setPersistenceCacheSizeBytes(10000000);
  await FirebaseDatabase.instance.reference().keepSynced(true);

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('google_fonts/LICENSE.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });
  runApp(StateContainer(child: MyApp()));
}

const bool kAutoConsume = true;

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'RefHQ',
      theme: refHQTheme(context),
      home: SubscriptionPage(title: 'RefHQ'),
    );
  }
}

class SubscriptionPage extends StatefulWidget {
  SubscriptionPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SubscriptionPageState createState() => _SubscriptionPageState();
}

class _SubscriptionPageState extends State<SubscriptionPage> {
  /* inappsection */

  bool enableRuleSet = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final InAppPurchaseConnection _connection = InAppPurchaseConnection.instance;
  StreamSubscription<List<PurchaseDetails>> _subscription;

  // ignore: unused_field
  List<String> _notFoundIds = [];
  List<ProductDetails> _products = [];
  List<PurchaseDetails> _purchases = [];

  // ignore: unused_field
  List<String> _consumables = [];

  // ignore: unused_field
  bool _isAvailable = false;

  // ignore: unused_field
  bool _purchasePending = false;
  bool _loading = true;

  // ignore: unused_field
  String _queryProductError;

  /*-----*/

  bool isActiveSubscription = false;

  String currentUser;

  List<DropdownMenuItem<Sports>> _dropdownMenuItemsSport;
  Sports _selectedItemSport;
  List<Sports> listDropdownItemsSport = List();

  List<DropdownMenuItem<Ruleset>> _dropdownMenuItemsRuleSet;
  Ruleset _selectedItemRuleSet;
  List<Ruleset> listDropdownItemsRuleSet = List();

  PackageInfo _packageInfo;

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
    initPlatformState();
    initSportsAndRuleset();



    /*-----*/
    initPrefs();
    Utility.cacheAppData();

    initInAppStore();

  }
  void initInAppStore() async{
    await initStoreInfo();
    checkActiveSubscription();
  }

  void checkActiveSubscription() async{
    debugPrint("debug---log : checkActiveSubscription");
    isActiveSubscription = await activeSubscription(context);
    setState(() {

    });
  }

  Future<void> _initPackageInfo() async {
    debugPrint("debug---log : _initPackageInfo");
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
      debugPrint('App name  ${_packageInfo.appName}');
      debugPrint('Package name  ${_packageInfo.packageName}');
      debugPrint('App version  ${_packageInfo.version}');
      debugPrint('Build number  ${_packageInfo.buildNumber}');
    });
  }

  bool isLoadingSportsAndRuleSet = false;
  // List<Map<String, dynamic>> listSportRuleSet = List();

  void initSportsAndRuleset() async {
    debugPrint("debug---log : initSportsAndRuleset");
    setState(() {
      isLoadingSportsAndRuleSet = true;
    });

    DataSnapshot snapshotSports = await FirebaseDatabase.instance.reference().child(prefix0.sports).once();
    List<dynamic> valuesSports = snapshotSports.value;

    listDropdownItemsSport.clear();
    // listDropdownItemsSport.add(Sports(prefix0.COLLAPSE_SPORT, prefix0.str_collapse_sport, true));
    listDropdownItemsSport.add(Sports(CHOOSE_SPORT, prefix0.str_choose_sport, true));
    List<int> mSportsIds = List();
    valuesSports.forEach((element) {
      if (element[prefix0.enable]) {
        Sports mSport = Sports.fromMap(element);
        listDropdownItemsSport.add(mSport);
        mSportsIds.add(mSport.value);
      }
    });
    listDropdownItemsSport.add(Sports(REQUEST_ANOTHER_SPORT, prefix0.str_request_another_sport, true));

    DataSnapshot snapshotRuleset = await FirebaseDatabase.instance.reference().child(prefix0.ruleset).once();
    List<dynamic> valuesRuleset = snapshotRuleset.value;

    listDropdownItemsRuleSet.clear();
    // listDropdownItemsRuleSet.add(
    //   Ruleset(prefix0.COLLAPSE_RULE_SET, prefix0.str_collapse_rule_set, prefix0.COLLAPSE_RULE_SET, true),
    // );
    listDropdownItemsRuleSet.add(
      Ruleset(CHOOSE_RULESET, prefix0.str_choose_rule_set, CHOOSE_RULESET, true),
    );
    debugPrint("enabled sportsIds : ${mSportsIds.toString()}");
    valuesRuleset.forEach((element) {
      var jsonResponse = Map<String, dynamic>.from(element);
      Ruleset mRuleset = Ruleset.fromMap(jsonResponse);
      if (mSportsIds.contains(mRuleset.sports) && element[prefix0.enable]) {
        listDropdownItemsRuleSet.add(mRuleset);
      }
    });
    listDropdownItemsRuleSet.add(Ruleset(prefix0.REQUEST_ANOTHER_RULESET, prefix0.str_request_another_rule_set, prefix0.REQUEST_ANOTHER_RULESET, true));

    StateContainer.of(context).updateSportsInfo(listDropdownItemsSport);
    StateContainer.of(context).updateRulesetInfo(listDropdownItemsRuleSet);

    _dropdownMenuItemsSport = buildDropDownMenuItemsSport(listDropdownItemsSport);
    _selectedItemSport = _dropdownMenuItemsSport[0].value;

    _dropdownMenuItemsRuleSet = buildDropDownMenuItemsRuleSet(listDropdownItemsRuleSet);
    _selectedItemRuleSet = _dropdownMenuItemsRuleSet[0].value;

    /*valuesSports.forEach((eleSport) {
      if (eleSport[prefix0.enable]) {
        Sports mSport = Sports.fromMap(eleSport);
        Map<String, dynamic> mapTmpSport = Map();
        mapTmpSport['type'] = prefix0.sports;
        mapTmpSport['data'] = mSport;

        debugPrint("Sport : ${mSport.name}");
        listSportRuleSet.add(mapTmpSport);

        valuesRuleset.forEach((eleRuleSet) {
          Ruleset mRuleset = Ruleset.fromMapDynamic(eleRuleSet);
          if (mSport.value == mRuleset.sports && mRuleset.enable) {
            Map<String, dynamic> mapTmpRuleSet = Map();
            mapTmpRuleSet['type'] = prefix0.ruleset;
            mapTmpRuleSet['data'] = mRuleset;
            mapTmpRuleSet[prefix0.sports] = mSport;
            debugPrint("Ruleset : ${mRuleset.name}");
            listSportRuleSet.add(mapTmpRuleSet);
          }
        });
      }
    });*/
    setState(() {
      isLoadingSportsAndRuleSet = false;
    });
  }

  void initPrefs() async {
    // prefSports = prefs.getString(AppPreferences.pref_sports);
    // prefRuleSet = prefs.getString(AppPreferences.pref_rule_set);
  }

  double _statusBarHeight = 0.0;

  Future<void> initPlatformState() async {
    debugPrint("debug---log : initPlatformState");
    double statusBarHeight;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      statusBarHeight = await FlutterStatusbarManager.getHeight;
    } on PlatformException {
      statusBarHeight = 0.0;
    }
    if (!mounted) return;

    setState(() {
      _statusBarHeight = statusBarHeight;
    });
    await FlutterStatusbarManager.setHidden(false, animation: StatusBarAnimation.SLIDE);
  }

  @override
  Widget build(BuildContext context) {
    return (_loading)
        ? Scaffold(
            extendBody: true,
            extendBodyBehindAppBar: true,
            body: SafeArea(
              child: LinearProgressIndicator(),
            ),
          )
        : isActiveSubscription
            ? (currentUser != null)
                ? isLoadingSportsAndRuleSet
                    ? Scaffold(
                        extendBody: true,
                        extendBodyBehindAppBar: true,
                        body: SafeArea(
                          child: LinearProgressIndicator(),
                        ),
                      )
                    : HomeScreen()
                : UserSignIn(
                    hasSubscription: true,
                  )
            : (currentUser != null)
                ? isLoadingSportsAndRuleSet
                    ? Scaffold(
                        extendBody: true,
                        extendBodyBehindAppBar: true,
                        body: SafeArea(
                          child: LinearProgressIndicator(),
                        ),
                      )
                    : HomeScreen()
                : isLoadingSportsAndRuleSet
                    ? Scaffold(
                        extendBody: true,
                        extendBodyBehindAppBar: true,
                        body: SafeArea(
                          child: LinearProgressIndicator(),
                        ),
                      )
                    : Scaffold(
                        extendBodyBehindAppBar: true,
                        extendBody: true,
                        // type: MaterialType.transparency,
                        key: _scaffoldKey,
                        body: LayoutBuilder(
                          builder: (BuildContext context, BoxConstraints constraints) {
                            return Container(
                              constraints: BoxConstraints(minHeight: constraints.minHeight, maxHeight: constraints.maxHeight),
                              decoration: BoxDecoration(
                                image: DecorationImage(image: AssetImage('assets/images/background.jpg'), fit: BoxFit.fill),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                //                    mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      width: double.infinity,
                                      /*decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage('assets/images/background.jpg'),
                                        fit: BoxFit.fill),
                                  ),*/
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text("Welcome to RefHQ!",
                                              style: Theme.of(context).textTheme.subhead.merge(
                                                    TextStyle(
                                                      fontSize: 22,
                                                    ),
                                                  )),
                                          SizedBox(
                                            height: 30,
                                          ),
                                          Text("Equipping sports officials to master the\nrules and best serve the game",
                                              textAlign: TextAlign.center,
                                              style: Theme.of(context).textTheme.subhead.merge(
                                                    TextStyle(fontSize: 16, fontStyle: FontStyle.italic),
                                                  )),
                                          SizedBox(
                                            height: 30,
                                          ),
                                          /*GestureDetector(
                                            onTap: () {
                                              _showMyDialog();
                                            },
                                            child: Container(
                                              alignment: Alignment.centerLeft,
                                              padding: EdgeInsets.only(left: 10, right: 10),
                                              decoration: BoxDecoration(shape: BoxShape.rectangle, border: Border.all(width: CommonConstant.borderDropdown, color: Colors.black), borderRadius: BorderRadius.circular(10), color: MyColors.dropdownBackgroudColor),
                                              constraints: BoxConstraints(
                                                minHeight: 55,
                                                minWidth: 100,
                                                maxWidth: 325
                                              ),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    // color: Colors.grey,
                                                    child: Text((selectedDialogRuleset==null && selectedDialogSport == null)?'Sport - Ruleset':
                                                    "${selectedDialogSport.name} - ${selectedDialogRuleset.name}",
                                                        style: TextStyle(
                                                            // fontStyle: (listItem.name == prefix0.str_request_another_rule_set)?FontStyle.italic:FontStyle.normal,
                                                            //   color: MyColors.btnGoTextColor,
                                                            fontSize: 18 *//*,
                                                            decoration: TextDecoration.underline*//*
                                                            )),
                                                  ),
                                                  Icon(
                                                    Icons.expand_more,
                                                    // color: MyColors.btnGoTextColor,
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),*/
                                          Container(
                                            width: 300,
                                            padding: EdgeInsets.only(left: 10, right: 10),
                                            decoration: BoxDecoration(shape: BoxShape.rectangle, border: Border.all(width: CommonConstant.borderDropdown, color: Colors.black), borderRadius: BorderRadius.circular(10), color: MyColors.dropdownBackgroudColor),
                                            height: 55,
                                            child: DropdownButtonHideUnderline(
                                              child: DropdownButton(
                                                  isExpanded: true,
                                                  icon: Icon(
                                                    Icons.keyboard_arrow_down,
                                                    color: Colors.white,
                                                    size: 25,
                                                  ),
                                                  value: _selectedItemSport,
                                                  items: _dropdownMenuItemsSport,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      enableRuleSet = true;
                                                      _selectedItemSport = value;

                                                      if (_selectedItemSport.value == CHOOSE_SPORT) {
                                                        _dropdownMenuItemsRuleSet = buildDropDownMenuItemsRuleSet(listDropdownItemsRuleSet);
                                                        _selectedItemRuleSet = _dropdownMenuItemsRuleSet[0].value;

                                                        enableRuleSet = false;
                                                        return;
                                                      }
                                                      if (_selectedItemSport.value == REQUEST_ANOTHER_SPORT) {
                                                        //                                                        Utility.requestAnotherSport(_selectedItemSport);
                                                        debugPrint("${_selectedItemRuleSet.value}");
                                                        _selectedItemRuleSet = Ruleset(REQUEST_ANOTHER_RULESET, prefix0.str_request_another_rule_set, REQUEST_ANOTHER_RULESET, true);
                                                        List<Ruleset> newListRuleset = List();
                                                        _dropdownMenuItemsRuleSet = buildDropDownMenuItemsRuleSet(newListRuleset);
                                                        return;
                                                      }

                                                      List<Ruleset> newListRuleset = List();
                                                      newListRuleset.add(
                                                        Ruleset(CHOOSE_RULESET, prefix0.str_choose_rule_set, CHOOSE_RULESET, true),
                                                      );
                                                      listDropdownItemsRuleSet.forEach((element) {
                                                        if (element.sports == _selectedItemSport.value) {
                                                          newListRuleset.add(element);
                                                        }
                                                      });

                                                      newListRuleset.add(Ruleset(REQUEST_ANOTHER_RULESET, prefix0.str_request_another_rule_set, REQUEST_ANOTHER_RULESET, true));

                                                      _dropdownMenuItemsRuleSet = buildDropDownMenuItemsRuleSet(newListRuleset);
                                                      _selectedItemRuleSet = _dropdownMenuItemsRuleSet[0].value;
                                                    });
                                                  }),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          enableRuleSet
                                              ? Container()
                                              : Container(
                                                  alignment: Alignment.centerLeft,
                                                  width: 300,
                                                  padding: EdgeInsets.only(left: 10, right: 10),
                                                  decoration: BoxDecoration(shape: BoxShape.rectangle, border: Border.all(width: CommonConstant.borderDropdown, color: Colors.black), borderRadius: BorderRadius.circular(10), color: MyColors.dropdownBackgroudColor),
                                                  constraints: BoxConstraints(minHeight: 55),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Container(
                                                        // color: Colors.grey,
                                                        child: Text(prefix0.str_choose_rule_set,
                                                            style: TextStyle(
                                                                // fontStyle: (listItem.name == prefix0.str_request_another_rule_set)?FontStyle.italic:FontStyle.normal,
                                                                color: MyColors.btnGoTextColor,
                                                                fontSize: 18,
                                                                decoration: TextDecoration.underline)),
                                                      ),
                                                      Icon(
                                                        Icons.expand_more,
                                                        color: MyColors.btnGoTextColor,
                                                      )
                                                    ],
                                                  ),
                                                ),
                                          listDropdownItemsRuleSet != null && enableRuleSet
                                              ? Container(
                                                  width: 300,
                                                  padding: EdgeInsets.only(left: 10, right: 10),
                                                  decoration: BoxDecoration(shape: BoxShape.rectangle, border: Border.all(width: CommonConstant.borderDropdown, color: Colors.black), borderRadius: BorderRadius.circular(10), color: MyColors.dropdownBackgroudColor),
                                                  height: 55,
                                                  child: DropdownButtonHideUnderline(
                                                    child: DropdownButton(
                                                        icon: Icon(
                                                          Icons.keyboard_arrow_down,
                                                          color: Colors.white,
                                                          size: 25,
                                                        ),
                                                        value: _selectedItemRuleSet,
                                                        items: _dropdownMenuItemsRuleSet,
                                                        onChanged: (value) async {
                                                          setState(() {
                                                            _selectedItemRuleSet = value;
                                                          });
                                                        }),
                                                  ),
                                                )
                                              : Container(),
                                          SizedBox(
                                            height: 30,
                                          ),
                                          (/*selectedDialogSport == null && selectedDialogRuleset == null*/_selectedItemSport.value == -1 || _selectedItemRuleSet.value == -1)
                                              ? Container(
                                                  decoration: BoxDecoration(shape: BoxShape.rectangle, border: Border.all(width: 2, color: Colors.black), borderRadius: BorderRadius.circular(10), color: MyColors.btnGoColor),
                                                  width: 100,
                                                  height: 50,
                                                  child: FlatButton(
                                                    child: Text(
                                                      "GO",
                                                      style: TextStyle(color: MyColors.btnGoTextColor, fontSize: 20),
                                                    ),
                                                  ),
                                                )
                                              : Container(
                                                  decoration: BoxDecoration(shape: BoxShape.rectangle, border: Border.all(width: 2, color: Colors.black), borderRadius: BorderRadius.circular(10), color: MyColors.btnGoColor),
                                                  width: 100,
                                                  height: 50,
                                                  child: FlatButton(
                                                    onPressed: onClickGoButton,
                                                    child: Text(
                                                      "GO",
                                                      style: TextStyle(color: Colors.white, fontSize: 20),
                                                    ),
                                                  ),
                                                ),
                                          SizedBox(
                                            height: 30,
                                          ),
                                          Text("You may select different sports / rule sets later",
                                              textAlign: TextAlign.center,
                                              style: Theme.of(context).textTheme.subhead.merge(
                                                    TextStyle(fontSize: 16, fontStyle: FontStyle.italic),
                                                  )),
                                          SizedBox(
                                            height: 30,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),

                                  Container(
                                    margin: EdgeInsets.only(top: 8, bottom: 8),
                                    child: bottomLogo(),
                                  ),
                                  //
                                ],
                              ),
                            );
                          },
                        ));
  }

  /*void onClickGoButtonNew() async {

    Map<String, dynamic> sport = Sports.toJson(selectedDialogSport);
    Map<String, dynamic> ruleSet = Ruleset.toJson(selectedDialogRuleset);

    StateContainer.of(context).updateSelectedSport(sport);
    StateContainer.of(context).updateSelectedRuleset(ruleSet);
    // await prefs.setString(AppPreferences.pref_sports, json.encode(sport));
    // await prefs.setString(AppPreferences.pref_rule_set, json.encode(ruleSet));
    *//*debugPrint("sport : ${json.decode(prefs.getString(AppPreferences.pref_sports))}");
    debugPrint("ruleset : ${json.decode(prefs.getString(AppPreferences.pref_rule_set))}");*//*

//    return;
    if (currentUser == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => UserSignIn(
                hasSubscription: false,
              )),
              (route) => false);
    } else {
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => HomeScreen()), (route) => false);
    }
  }*/
  void onClickGoButton() async {
    if (_selectedItemSport.value == prefix0.REQUEST_ANOTHER_SPORT) {
      await EmailSender.requestAnotherSport(_selectedItemSport);
      return;
    }
    if (_selectedItemRuleSet.value == prefix0.REQUEST_ANOTHER_RULESET) {
      await EmailSender.requestAnotherRuleSet(_selectedItemRuleSet);
      return;
    }
    Map<String, dynamic> sport = Sports.toJson(_selectedItemSport);
    Map<String, dynamic> ruleSet = Ruleset.toJson(_selectedItemRuleSet);

    StateContainer.of(context).updateSelectedSport(sport);
    StateContainer.of(context).updateSelectedRuleset(ruleSet);
    // await prefs.setString(AppPreferences.pref_sports, json.encode(sport));
    // await prefs.setString(AppPreferences.pref_rule_set, json.encode(ruleSet));
    /*debugPrint("sport : ${json.decode(prefs.getString(AppPreferences.pref_sports))}");
    debugPrint("ruleset : ${json.decode(prefs.getString(AppPreferences.pref_rule_set))}");*/

//    return;
    if (currentUser == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => UserSignIn(
                    hasSubscription: false,
                  )),
          (route) => false);
    } else {
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => HomeScreen()), (route) => false);
    }
  }

  Widget _navigateToSignIn() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => UserSignIn()));
    return SizedBox();
  }

  List<DropdownMenuItem<Sports>> buildDropDownMenuItemsSport(List listItems) {
    List<DropdownMenuItem<Sports>> items = List();
    for (Sports listItem in listItems) {
      /*if (listItem.value == prefix0.COLLAPSE_SPORT) {
        items.add(
          DropdownMenuItem(
            child: Align(
              alignment: Alignment.centerLeft,
              child: Container(
                constraints: BoxConstraints(
                  minWidth: MediaQuery.of(context).size.width
                ),
                // color: Colors.red,
                alignment: Alignment.centerRight,
                // width: double.infinity,
                child: Icon(Icons.expand_less),
              ),
            ),
            value: listItem,
          ),
        );
      } else {*/
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: Text(
                listItem.name ?? "",
                style: TextStyle(fontStyle: (listItem.name == prefix0.str_request_another_sport) ? FontStyle.italic : FontStyle.normal, color: Colors.white, fontSize: 18, decoration: listItem.name == prefix0.str_choose_sport ? TextDecoration.underline : TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
      /*}*/
    }
    return items;
  }

  List<DropdownMenuItem<Ruleset>> buildDropDownMenuItemsRuleSet(List listItems) {
    List<DropdownMenuItem<Ruleset>> items = List();
    for (Ruleset listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: Text(
                listItem.name,
                style: TextStyle(fontStyle: (listItem.name == prefix0.str_request_another_rule_set) ? FontStyle.italic : FontStyle.normal, color: Colors.white, fontSize: 18, decoration: listItem.name == prefix0.str_choose_rule_set ? TextDecoration.underline : TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }

  Future<void> initStoreInfo() async {
    debugPrint("debug---log : initStoreInfo");
    final bool isAvailable = await _connection.isAvailable();
    debugPrint("isAvailable : $isAvailable");
    if (!isAvailable) {
      setState(() {
        _isAvailable = isAvailable;
        _products = [];
        _purchases = [];
        _notFoundIds = [];
        _consumables = [];
        _purchasePending = false;
        _loading = false;
      });
      return;
    }

    try {
      var _auth = Auth();
      currentUser = await _auth.currentUser();
    } catch (e) {
      print(e);
    }

    ProductDetailsResponse productDetailResponse = await _connection.queryProductDetails(Utility.productIds.toSet());
    debugPrint("productDetailResponse : $productDetailResponse");
    if (productDetailResponse.error != null) {
      debugPrint("productDetailResponse 1");
      setState(() {
        _queryProductError = productDetailResponse.error.message;
        _isAvailable = isAvailable;
        _products = productDetailResponse.productDetails;
        _purchases = [];
        _notFoundIds = productDetailResponse.notFoundIDs;
        _consumables = [];
        _purchasePending = false;
        _loading = false;

        debugPrint("_isAvailable : $productDetailResponse");
        debugPrint("_products : $_products");
        _products.forEach((element) {
          debugPrint("_products : ${element.toString()}");
        });
      });
      return;
    }
    debugPrint("productDetailResponse 2: ");
    if (productDetailResponse.productDetails.isEmpty) {
      debugPrint("productDetailResponse 3: ");
      setState(() {
        _queryProductError = null;
        _isAvailable = isAvailable;
        _products = productDetailResponse.productDetails;
        _purchases = [];
        _notFoundIds = productDetailResponse.notFoundIDs;
        _consumables = [];
        _purchasePending = false;
        _loading = false;
      });
      return;
    }
    debugPrint("productDetailResponse 4: ");
    final QueryPurchaseDetailsResponse purchaseResponse = await _connection.queryPastPurchases();
    if (purchaseResponse.error != null) {
      // handle query past purchase error..
    }
    final List<PurchaseDetails> verifiedPurchases = [];
    for (PurchaseDetails purchase in purchaseResponse.pastPurchases) {
      debugPrint("productDetailResponse 5: ");
      if (await _verifyPurchase(purchase)) {
        if (Platform.isIOS) {
          InAppPurchaseConnection.instance.completePurchase(purchase);
        }
        verifiedPurchases.add(purchase);
      }
    }
    List<String> consumables = await ConsumableStore.load();
    setState(() {
      _isAvailable = isAvailable;
      _products = productDetailResponse.productDetails;
      _purchases = verifiedPurchases;
      _notFoundIds = productDetailResponse.notFoundIDs;
      _consumables = consumables;
      _purchasePending = false;
      _loading = false;
      debugPrint("productDetailResponse 6: ");
      _products.forEach((element) {
        Map<String, dynamic> mapProduct = Map();
        mapProduct['title'] = element.title;
        mapProduct['description'] = element.description;
        mapProduct['id'] = element.id;
        mapProduct['price'] = element.price;
        mapProduct['skProduct'] = element.skProduct;
        mapProduct['skuDetail'] = element.skuDetail;
        debugPrint("_products : ${mapProduct.toString()}");
      });

      debugPrint("purchase DetailResponse 7: ${_purchases.length} ");
      _purchases.forEach((element) {
        debugPrint("1 ${element.productID}");
      });
    });
  }

  void _listenToPurchaseUpdated(List<PurchaseDetails> purchaseDetailsList) {
    purchaseDetailsList.forEach((PurchaseDetails purchaseDetails) async {
      if (purchaseDetails.status == PurchaseStatus.pending) {
        showPendingUI();
      } else {
        if (purchaseDetails.status == PurchaseStatus.error) {
          handleError(purchaseDetails.error);
        } else if (purchaseDetails.status == PurchaseStatus.purchased) {
          bool valid = await _verifyPurchase(purchaseDetails);
          if (valid) {
            deliverProduct(purchaseDetails);
          } else {
            _handleInvalidPurchase(purchaseDetails);
          }
        }
        if (Platform.isIOS) {
          InAppPurchaseConnection.instance.completePurchase(purchaseDetails);
        } else if (Platform.isAndroid) {
          /*if (!kAutoConsume && purchaseDetails.productID == _kConsumableId) {
            InAppPurchaseConnection.instance.consumePurchase(purchaseDetails);
          }*/
        }
      }
    });
  }

  void showPendingUI() {
    setState(() {
    _purchasePending = true;
    });
  }

  void handleError(IAPError error) {
    setState(() {
    _purchasePending = false;
    });
    String errorMessage = error.message;
    switch (errorMessage) {
      case 'BillingResponse.userCanceled':
        break;
      case 'BillingResponse.error':
        displaySnackBarWithKey('Billing Error. Purchase failed.');
        break;
      default:
        displaySnackBarWithKey('main Unknown Error');
    }
  }

  Future<bool> _verifyPurchase(PurchaseDetails purchaseDetails) {
    // IMPORTANT!! Always verify a purchase before delivering the product.
    // For the purpose of an example, we directly return true.
    if (purchaseDetails != null &&
        (purchaseDetails.status == PurchaseStatus.purchased || purchaseDetails.transactionDate != null) &&
        Utility.productIds.contains(purchaseDetails.productID)) {
      return Future<bool>.value(true);
    }
    else {
      return Future<bool>.value(false);
    }

  }

  void deliverProduct(PurchaseDetails purchaseDetails) async {
    // IMPORTANT!! Always verify a purchase purchase details before delivering the product.
    setState(() {
    _purchases.add(purchaseDetails);
    _purchasePending = false;
    });
  }

  void _handleInvalidPurchase(PurchaseDetails purchaseDetails) {
    // handle invalid purchase here if  _verifyPurchase` failed.
  }

  void displaySnackBarWithKey(String s) {
    MyLog.printInappLog("displaySnackBarWithKey : ${s}");
    Fluttertoast.showToast(msg: s, toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
  }

  showActiveMessage(PurchaseDetails item) {
    String strMsg = '';
    if (item == null) {
      strMsg = "showActiveMessage : No subscription";
    } else {
      strMsg = "showActiveMessage : Active subscription is : ${Utility.mapFromPurchaseDetail(item)}";
    }
    MyLog.printInappLog(strMsg);
  }

  activeSubscription(BuildContext context) async{
    MyLog.printInappLog("Total purchased : ${_purchases.length}");
    String purchased = '';
    for (PurchaseDetails item in _purchases) {
      purchased += '${item.productID}, ';
    }
    MyLog.printInappLog("Purchased Items : ${purchased}");

    for (PurchaseDetails item in _purchases) {
      if (item.productID == Utility.productIds[0]) {
        /* Update data in state management */
        String updateSubscriptionType = Utility.productIds[1];
        StateContainer.of(context).updateSubscriptionType(updateSubscriptionType);

        /* If user has old subscription then give Single Ruleset subscription with Basketball - NCAA-M (2020-21)*/
        DatabaseReference dbRef = FirebaseDatabase.instance.reference();
        FirebaseUser firebaseUser = await FirebaseAuth.instance.currentUser();
        if(firebaseUser == null){
          showActiveMessage(item);
          return false;
        }
        String uid = await Auth().currentUser();
        var users = await dbRef.child(prefix0.users).child(uid).once();
        UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));
        if(currentUser.sports == null && currentUser.ruleset == null ){
          Map<String,dynamic> mapSports= Map();
          mapSports[prefix0.enable] = true;
          mapSports['name'] = 'Basketball';
          mapSports[prefix0.value] = 0;

          Map<String,dynamic> mapRuleset= Map();
          mapRuleset[prefix0.enable] = true;
          mapRuleset['name'] = 'NCAA-M (2020-21)';
          mapRuleset[prefix0.value] = 0;
          mapSports[prefix0.sports] = 0;

          await dbRef.child(prefix0.users).child(userId).update({
            prefix0.subscriptionType:updateSubscriptionType,
            prefix0.sports:mapSports,
            prefix0.ruleset:mapRuleset
          });

        }
        else{
          DatabaseReference dbRef = FirebaseDatabase.instance.reference();
          await dbRef.child(prefix0.users).child(userId).update({
            prefix0.subscriptionType:updateSubscriptionType
          });
        }


        showActiveMessage(item);
        return true;
      } else if (item.productID == Utility.productIds[1]) {
        StateContainer.of(context).updateSubscriptionType(item.productID);
        showActiveMessage(item);
        return true;
      } else if (item.productID == Utility.productIds[2]) {
        StateContainer.of(context).updateSubscriptionType(item.productID);
        showActiveMessage(item);
        return true;
      } else if (item.productID == Utility.productIds[3]) {
        StateContainer.of(context).updateSubscriptionType(item.productID);
        showActiveMessage(item);
        return true;
      }
    }

    showActiveMessage(null);
    return false;
  }
/*
  Sports selectedDialogSport;
  Ruleset selectedDialogRuleset;
  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.transparent,
          // title: Text('AlertDialog Title'),
          content: Container(
            padding: EdgeInsets.only(top: 10,bottom: 10,left: 30,right: 30),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                border: Border.all(width: 2, color: Colors.black),
                borderRadius: BorderRadius.circular(10), color: MyColors.btnGoColor),
            child: SingleChildScrollView(
              child: ListBody(
                *//*children: <Widget>[
                Text('This is a demo alert dialog.'),
                Text('Would you like to approve of this message?'),
                listDropdownItemsSport
              ],*//*
                children: listSportRuleSet.asMap().entries.map((sportRulesetEntries) {
                  Sports sport;
                  Ruleset ruleset;
                  String type = sportRulesetEntries.value['type'];
                  if (type == prefix0.sports) {
                    sport = sportRulesetEntries.value['data'];
                  } else if (type == prefix0.ruleset) {
                    sport = sportRulesetEntries.value[prefix0.sports];
                    ruleset = sportRulesetEntries.value['data'];
                  }
                  return Builder(
                    builder: (context) {
                      return GestureDetector(
                        onTap: () {},
                        child: Container(
                          child: type == prefix0.sports ?
                              Container(
                                child: Text("${sport.name}",
                                    style: TextStyle(
                                    fontSize: 18 ,
                                    decoration: TextDecoration.underline,
                                    height: 3
                                ))) :
                              GestureDetector(
                                onTap: (){

                                  selectedDialogSport = sport;
                                  selectedDialogRuleset = ruleset;
                                  setState(() {

                                  });
                                  Navigator.of(context).pop();
                                },
                                child: Container(child: Text("     ${ruleset.name}",
                                    style: TextStyle(
                                        fontSize: 18 ,
                                        height: 2
                                    )),),
                              ),
                        ),
                      );
                    },
                  );
                }).toList(),
              ),
            ),
          ),
          *//*actions: <Widget>[
          TextButton(
            child: Text('Approve'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],*//*
        );
      },
    );
  }
  */
}


import 'package:connectivity/connectivity.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ref_hq/auth.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:ref_hq/data_models/user_model.dart';
import 'package:ref_hq/newsletter/newsletter_month_screen.dart';
import 'package:ref_hq/utils/my_colors.dart';
import 'package:ref_hq/widget/display_sports_ruleset_view.dart';
import 'package:ref_hq/widget/status_bar_height.dart';

class NewsletterYearScreen extends StatefulWidget {
  @override
  _NewsletterYearScreenState createState() => _NewsletterYearScreenState();
}

class _NewsletterYearScreenState extends State<NewsletterYearScreen> {

  bool isGettingNewsLetter = false;
  List<Map<String, dynamic>> listNewsLetter = List();

  Map<dynamic, dynamic> mapSport = Map();
  Map<dynamic, dynamic> mapRuleSet = Map();

  @override
  void initState() {
    FirebaseAnalytics().setAnalyticsCollectionEnabled(true);
    super.initState();
    initPrefs();
  }

  void initPrefs() async {
    setState(() {
      isGettingNewsLetter = true;
    });
    await initNewsLetterConnection();

    DatabaseReference ref = FirebaseDatabase.instance.reference();
    String uid = await Auth().currentUser();
    var users = await ref.child(prefix0.users).child(uid).once();
    UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));
    // prefSports = prefs.getString(AppPreferences.pref_sports);
    // mapSport = json.decode(prefSports);
    mapSport = currentUser.sports;

    // prefRuleSet = prefs.getString(AppPreferences.pref_rule_set);
    // mapRuleSet = json.decode(prefRuleSet);
    mapRuleSet = currentUser.ruleset;
    setState(() {
      isGettingNewsLetter = false;
    });
  }

  Future<void> getNewsLetter() async {
    List<int> yearsKey = List();
    DataSnapshot snapshot = await FirebaseDatabase.instance.reference().child(prefix0.newsletter).once();
    Map<dynamic, dynamic> valuesNewsLetter = snapshot.value;
    listNewsLetter.clear();
    valuesNewsLetter.forEach((key, value) {
      yearsKey.add(int.parse(key));
      debugPrint("key : ${key.toString()}");
      debugPrint("values : ${value.toString()}");
      Map<String, dynamic> mapNewsletter = Map();
      mapNewsletter[prefix0.key] = "${key.toString()}";
      mapNewsletter[prefix0.value] = value;
      listNewsLetter.add(mapNewsletter);
    });
    /*debugPrint("before sort : ${yearsKey.toString()}");
    yearsKey.sort((b, a) => a.compareTo(b));
    debugPrint("after sort : ${yearsKey.toString()}");*/
    debugPrint("before sort : ${listNewsLetter.toString()}");
    listNewsLetter.sort((b, a) => a[prefix0.key].compareTo(b[prefix0.key]));
    debugPrint("after sort : ${listNewsLetter.toString()}");
  }

  Future<void> initNewsLetterConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      await FirebaseDatabase.instance.goOffline();
      getNewsLetter();
    } else {
      await FirebaseDatabase.instance.goOnline();
      getNewsLetter();
    }
  }


  @override
  Widget build(BuildContext context) {
    double heightProgress = 4.0;
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: SafeArea(
        top: false,
        bottom: false,
        child: LayoutBuilder(
          builder: (context, constraints) {
            return Container(
              height: constraints.maxHeight,
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage('assets/images/background.jpg'), fit: BoxFit.fill),
              ),
              child: Column(
                children: [
                  StatusBarHeight(),
                  Row(
                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: SizedBox(width: 55, height: 55, child: Icon(Icons.arrow_back_ios))),
                      Expanded(
                        child: Text(
                          "The Match-Up Newsletter",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        width: 55,
                        height: 55,
                      )
                    ],
                  ),
                  isGettingNewsLetter
                      ? Container(height: heightProgress, child: LinearProgressIndicator())
                      : Container(
                          height: heightProgress,
                        ),
                  isGettingNewsLetter
                      ? Container()
                      : DisplaySportsRuleSetView(
                          mapSport: mapSport,
                          mapRuleSet: mapRuleSet,
                        ),
                  SizedBox(
                    height: 20,
                  ),
                  listNewsLetter.length > 0
                      ? Expanded(
                          flex: 1,
                          child: ListView.separated(
                              shrinkWrap: true,
                              itemBuilder: (context, index) {
                                final newsLetter = listNewsLetter[index];
                                return Container(
                                  padding: EdgeInsets.only(left: 15, right: 15),
                                  height: 55,
                                  child: ConstrainedBox(
                                    constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                    child: RaisedButton(
                                      color: MyColors.pregameNotesListButtonColor,
                                      child: Container(
                                          alignment: Alignment.centerLeft,
                                          width: double.infinity,
                                          height: 55,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                "${newsLetter[prefix0.key]}",
                                                style: TextStyle(fontSize: 16),
                                              ),
                                              Icon(Icons.chevron_right)
                                            ],
                                          )),
                                      onPressed: () async {
                                        List<dynamic> listMonths = List();
                                        listMonths = newsLetter[prefix0.value];
                                        Navigator.of(context).push(MaterialPageRoute(
                                          builder: (context) => NewsletterMonthScreen(
                                            listNewsLetterDetail: listMonths,
                                          ),
                                        ));
                                      },
                                    ),
                                  ),
                                );
                              },
                              separatorBuilder: (context, index) {
                                return SizedBox(
                                  height: 10,
                                );
                              },
                              itemCount: listNewsLetter.length),
                        )
                      : Container()
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:ref_hq/newsletter/newsletter_document_view_screen.dart';
import 'package:ref_hq/utils/my_colors.dart';
import 'package:ref_hq/widget/status_bar_height.dart';
class NewsletterMonthScreen extends StatefulWidget {
  List<dynamic> listNewsLetterDetail = List();
  NewsletterMonthScreen({this.listNewsLetterDetail});
  @override
  _NewsletterMonthScreenState createState() => _NewsletterMonthScreenState();
}

class _NewsletterMonthScreenState extends State<NewsletterMonthScreen> {

  @override
  void initState() {
    FirebaseAnalytics().setAnalyticsCollectionEnabled(true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double heightProgress = 4.0;
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: SafeArea(
        child: LayoutBuilder(
          builder: (context, constraints) {
            return Container(
              height: constraints.maxHeight,
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage('assets/images/background.jpg'), fit: BoxFit.fill),
              ),
              child: Column(
                children: [
                  StatusBarHeight(),
                  Row(
                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: SizedBox(
                              width: 55,
                              height: 55,
                              child: Icon(Icons.arrow_back_ios))),
                      Expanded(
                        child: Text("The Match-Up Newsletter",textAlign: TextAlign.center,style: TextStyle(
                            fontSize: 20
                        ),),
                      ),SizedBox(
                        width: 55,
                        height: 55,)
                    ],
                  ),
                  SizedBox(height: 40,),
                  Expanded(
                    flex: 1,
                    child: ListView.separated(
                      shrinkWrap: true,
                        itemBuilder: (context, index) {
                          final newsLetter = widget.listNewsLetterDetail[index];
                          return Container(
                            padding: EdgeInsets.only(left: 15,right: 15),
                            height: 55,
                            child: ConstrainedBox(
                              constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                              child: RaisedButton(
                                color: MyColors.pregameNotesListButtonColor,
                                child: Container(
                                    alignment: Alignment.centerLeft,
                                    width: double.infinity,
                                    height: 55,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "${newsLetter['month']}",
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        Icon(Icons.chevron_right)
                                      ],
                                    )),
                                onPressed: () async {
                                  Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => NewsletterDocumentViewScreen(
                                      title:newsLetter['month'] ,
                                      documentLink:newsLetter[prefix0.documentLink] ,
                                    ),
                                  ));
                                },
                              ),
                            ),
                          );
                        },
                        separatorBuilder: (context, index) {
                          return SizedBox(
                            height: 10,
                          );
                        },
                        itemCount: widget.listNewsLetterDetail.length),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

}

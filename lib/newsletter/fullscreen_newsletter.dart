import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:ref_hq/constants.dart';
import 'package:ref_hq/data_models/ruleset.dart';
import 'package:ref_hq/data_models/sports.dart';
import 'package:ref_hq/data_models/user_model.dart';
import 'package:ref_hq/misc_widgets.dart';
import 'package:ref_hq/ref_theme.dart';
import 'package:ref_hq/state_container/state_container.dart';
import 'package:ref_hq/utils/app_preferences.dart';
import 'package:ref_hq/utils/my_colors.dart';
import 'package:ref_hq/utils/utility.dart';

class FullscreenNewsletter extends StatefulWidget {
  String documentLink;
  FullscreenNewsletter({this.documentLink});
  @override
  _FullscreenNewsletterState createState() => _FullscreenNewsletterState();
}

class _FullscreenNewsletterState extends State<FullscreenNewsletter> {
  bool _isLoading = true;
  PDFDocument doc;

  @override
  void initState() {
    FirebaseAnalytics().setAnalyticsCollectionEnabled(true);
    super.initState();
    _loadFromUrl();
  }



  void _loadFromUrl() async {
    setState(() {
      _isLoading = true;
    });
    doc = await PDFDocument.fromURL(widget.documentLink);
    setState(() {
      _isLoading = false;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height, minWidth: MediaQuery.of(context).size.width),
              child: _isLoading
                  ? Center(child: SizedBox(height: 20, width: 20, child: CircularProgressIndicator()))
                  : PDFViewer(
                      showIndicator: false,
                      showPicker: false,
                      showNavigation: true,
                      document: doc,
                    ),
            ),
            Container(
              margin: EdgeInsets.only(top: AppBar().preferredSize.height),
              height: 50,
              child: Row(
                children: [
                  Expanded(
                    child: Container(),
                  ),
                  Container(
                    height: 50,
                    width: 50,
                    padding: EdgeInsets.only(right: 10),
                    child: IconButton(
                      icon: Icon(Icons.close,size: 35,),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: Text(widget.title),
//        centerTitle: true,
//      ),
//      body: WebView(
//        initialUrl: 'https://docs.google.com/gview?embedded=true&url=${widget.documentLink}'/*widget.documentLink*//*'https://flutter.dev'*/,//"https://firebasestorage.googleapis.com/v0/b/refhq-f57bf.appspot.com/o/newsletter%2FGSSSB%20BIN%20SACHIVALAY%20CLERK%2017-11-2019.pdf?alt=media&token=09ad4932-0f4b-4f37-972c-96445a0c56ef"/*"http://www.pdf995.com/samples/pdf.pdf"*/ /*"https://firebasestorage.googleapis.com/v0/b/refhq-f57bf.appspot.com/o/newsletter%2FGSSSB%20BIN%20SACHIVALAY%20CLERK%2017-11-2019.pdf?alt=media&token=09ad4932-0f4b-4f37-972c-96445a0c56ef"*//*widget.documentLink*/,
//        initialUrl: widget.documentLink,
//        javascriptMode: JavascriptMode.unrestricted,
//        onWebViewCreated: (WebViewController webViewController) {
//          _controller.complete(webViewController);
//        },
//      ),
//    );
//  }

}

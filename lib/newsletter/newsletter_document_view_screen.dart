
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:ref_hq/newsletter/fullscreen_newsletter.dart';
import 'package:ref_hq/widget/status_bar_height.dart';

class NewsletterDocumentViewScreen extends StatefulWidget {
  final title;
  String documentLink;

  NewsletterDocumentViewScreen({this.title, this.documentLink});

  @override
  _NewsletterDocumentViewScreenState createState() => _NewsletterDocumentViewScreenState();
}

class _NewsletterDocumentViewScreenState extends State<NewsletterDocumentViewScreen> {
  bool _isLoading = true;
  PDFDocument doc;

  @override
  void initState() {
    FirebaseAnalytics().setAnalyticsCollectionEnabled(true);
    super.initState();
    _loadFromUrl();
  }

  void _loadFromUrl() async {
    setState(() {
      _isLoading = true;
    });
    doc = await PDFDocument.fromURL(widget.documentLink);
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: SafeArea(
        child: Container(
          constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height, minWidth: MediaQuery.of(context).size.width),
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage('assets/images/background.jpg'), fit: BoxFit.fill),
          ),
          child:Column(
            children: [
              StatusBarHeight(),
              Row(
                children: [
                  GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: SizedBox(
                          width: 55,
                          height: 55,
                          child: Icon(Icons.arrow_back_ios))),
                  Expanded(
                    child: Text(widget.title,textAlign: TextAlign.center,style: TextStyle(
                        fontSize: 20
                    ),),
                  ),SizedBox(
                    width: 55,
                    height: 55,)
                ],
              ),
              Expanded(child:  _isLoading
                  ? Center(child: SizedBox(height: 20, width: 20, child: CircularProgressIndicator()))
                  : GestureDetector(
                onTapDown: (details) {
                  debugPrint("onTapDown");

                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => FullscreenNewsletter(documentLink: widget.documentLink,),
                  ));
                },
                child: PDFViewer(
//            showIndicator: true,
                  showPicker: false,
//            showNavigation: false,

                  document: doc,
                ),
              ),)
            ],
          )
        ),
      ),
    );
  }
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: Text(widget.title),
//        centerTitle: true,
//      ),
//      body: WebView(
//        initialUrl: 'https://docs.google.com/gview?embedded=true&url=${widget.documentLink}'/*widget.documentLink*//*'https://flutter.dev'*/,//"https://firebasestorage.googleapis.com/v0/b/refhq-f57bf.appspot.com/o/newsletter%2FGSSSB%20BIN%20SACHIVALAY%20CLERK%2017-11-2019.pdf?alt=media&token=09ad4932-0f4b-4f37-972c-96445a0c56ef"/*"http://www.pdf995.com/samples/pdf.pdf"*/ /*"https://firebasestorage.googleapis.com/v0/b/refhq-f57bf.appspot.com/o/newsletter%2FGSSSB%20BIN%20SACHIVALAY%20CLERK%2017-11-2019.pdf?alt=media&token=09ad4932-0f4b-4f37-972c-96445a0c56ef"*//*widget.documentLink*/,
//        initialUrl: widget.documentLink,
//        javascriptMode: JavascriptMode.unrestricted,
//        onWebViewCreated: (WebViewController webViewController) {
//          _controller.complete(webViewController);
//        },
//      ),
//    );
//  }

}

import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ref_hq/constants.dart' as constant;
import 'package:ref_hq/home/home_screen.dart';
import 'package:ref_hq/home/quiz_questions.dart';
import 'package:ref_hq/ref_theme.dart';
import 'package:ref_hq/widget/display_sports_ruleset_view.dart';
import 'package:ref_hq/widget/status_bar_height.dart';

import '../always_visible_scrollbar.dart';
import '../constants.dart';
import '../data_models/question.dart';
import '../data_models/user_model.dart';

class RulewiseQuestionLayout extends StatefulWidget {
  List<Question> questions;
  final QuizType quizType;
  final UserModel user;
  final sport;
  final ruleset;
  final title;
  RulewiseQuestionLayout({Key key,this.title, this.questions, this.quizType, this.user,this.sport,this.ruleset})
      : super(key: key);

  @override
  _RulewiseQuestionLayoutState createState() => _RulewiseQuestionLayoutState();
}

class _RulewiseQuestionLayoutState extends State<RulewiseQuestionLayout> {
  Question currentQuestion;
  String correctAnswer;
  Color button1Color, button2Color, button3Color, button4Color;
  int maxNumber, index = 0;
  // int streak;
  DatabaseReference ref = FirebaseDatabase.instance.reference();
  ScrollController _scrollController = ScrollController();

  List<Question> resetQuestions = List();
  @override
  Widget build(BuildContext context) {
    /*if (streak == null) {
      streak = int.parse(widget.user.currentStreak);
    }*/
    if (widget.quizType == QuizType.Flagged && currentQuestion == null) {
      return Center(
        child: Text('User has not flagged any questions yet!'),
      );
    } else {
      return SafeArea(
        top: false,
        bottom: false,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              StatusBarHeight(),
              Row(
                children: [
                  GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: SizedBox(
                          width: 55,
                          height: 55,
                          child: Icon(Icons.arrow_back_ios))),
                  Expanded(
                    child: Text(widget.title,textAlign: TextAlign.center,style: TextStyle(
                        fontSize: 20
                    ),),
                  ),SizedBox(
                    width: 55,
                    height: 55,)
                ],
              ),
              DisplaySportsRuleSetView(
                mapSport: widget.sport,
                mapRuleSet: widget.ruleset,
              ),
              ///This is Streak and Question Number
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    (widget.quizType == QuizType.All ||
                            widget.quizType == QuizType.Flagged)
                        ? AutoSizeText(
                            '${index + 1}/$maxNumber',
                            style: Theme.of(context).textTheme.subtitle.merge(
                                  TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                          )
                        : SizedBox(),
                    (widget.quizType == QuizType.All)
                        ? AutoSizeText(
                            'Streak: $streak',
                            style: Theme.of(context).textTheme.subtitle.merge(
                                  TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                          )
                        : SizedBox(),
                  ],
                ),
              ),

              ///These are the Question and Answers.
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 20,bottom: 20),
                  child: Scrollbar(
                    child: SingleChildScrollView(
                      controller: _scrollController,
                      padding: EdgeInsets.all(8),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Text(
                            "${currentQuestion.question}",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 18),
                            overflow: TextOverflow.visible,
                          ),
                          SizedBox(height: 20,),
                          Scrollbar(
                            child: Column(
                              children: <Widget>[
                                (currentQuestion.answersList.length > 0)
                                    ? RaisedButton(
                                        materialTapTargetSize:
                                            MaterialTapTargetSize.shrinkWrap,
                                        color: (currentQuestion.viewMode ==
                                                ViewMode.Quiz)
                                            ? null
                                            : button1Color,
                                        padding:EdgeInsets.fromLTRB(16, 10, 16, 10),
                                        child: ConstrainedBox(
                                          constraints: BoxConstraints(
                                              minWidth: MediaQuery.of(context)
                                                  .size
                                                  .width),
                                          child: Wrap(
                                            direction: Axis.horizontal,
                                            children: [
                                              Text(
                                                currentQuestion.answersList[0],
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 16
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        onPressed: () {
                                          if (currentQuestion.viewMode ==
                                              ViewMode.Quiz) {
                                            selectedAnswer(
                                                chosenAnswer: currentQuestion
                                                    .answersList[0],
                                                answerNumber:
                                                    SelectedAnswer.Answer1,
                                                viewMode: ViewMode.Selected);
                                          }
                                        },
                                      )
                                    : SizedBox(height: 0),
                                SizedBox(height: 16),
                                (currentQuestion.answersList.length > 1)
                                    ? RaisedButton(
                                        materialTapTargetSize:
                                            MaterialTapTargetSize.shrinkWrap,
                                        color: (currentQuestion.viewMode ==
                                                ViewMode.Quiz)
                                            ? null
                                            : button2Color,
                                        padding:
                                            EdgeInsets.fromLTRB(16, 10, 16, 10),
                                        child: ConstrainedBox(
                                          constraints: BoxConstraints(
                                              minWidth: MediaQuery.of(context)
                                                  .size
                                                  .width),
                                          child: Wrap(
                                            direction: Axis.horizontal,
                                            children: [
                                              Text(
                                                currentQuestion.answersList[1],
                                                style: TextStyle(
                                                    fontSize: 16
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ],
                                          ),
                                        ),
                                        onPressed: () {
                                          if (currentQuestion.viewMode ==
                                              ViewMode.Quiz) {
                                            selectedAnswer(
                                                chosenAnswer: currentQuestion
                                                    .answersList[1],
                                                answerNumber:
                                                    SelectedAnswer.Answer2,
                                                viewMode: ViewMode.Selected);
                                          }
                                        },
                                      )
                                    : SizedBox(height: 0),
                                SizedBox(height: 16),
                                (currentQuestion.answersList.length > 2)
                                    ? RaisedButton(
                                        materialTapTargetSize:
                                            MaterialTapTargetSize.shrinkWrap,
                                        color: (currentQuestion.viewMode ==
                                                ViewMode.Quiz)
                                            ? null
                                            : button3Color,
                                        padding:
                                            EdgeInsets.fromLTRB(16, 10, 16, 10),
                                        child: ConstrainedBox(
                                          constraints: BoxConstraints(
                                              minWidth: MediaQuery.of(context)
                                                  .size
                                                  .width),
                                          child: Wrap(
                                            direction: Axis.horizontal,
                                            children: [
                                              Text(
                                                currentQuestion.answersList[2],
                                                style: TextStyle(
                                                    fontSize: 16
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ],
                                          ),
                                        ),
                                        onPressed: () {
                                          if (currentQuestion.viewMode ==
                                              ViewMode.Quiz) {
                                            selectedAnswer(
                                                chosenAnswer: currentQuestion
                                                    .answersList[2],
                                                answerNumber:
                                                    SelectedAnswer.Answer3,
                                                viewMode: ViewMode.Selected);
                                          }
                                        },
                                      )
                                    : SizedBox(height: 0),
                                SizedBox(height: 16),
                                (currentQuestion.answersList.length > 3)
                                    ? RaisedButton(
                                        materialTapTargetSize:
                                            MaterialTapTargetSize.shrinkWrap,
                                        color: (currentQuestion.viewMode ==
                                                ViewMode.Quiz)
                                            ? null
                                            : button4Color,
                                        padding:
                                            EdgeInsets.fromLTRB(16, 10, 16, 10),
                                        child: ConstrainedBox(
                                          constraints: BoxConstraints(
                                              minWidth: MediaQuery.of(context)
                                                  .size
                                                  .width),
                                          child: Wrap(
                                            direction: Axis.horizontal,
                                            children: [
                                              Text(
                                                currentQuestion.answersList[3],
                                                style: TextStyle(
                                                    fontSize: 16
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ],
                                          ),
                                        ),
                                        onPressed: () {
                                          if (currentQuestion.viewMode ==
                                              ViewMode.Quiz) {
                                            selectedAnswer(
                                                chosenAnswer: currentQuestion
                                                    .answersList[3],
                                                answerNumber:
                                                    SelectedAnswer.Answer4,
                                                viewMode: ViewMode.Selected);
                                          }
                                        },
                                      )
                                    : SizedBox(height: 0),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),

              /// These are navigation, next question, and Flag Question buttons.
              Container(
                height: 80,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    ///These are navigation buttons
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        ConstrainedBox(
                          constraints: BoxConstraints(maxWidth: 50),
                          child: IconButton(
                            padding: EdgeInsets.all(0),
                            alignment: Alignment.center,
                            icon: Icon(Icons.chevron_left),
                            iconSize: 48,
                            onPressed: (index <= 0)
                                ? null
                                : () => _goToPreviousQuestion(),
                          ),
                        ),
                        ConstrainedBox(
                          constraints: BoxConstraints(maxWidth: 40),
                          child: IconButton(
                              padding: EdgeInsets.all(0),
                              alignment: Alignment.center,
                              icon: Icon(Icons.chevron_right),
                              iconSize: 48,
                              onPressed: (index !=
                                          widget.questions.length - 1 &&
                                      currentQuestion.viewMode !=
                                          ViewMode.Selected)
                                  ? (currentQuestion.viewMode != ViewMode.Quiz)
                                      ? () => _goToNextQuestion()
                                      : null
                                  : null),
                        ),
                      ],
                    ),

                    ///These is Next Question Button and Rule Text Widget
                    Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(height: 4),
                        (currentQuestion.viewMode != ViewMode.Quiz)
                            ? AutoSizeText(
                                (currentQuestion.rule == '-')
                                    ? ""
                                    : "Rule: ${currentQuestion.rule}",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.italic),
                                minFontSize: 5,
                              )
                            : SizedBox(),
                        SizedBox(height: 8),
                        (index != widget.questions.length - 1)
                            ? (currentQuestion.viewMode ==
                                    ViewMode.Selected)
                                ? Container(
                                  height: 40,
                                  child: RaisedButton(
                                      onPressed: () {
                                        if(index == widget.questions.length - 1){
                                          debugPrint("last Question");
                                          _goToFirstQuestion();
                                        }
                                        else {
                                          _goToNextQuestion();
                                        }
                                      },
                                      child: Text('Next Question')),
                                )
                                : SizedBox(height: 40)
                            : SizedBox(height: 40),
                        SizedBox(height: 4),
                      ],
                    ),

                    ///This is Flag question Icon and Text Widget.
                    Container(
                        alignment: Alignment.bottomCenter,
                      child: (widget.quizType == QuizType.Flagged ||
                          currentQuestion.viewMode == ViewMode.ReviewFlagged)
                          ? _removeFlagQuestion()
                          : _flagQuestion(),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    resetQuestions = widget.questions;
    if (widget.questions.length != 0) {
      if (widget.questions[0].viewMode == ViewMode.ReviewFlagged) {
        _setUpQuestionForReview();
      } else
        _setUpQuestion();
    }
  }

  void addAnswersToListAndShuffle() {
    currentQuestion.answersList.clear();
    if (currentQuestion.answer1 != "null" && currentQuestion.answer1 != "-")
      currentQuestion.answersList.add(currentQuestion.answer1);
    if (currentQuestion.answer2 != "null" && currentQuestion.answer2 != "-")
      currentQuestion.answersList.add(currentQuestion.answer2);
    if (currentQuestion.answer3 != "null" && currentQuestion.answer3 != "-")
      currentQuestion.answersList.add(currentQuestion.answer3);
    if (currentQuestion.answer4 != "null" && currentQuestion.answer4 != "-")
      currentQuestion.answersList.add(currentQuestion.answer4);
    randomizeAnswerList();
  }

  void randomizeAnswerList() {
    //Randomizes only when the flag is found to be 1.
    if (currentQuestion.flag == 1) currentQuestion.answersList.shuffle();
  }

  String determineCorrectAnswer() {
    switch (currentQuestion.correct) {
      case 1:
        return currentQuestion.answer1;
        break;
      case 2:
        return currentQuestion.answer2;
        break;
      case 3:
        return currentQuestion.answer3;
        break;
      case 4:
        return currentQuestion.answer4;
        break;
      default:
        return null;
    }
  }

  void selectedAnswer(
      {@required String chosenAnswer,
      @required SelectedAnswer answerNumber,
      ViewMode viewMode}) {
    currentQuestion.selectedAnswerString = chosenAnswer;
    currentQuestion.selectedAnswer = answerNumber;
    bool isAnswerCorrect = chosenAnswer == correctAnswer;
    if (viewMode != null) {
      currentQuestion.viewMode = viewMode;
      // if (widget.quizType == QuizType.All) {
        // calculateCurrentStreak(isAnswerCorrect);
      // }
    }
    var newButtonColor =
        (isAnswerCorrect) ? correctAnswerColor : Theme.of(context).accentColor;
    //If answer correct, change button to green else red.

    switch (answerNumber) {
      case SelectedAnswer.Answer1:
        button1Color = newButtonColor;
        break;
      case SelectedAnswer.Answer2:
        button2Color = newButtonColor;
        break;
      case SelectedAnswer.Answer3:
        button3Color = newButtonColor;
        break;
      case SelectedAnswer.Answer4:
        button4Color = newButtonColor;
        break;
    }
    markTheCorrectAnswer(chosenAnswer);
    setState(() {});
  }

  void markTheCorrectAnswer(String chosenAnswer) {
    int correctAnswerIndexPlus1;
    for (int i = 0; i < currentQuestion.answersList.length; i++) {
      if (currentQuestion.answersList[i] == correctAnswer) {
        correctAnswerIndexPlus1 = i + 1;
      }
    }
    var newButtonColor = correctAnswerColor;
    switch (correctAnswerIndexPlus1) {
      case 1:
        button1Color = newButtonColor;
        break;
      case 2:
        button2Color = newButtonColor;
        break;
      case 3:
        button3Color = newButtonColor;
        break;
      case 4:
        button4Color = newButtonColor;
        break;
    }
  }

  _goToPreviousQuestion() {
    setState(() {
      if (index > 0) index -= 1;
      (currentQuestion.viewMode == ViewMode.ReviewFlagged)
          ? _setUpQuestionForReview()
          : _setUpQuestion();
    });
  }

  _goToNextQuestion() {
    setState(() {
      if (index < (maxNumber - 1)) index += 1;
      (currentQuestion.viewMode == ViewMode.ReviewFlagged)
          ? _setUpQuestionForReview()
          : _setUpQuestion();
    });
  }
  _goToFirstQuestion() {
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => HomeScreen()), (route) => false);
    /*Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (context) => RulewiseQuestionLayout(
        title: title,
        ruleset: widget.ruleset,
        sport: widget.sport,
        questions: widget.questions,
        quizType: widget.quizType,
        user: widget.user,
      )),
    );*/
    /*QuizQuestion quizQuestionRoute = QuizQuestion(
      sport: widget.sport,
      ruleset: widget.ruleset,
      title: title *//*'QUIZ ALL QUESTIONS'*//*,
      questions: widget.,
      quizType: QuizType.Rulewise,
      user: currentUser,
    );

    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (context) => quizQuestionRoute),
    );*/
    /*Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => QuizQuestion(
          sport: widget.sport,
          ruleset: widget.ruleset,
          title: *//*_selectedItemSport.name*//*'Flagged Questions',
          questions: widget.questions,
          quizType: QuizType.Rulewise,
          user: widget.user,
        ),
      ),
    );*/
  /*  RulewiseQuestionLayout(
      title: title,
      ruleset: widget.ruleset,
      sport: widget.sport,
      questions: widget.questions,
      quizType: widget.quizType,
      user: widget.user,
    );*/
    /*setState(() {
      index = 0;
      widget.questions = resetQuestions;
      _resetButtonColorValues();
      maxNumber = widget.questions.length;
      currentQuestion = widget.questions[index];

      if (_scrollController.hasClients)
        _scrollController.animateTo(0,
            duration: Duration(milliseconds: 100), curve: Curves.linear);
    });*/
  }

  void _setUpQuestionForReview() {
    _resetButtonColorValues();
    maxNumber = widget.questions.length;
    currentQuestion = widget.questions[index];
    if (currentQuestion.answersList.length == 0) addAnswersToListAndShuffle();
    correctAnswer = determineCorrectAnswer();
    markTheCorrectAnswer(correctAnswer);
    if (_scrollController.hasClients)
      _scrollController.animateTo(0,
          duration: Duration(milliseconds: 100), curve: Curves.linear);
  }

  void _setUpQuestion() {
    _resetButtonColorValues();
    maxNumber = widget.questions.length;
    currentQuestion = widget.questions[index];
    if (currentQuestion.answersList.length == 0) addAnswersToListAndShuffle();
    correctAnswer = determineCorrectAnswer();
    //Lock the question in review mode so that the answers don't shuffle anymore.
    currentQuestion.viewMode = (currentQuestion.selectedAnswer == null)
        ? ViewMode.Quiz
        : ViewMode.Review;
    if (currentQuestion.viewMode == ViewMode.Review) {
      selectedAnswer(
          chosenAnswer: currentQuestion.selectedAnswerString,
          answerNumber: currentQuestion.selectedAnswer);
    }
    if (_scrollController.hasClients)
      _scrollController.animateTo(0,
          duration: Duration(milliseconds: 100), curve: Curves.linear);
  }

  void _resetButtonColorValues() {
    button1Color = null;
    button2Color = null;
    button3Color = null;
    button4Color = null;
  }

  /*void calculateCurrentStreak(bool isAnswerCorrect) async {
    //If correct answer, mark wasPreviousQuestionCorrect as True
    if (isAnswerCorrect)
      streak += 1;
    else
      streak = 0;

    await ref
        .child(users)
        .child(widget.user.key)
        .update({currentStreak: '$streak'});

    if (streak > widget.user.streak) {
      widget.user.streak = streak;
      await ref
          .child(users)
          .child(widget.user.key)
          .update({constant.streak: '$streak'});
    }
  }*/

  void _flagThisQuestion(String key) async {
    String childName = '${widget.sport['name']} - ${widget.ruleset['name']}';
    setState(() {
      currentQuestion.flagType = FlagType.AttemptingFlag;
    });
    var questionString = currentQuestion.toJson();
    try {
      await ref.child(users).child(widget.user.key).child(savedQuestions).child(childName).push().set(questionString);
    } catch (e) {
      setState(() {
        currentQuestion.flagType = FlagType.UnFlagged;
      });
    }
    setState(() {
      currentQuestion.flagType = FlagType.Flagged;
    });
  }

  _flagQuestion() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: (currentQuestion.flagType == FlagType.UnFlagged)
              ? Icon(Icons.outlined_flag)
              : (currentQuestion.flagType == FlagType.AttemptingFlag)
                  ? Icon(Icons.flag)
                  : Icon(
                      Icons.flag,
                      color: Theme.of(context).accentColor,
                    ),
          iconSize: 24,
          padding: EdgeInsets.all(0),
          onPressed: (currentQuestion.flagType == FlagType.UnFlagged)
              ? () {
                  _flagThisQuestion(widget.user.key);
                }
              : null,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: Text('Flag Question', style: TextStyle(fontSize: 12)),
        ),
      ],
    );
  }

  _removeFlagQuestion() {
    return Column(
      children: <Widget>[
        IconButton(
            icon: Icon(
              Icons.remove_circle,
              color: Colors.red,
            ),
            iconSize: 24,
            padding: EdgeInsets.all(0),
            onPressed: () {
              _removeThisQuestion(widget.user.key);
            }),
        Padding(
          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: Text('Remove Flag', style: TextStyle(fontSize: 12)),
        ),
      ],
    );
  }

  void _removeThisQuestion(String key) async {
    String childName = '${widget.sport['name']} - ${widget.ruleset['name']}';
    debugPrint("path : ${users} ${widget.user.key} ${constant.savedQuestions} ${childName} ${currentQuestion.key}");
    await ref.child(users).child(widget.user.key).child(savedQuestions).child(childName).child(currentQuestion.key).set(null);
    widget.questions.removeAt(index);
    maxNumber = widget.questions.length;
    if (widget.questions.length == 0) {
      Navigator.pop(context);
    } else if ((widget.questions.length) == index) {
      index -= 1;
      setState(() {
        (currentQuestion.viewMode == ViewMode.ReviewFlagged)
            ? _setUpQuestionForReview()
            : _setUpQuestion();
      });
    } else {
      setState(() {
        (currentQuestion.viewMode == ViewMode.ReviewFlagged)
            ? _setUpQuestionForReview()
            : _setUpQuestion();
      });
    }
  }
}

import 'package:carousel_slider/carousel_slider.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:photo_view/photo_view.dart';
import 'package:ref_hq/utils/my_toast.dart';


class FullScreenViewFromNetwork extends StatefulWidget {
  List<dynamic> listSlides;
  int index=0;
  FullScreenViewFromNetwork({this.listSlides,this.index});
  @override
  _FullScreenViewFromNetworkState createState() => _FullScreenViewFromNetworkState();
}

class _FullScreenViewFromNetworkState extends State<FullScreenViewFromNetwork> {
  ScrollController mScrollController = ScrollController();
  CarouselController buttonCarouselController = CarouselController();
  @override
  void initState() {
    FirebaseAnalytics().setAnalyticsCollectionEnabled(true);
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.initState();
    debugPrint("Slides : ${widget.listSlides}");
  }

  @override
  void dispose() {
    // TODO: implement dispose
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    // SystemChrome.setEnabledSystemUIOverlays([]);
    // SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    return Material(
      child: Container(
        color: Colors.black,
        constraints: BoxConstraints(
          minHeight: MediaQuery.of(context).size.height,
          minWidth: MediaQuery.of(context).size.width,
        ),
        child: Stack(
          children: [
            CarouselSlider(
              carouselController: buttonCarouselController,
              options: CarouselOptions(
                /*autoPlay: false,
                enlargeCenterPage: true,
                viewportFraction: 1,
                aspectRatio: 1.0,
                initialPage: widget.index,
                height: MediaQuery.of(context).size.height,
                enableInfiniteScroll: false,*/
                initialPage: widget.index,
                autoPlay: false,
                height: MediaQuery.of(context).size.height,
                viewportFraction: 1,
                aspectRatio: 1.0,
                enlargeCenterPage: true,
                enableInfiniteScroll: false,
                enlargeStrategy: CenterPageEnlargeStrategy.height,

              ),
              // aspectRatio: 2.0,
              items: widget.listSlides.asMap().entries.map((entries) {
                int index = entries.key;
                return Builder(
                  builder: (context) {
                    return Container(
                      color: Colors.black,
                      constraints: BoxConstraints(
                        minHeight: MediaQuery.of(context).size.height,
                        minWidth: MediaQuery.of(context).size.width,
                      ),
                      child: Stack(
                        children: [
                          Container(
                              constraints: BoxConstraints(
                                minHeight: MediaQuery.of(context).size.height,
                                minWidth: MediaQuery.of(context).size.width,
                              ),
                              child: Center(child: CircularProgressIndicator())
                          ),
                          Container(
                            constraints: BoxConstraints(
                              minHeight: MediaQuery.of(context).size.height,
                              minWidth: MediaQuery.of(context).size.width,
                            ),
                            child: PhotoView(
                              imageProvider: NetworkImage(entries.value)/*FancyShimmerImage(
                                  imageUrl: entries.value['url'],
                                  shimmerBaseColor: Colors.white,
                                  shimmerHighlightColor:Colors.grey,
                                  // config.Colors().mainColor(1),
                                  shimmerBackColor: Colors.green,
                                  boxFit: BoxFit.cover,
                                )*/,
                            ),/*Image.network(entries.value)*/
                          ),
                        ],
                      ),
                    );
                  },
                );
              }).toList()
              ,

            ),
            Container(
              margin: EdgeInsets.only(top: AppBar().preferredSize.height),
              height: 50,
              child: Row(
                children: [
                  Expanded(
                    child: Container(),
                  ),
                  Container(
                    height: 50,
                    width: 50,
                    padding: EdgeInsets.only(right: 10),
                    child: IconButton(
                      icon: Icon(Icons.close,size: 35,),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

}

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:package_info/package_info.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:ref_hq/constants.dart';
import 'package:ref_hq/consumable_store.dart';
import 'package:ref_hq/data_models/ruleset.dart';
import 'package:ref_hq/data_models/sports.dart';
import 'package:ref_hq/data_models/subscription_type.dart';
import 'package:ref_hq/data_models/user_model.dart';
import 'package:ref_hq/data_models/user_subscription.dart';
import 'package:ref_hq/home/all_question_layout.dart';
import 'package:ref_hq/home/choose_quiz.dart';
import 'package:ref_hq/home/quiz_questions.dart';
import 'package:ref_hq/home/settings.dart';
import 'package:ref_hq/misc_widgets.dart';
import 'package:ref_hq/newsletter/newsletter_year_screen.dart';
import 'package:ref_hq/pregame/pregame_screen.dart';
import 'package:ref_hq/ref_theme.dart';
import 'package:ref_hq/state_container/state_container.dart';
import 'package:ref_hq/utils/app_preferences.dart';
import 'package:ref_hq/utils/common_constant.dart';
import 'package:ref_hq/utils/email_sender.dart';
import 'package:ref_hq/utils/my_colors.dart';
import 'package:ref_hq/utils/my_log.dart';
import 'package:ref_hq/utils/my_toast.dart';
import 'package:ref_hq/utils/utility.dart';
import 'package:url_launcher/url_launcher.dart';

import '../auth.dart';
import '../data_models/question.dart';

class MembershipScreen extends StatefulWidget {

  List<Question> flaggedQuestions;
  MembershipScreen({this.flaggedQuestions});
  @override
  _MembershipScreenState createState() => _MembershipScreenState();
}

class _MembershipScreenState extends State<MembershipScreen> {
  /* inappsection*/

  bool isTapped = false;

  // final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final InAppPurchaseConnection _connection = InAppPurchaseConnection.instance;
  StreamSubscription<List<PurchaseDetails>> _subscription;

  // ignore: unused_field
  List<String> _notFoundIds = [];
  List<ProductDetails> _products = [];
  List<PurchaseDetails> _purchases = [];

  // ignore: unused_field
  List<String> _consumables = [];

  // ignore: unused_field
  bool _isAvailable = false;

  // ignore: unused_field
  bool _purchasePending = false;
  bool _loading = true;
  bool _isLoadingSportsAndRuleSet = true;

  // ignore: unused_field
  String _queryProductError;

  /*-----*/

  // Map<String, dynamic> subscriptionData;


  List<DropdownMenuItem<Sports>> _dropdownMenuItemsSport;
  Sports _selectedItemSport;
  Sports _oldSelectedSport;
  List<Sports> listDropdownItemsSport = List();

  bool showIAABlogo = false;
  bool showNewsLetter = false;
  List<DropdownMenuItem<Ruleset>> _dropdownMenuItemsRuleSet;
  Ruleset _selectedItemRuleSet;
  Ruleset _oldSelectedRuleset;
  List<Ruleset> listDropdownItemsRuleSet = List();
  double dropdownRuleSetItemHeight = 50.0;

  Map<dynamic, dynamic> mapSport = Map();
  Map<dynamic, dynamic> mapRuleSet = Map();

  SubscriptionType subscriptionType = SubscriptionType.NONE;

  bool enableSports = true;
  bool enableRuleSet = true;
  // QuizQuestion quizQuestionRoute;
  ChooseQuiz chooseQuiz;

  /* */
  List<Map<String, dynamic>> listSportRuleSet = List();

  @override
  void initState() {
    FirebaseAnalytics().setAnalyticsCollectionEnabled(true);
    super.initState();

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      /*inappsection*/
      Stream purchaseUpdated = InAppPurchaseConnection.instance.purchaseUpdatedStream;
      _subscription = purchaseUpdated.listen((purchaseDetailsList) {
        MyLog.printInappLog("purchaseUpdatedStream : listen");
        _listenToPurchaseUpdated(purchaseDetailsList,context);
      }, onDone: () {
        MyLog.printInappLog("purchaseUpdatedStream : done");
        _subscription.cancel();
      }, onError: (error) {
        // handle error here.
        MyLog.printInappLog("purchaseUpdatedStream : error");
        displaySnackBarWithKey('onError : ${error.toString()}');
      });

      /*-----*/
    });

    /*-----*/

    initPrefs();
  }

  void initPrefs() async {
    setState(() {
      _isLoadingSportsAndRuleSet = true;
    });

    DatabaseReference ref = FirebaseDatabase.instance.reference();
    String uid = await Auth().currentUser();
    var users = await ref.child(prefix0.users).child(uid).once();
    UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));

    String strSubscriptionType = currentUser.subscriptionType;
    if (strSubscriptionType != null) {
      subscriptionType = UserSubscription.getSubscriptionTypeEnum(strSubscriptionType);
    }
    debugPrint("str Subscription Type : ${strSubscriptionType}");
    debugPrint("Enum Subscription Type : ${subscriptionType}");

    listDropdownItemsSport = StateContainer.of(context).sports;
    _dropdownMenuItemsSport = buildDropDownMenuItemsSports(listDropdownItemsSport);

    listDropdownItemsRuleSet = StateContainer.of(context).ruleSet;
    _dropdownMenuItemsRuleSet = buildDropDownMenuItemsRuleSet(listDropdownItemsRuleSet);

    // prefSports = prefs.getString(AppPreferences.pref_sports);
    // mapSport = json.decode(prefSports);
    mapSport = currentUser.sports;
    if (mapSport != null) {
      List<DropdownMenuItem<Sports>> mList = _dropdownMenuItemsSport.where((element) => element.value.name == mapSport['name'] && element.value.value == mapSport['value']).toList();
      if (mList.length > 0) {
        _selectedItemSport = mList[0].value;
        _oldSelectedSport = _selectedItemSport;
        // selectedDialogSport = _selectedItemSport;

      } else {
        _selectedItemSport = _dropdownMenuItemsSport[1].value;
        _oldSelectedSport = _selectedItemSport;
      }
    } else {
      _selectedItemSport = _dropdownMenuItemsSport[0].value;
      _oldSelectedSport = _selectedItemSport;
    }

    // prefRuleSet = prefs.getString(AppPreferences.pref_rule_set);
    // mapRuleSet = json.decode(prefRuleSet);
    mapRuleSet = currentUser.ruleset;
    if (mapRuleSet != null) {
      filterRuleSet();
      List<DropdownMenuItem<Ruleset>> mList = _dropdownMenuItemsRuleSet.where((element) => element.value.name == mapRuleSet['name'] && element.value.value == mapRuleSet['value'] && element.value.sports == mapRuleSet['sports']).toList();

      if (mList.length > 0) {
        _selectedItemRuleSet = mList[0].value;
        _oldSelectedRuleset = _selectedItemRuleSet;
        // selectedDialogRuleset = _selectedItemRuleSet;
      } else {
        _selectedItemRuleSet = _dropdownMenuItemsRuleSet[1].value;
        _oldSelectedRuleset = _selectedItemRuleSet;
      }
    } else {
      _selectedItemRuleSet = _dropdownMenuItemsRuleSet[0].value;
      _oldSelectedRuleset = _selectedItemRuleSet;
    }

    checkForIAABLogo();
    checkForNewsLetterButton();

    listDropdownItemsSport.forEach((eleSport) {
      debugPrint("${eleSport.value}, ${eleSport.name}");
      if (eleSport.enable && eleSport.value != -1 && eleSport.value != -2) {
        Map<String, dynamic> mapTmpSport = Map();
        mapTmpSport['type'] = prefix0.sports;
        mapTmpSport['data'] = eleSport;

        debugPrint("Sport : ${eleSport.name}");
        listSportRuleSet.add(mapTmpSport);

        listDropdownItemsRuleSet.forEach((eleRuleSet) {
          if (eleSport.value == eleRuleSet.sports && eleRuleSet.enable && eleRuleSet.value != -1 && eleRuleSet.value != -2) {
            Map<String, dynamic> mapTmpRuleSet = Map();
            mapTmpRuleSet['type'] = prefix0.ruleset;
            mapTmpRuleSet['data'] = eleRuleSet;
            mapTmpRuleSet[prefix0.sports] = eleSport;
            debugPrint("Ruleset : ${eleRuleSet.name}");
            listSportRuleSet.add(mapTmpRuleSet);
          }
        });
      }
    });
    await initStoreInfo();
    await activeSubscription(context);
    _isLoadingSportsAndRuleSet = false;
    _loading = false;
    setState(() {});
  }

  void checkForNewsLetterButton() {
    showNewsLetter = false;

    if (_selectedItemSport.value == prefix0.VALUE_BASKETBALL) {
      showNewsLetter = true;
    } else {
      showNewsLetter = false;
    }
  }

  void checkForIAABLogo() {
    bool isSportBasketBall = false;
    if (_selectedItemSport.value == prefix0.VALUE_BASKETBALL) {
      isSportBasketBall = true;
    }
    bool isRuleSetNFHC = false;
    if (_selectedItemRuleSet.value == prefix0.VALUE_NFHC_2020_21) {
      isRuleSetNFHC = true;
    }
    showIAABlogo = (isSportBasketBall && isRuleSetNFHC) ? true : false;
    if (_selectedItemRuleSet.name == "NFHS (2020-21)") {
      dropdownRuleSetItemHeight = 50;
      /*dropdownRuleSetItemHeight = 65;*/
    } else {
      dropdownRuleSetItemHeight = 50;
    }
  }

  List<DropdownMenuItem<Sports>> buildDropDownMenuItemsSports(List listItems) {
    List<DropdownMenuItem<Sports>> items = List();
    for (Sports listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: Text(
                listItem.name,
                style: TextStyle(fontStyle: (listItem.name == prefix0.str_request_another_sport) ? FontStyle.italic : FontStyle.normal, color: Colors.white, fontSize: 17, decoration: listItem.name == prefix0.str_choose_sport ? TextDecoration.underline : TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }

  BoxDecoration linearBoxDecoration() {
    return BoxDecoration(
        gradient: LinearGradient(begin: Alignment.topRight, end: Alignment.bottomLeft, colors: [MyColors.color1, MyColors.color2, MyColors.color3]),
        shape: BoxShape.rectangle,
        // border: Border.all(width: 3, color: Colors.black),
        borderRadius: BorderRadius.circular(10),
        color: MyColors.dropdownBackgroudColor);
  }

  BoxDecoration solidBoxDecoration() {
    return BoxDecoration(
        gradient: LinearGradient(begin: Alignment.topRight, end: Alignment.bottomLeft, colors: [Colors.grey, Colors.grey, Colors.grey]),
        shape: BoxShape.rectangle,
        // border: Border.all(width: 3, color: Colors.black),
        borderRadius: BorderRadius.circular(10),
        color: Colors.red);
  }

  Widget selectedCheckCircle() {
    return Container(
        height: 35,
        width: 35,
        decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: Colors.black, width: CommonConstant.borderDropdown), color: Colors.white),
        child: Image.asset('assets/icon/icon_checked.png') //Icon(Icons.check_circle,size: 30,color: MyColors.colorConvert('#437C1D'),)
        );
  }

  Widget unSelectedCheckCircle() {
    return Container(
      width: 35,
      height: 35,
      decoration: BoxDecoration(
          gradient: new RadialGradient(
            colors: [MyColors.colorConvert('#A1A1A1'), MyColors.colorConvert('#C3C3C3'), MyColors.colorConvert('#F7F7F7')],
          ),
          shape: BoxShape.circle,
          border: Border.all(color: Colors.black, width: CommonConstant.borderDropdown - 0.5),
          color: Colors.grey),
    );
  }

  List<DropdownMenuItem<Ruleset>> buildDropDownMenuItemsRuleSet(List listItems) {
    List<DropdownMenuItem<Ruleset>> items = List();
    for (Ruleset listItem in listItems) {
      debugPrint("${listItem.name} ${listItem.value}");
      /*if (listItem.name == "NFHS (2020-21)") {
        items.add(
          DropdownMenuItem(
            child: Container(
              child: Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  child: Column(
                    children: [
                      Container(
                        // color: Colors.grey,
                        child: Text(
                          listItem.name,
                          style: TextStyle(color: Colors.white, fontSize: 18 */ /*, decoration: TextDecoration.underline*/ /*),
                        ),
                      ),
                      Container(width: 70, alignment: Alignment.center, margin: EdgeInsets.only(top: 5), child: Image.asset('assets/images/iaab_logo.png'))
                    ],
                  ),
                ),
              ),
            ),
            value: listItem,
          ),
        );
      } else {*/
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              // color: Colors.grey,
              child: Text(
                listItem.name,
                style: TextStyle(fontStyle: (listItem.name == prefix0.str_request_another_rule_set) ? FontStyle.italic : FontStyle.normal, color: Colors.white, fontSize: 17, decoration: listItem.name == prefix0.str_choose_rule_set ? TextDecoration.underline : TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
      /*}*/
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return (_loading)
        ? Scaffold(
            extendBody: true,
            extendBodyBehindAppBar: true,
            body: SafeArea(
              child: LinearProgressIndicator() /*Container()*/,
            ),
          )
        : isActiveSubscription
            ? chooseQuiz/*quizQuestionRoute*/ /*ChooseQuiz()*/
            : (_isLoadingSportsAndRuleSet)
                ? Scaffold(
                    extendBody: true,
                    extendBodyBehindAppBar: true,
                    body: SafeArea(
                      child: LinearProgressIndicator(),
                    ),
                  )
                : Material(
                    type: MaterialType.transparency,
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(image: AssetImage('assets/images/background.jpg'), fit: BoxFit.fill),
                      ),
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            SafeArea(
                              child: Container(
                                  height: AppBar().preferredSize.height,
                                  padding: EdgeInsets.only(right: 10, left: 0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      ConstrainedBox(
                                        constraints: BoxConstraints(
                                          maxWidth: AppBar().preferredSize.height,
                                          maxHeight: AppBar().preferredSize.height,
                                        ),
                                        child: FlatButton(
                                          child: Icon(Icons.arrow_back_ios),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ),
                                      /*FlatButton(
                                        child: Text(
                                          'Restore',
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        onPressed: () async {
//                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => ChooseQuiz()));
                                          final PackageInfo info = await PackageInfo.fromPlatform();
                                          String packageName = info.packageName;
                                          DatabaseReference ref = FirebaseDatabase.instance.reference();
                                          String uid = await Auth().currentUser();
                                          var users = await ref.child(prefix0.users).child(uid).once();
                                          UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));
                                          // String sku = currentUser.subscriptionType;
                                          String sku = Utility.kProductIds[1];
                                          String launchUrl = "https://play.google.com/store/account/subscriptions?sku=$sku&package=$packageName";
                                          launch(launchUrl);
                                        },
                                      ),*/
                                    ],
                                  )),
                            ),
//              SizedBox(height: 20,),
                            Expanded(
                              child: SingleChildScrollView(
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(top: 0, bottom: 0),
                                      alignment: Alignment.center,
                                      // color: Colors.black,
                                      constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                      child: Container(width: MediaQuery.of(context).size.width * 0.8, child: bottomLogo()),
                                    ),
//                                    SizedBox(
//                                      height: 15,
//                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 20,right: 20),
                                      child: Text(
                                        "Access all quiz features for free with a 7 day trial, which auto renews to one of the subscriptions selected below. This subscription may be canceled anytime.",
                                        style: TextStyle(fontSize: 14),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    /*Text(
                                      "Free for 7 Days",
                                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                                    ),*/
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      "Choose Your Access Pass:",
                                      style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    // Single Rule Set
                                    InkWell(
                                      onTap: () {
                                        // selectionEnum = SelectionEnum.RULESET;
                                        subscriptionType = SubscriptionType.SINGLE_RULESET;
                                        setState(() {});
                                      },
                                      child: Container(
                                        decoration: subscriptionType == SubscriptionType.SINGLE_RULESET ? linearBoxDecoration() : solidBoxDecoration(),
                                        width: 375,
//                        height: 115,

                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            subscriptionType == SubscriptionType.SINGLE_RULESET ? selectedCheckCircle() : unSelectedCheckCircle(),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Column(
                                              children: [
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Container(
                                                  padding: EdgeInsets.only(left: 10, right: 10),
                                                  width: 300,
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Text(
                                                            "\$3.99 ",
                                                            style: TextStyle(fontSize: 14, color: Colors.white),
                                                          ),
                                                          Text(
                                                            "per month - ",
                                                            style: TextStyle(fontSize: 14, color: Colors.white),
                                                          ),
                                                        ],
                                                      ),

                                                      Text(
                                                        "Single Rule Set",
                                                        style: TextStyle(fontSize: 14, color: Colors.white),
                                                      ),

                                                    ],
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                GestureDetector(
                                                  onTap: () {
                                                    _showMyDialog();
                                                  },
                                                  child: Container(
                                                    alignment: Alignment.centerLeft,
                                                    padding: EdgeInsets.only(left: 10, right: 10),
                                                    decoration: BoxDecoration(shape: BoxShape.rectangle, border: Border.all(width: CommonConstant.borderDropdown, color: Colors.grey), borderRadius: BorderRadius.circular(10), color: Colors.grey),
                                                    constraints: BoxConstraints(minHeight: 55, minWidth: 100, maxWidth: 300),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Container(
                                                          // color: Colors.grey,
                                                          child: Text(((_selectedItemSport.value == prefix0.CHOOSE_SPORT && _selectedItemRuleSet.value == prefix0.CHOOSE_RULESET)
                                                            || (_selectedItemSport.value == prefix0.REQUEST_ANOTHER_SPORT || _selectedItemRuleSet.value == prefix0.REQUEST_ANOTHER_RULESET))?
                                                              'Sport - Ruleset' : "${_selectedItemSport.name} - ${_selectedItemRuleSet.name}",
                                                              style: TextStyle(
                                                                  // fontStyle: (listItem.name == prefix0.str_request_another_rule_set)?FontStyle.italic:FontStyle.normal,
                                                                  //   color: MyColors.btnGoTextColor,
                                                                  fontSize: 17 /*,
                                                            decoration: TextDecoration.underline*/
                                                                  )),
                                                        ),
                                                        Icon(
                                                          Icons.expand_more,
                                                          // color: MyColors.btnGoTextColor,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                /*
                                                //TODO Do not remove this code.
                                                listDropdownItemsRuleSet != null
                                                    ? Container(
                                                        width: 300,
                                                        constraints: BoxConstraints(minHeight: _selectedItemRuleSet != null ? _selectedItemRuleSet.name == "NFHS (2020-21)" ? (dropdownRuleSetItemHeight */ /*+ 10*/ /*) : dropdownRuleSetItemHeight : 50),
                                                        padding: EdgeInsets.only(left: 10, right: 10),
                                                        decoration: BoxDecoration(shape: BoxShape.rectangle, border: Border.all(width: CommonConstant.borderDropdown, color: Colors.grey), borderRadius: BorderRadius.circular(10), color: Colors.grey */ /*MyColors.dropdownBackgroudColor*/ /*),
                                                        // height: 50,
                                                        child: DropdownButtonHideUnderline(
                                                          child: DropdownButton(
                                                              itemHeight: dropdownRuleSetItemHeight,
                                                              // dropdownColor: Colors.grey,
                                                              icon: Icon(
                                                                Icons.keyboard_arrow_down,
                                                                color: Colors.white,
                                                                size: 25,
                                                              ),
                                                              value: _selectedItemRuleSet,
                                                              items: _dropdownMenuItemsRuleSet,
                                                              onChanged: (Ruleset value) async {
                                                                setState(() {
                                                                  debugPrint("ruleset ${Ruleset.toJson(value)}");
                                                                  _selectedItemRuleSet = value;
                                                                  if (_selectedItemRuleSet.name == "NFHS (2020-21)") {
                                                                    */ /*dropdownRuleSetItemHeight = 65;*/ /*
                                                                    dropdownRuleSetItemHeight = 50;
                                                                  } else {
                                                                    dropdownRuleSetItemHeight = 50;
                                                                  }
                                                                });
                                                                if (_selectedItemRuleSet.value == prefix0.REQUEST_ANOTHER_RULESET) {
                                                                  await EmailSender.requestAnotherRuleSet(_selectedItemRuleSet);
                                                                }
                                                                setState(() {
                                                                  checkForIAABLogo();
                                                                });
                                                              }),
                                                        ),
                                                      )
                                                    : Container(),

                                                */
                                                SizedBox(
                                                  height: 5,
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    // Single Soprt
                                    InkWell(
                                      onTap: () {
                                        // selectionEnum = SelectionEnum.SPORT;
                                        subscriptionType = SubscriptionType.SINGLE_SPORT;
                                        setState(() {});
                                      },
                                      child: Container(
                                        decoration: subscriptionType == SubscriptionType.SINGLE_SPORT ? linearBoxDecoration() : solidBoxDecoration(),
                                        width: 375,
//                        height: 115,

                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            subscriptionType == SubscriptionType.SINGLE_SPORT ? selectedCheckCircle() : unSelectedCheckCircle(),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Column(
                                              children: [
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Container(
                                                  padding: EdgeInsets.only(left: 10, right: 10),
                                                  width: 300,
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Text(
                                                            "\$5.99 ",
                                                            style: TextStyle(fontSize: 14, color: Colors.white),
                                                          ),
                                                          Text(
                                                            "per month - ",
                                                            style: TextStyle(fontSize: 14, color: Colors.white),
                                                          ),
                                                        ],
                                                      ),
                                                      Text(
                                                        "Single Sport",
                                                        style: TextStyle(fontSize: 14, color: Colors.white),
                                                      ),

                                                    ],
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                _dropdownMenuItemsSport != null
                                                    ? Container(
                                                        width: 300,
                                                        padding: EdgeInsets.only(left: 10, right: 10),
                                                        decoration: BoxDecoration(shape: BoxShape.rectangle, border: Border.all(width: CommonConstant.borderDropdown, color: Colors.grey), borderRadius: BorderRadius.circular(10), color: Colors.grey /*MyColors.dropdownBackgroudColor*/),
                                                        height: 50,
                                                        child: DropdownButtonHideUnderline(
                                                          child: DropdownButton(
                                                              icon: Icon(
                                                                Icons.keyboard_arrow_down,
                                                                color: Colors.white,
                                                                size: 25,
                                                              ),
                                                              value: _selectedItemSport,
                                                              items: _dropdownMenuItemsSport,
                                                              onChanged: (value) async {
                                                                setState(() {
                                                                  debugPrint("Sports ${Sports.toJson(value)}");
                                                                  _selectedItemSport = value;
                                                                });
                                                                if (_selectedItemSport.value == CHOOSE_SPORT) {
                                                                  _dropdownMenuItemsRuleSet = buildDropDownMenuItemsRuleSet(listDropdownItemsRuleSet);
                                                                  _selectedItemRuleSet = _dropdownMenuItemsRuleSet[0].value;
                                                                  return;
                                                                }
                                                                if (_selectedItemSport.value == REQUEST_ANOTHER_SPORT) {
                                                                  await EmailSender.requestAnotherSport(_selectedItemSport);
                                                                  return;
                                                                }

                                                                setState(() {
                                                                  filterRuleSet();
                                                                  checkForIAABLogo();
                                                                  checkForNewsLetterButton();
                                                                });
                                                              }),
                                                        ),
                                                      )
                                                    : Container(),
                                                SizedBox(
                                                  height: 5,
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    // All Access
                                    InkWell(
                                      onTap: () {
                                        subscriptionType = SubscriptionType.ALL_ACCESS;
                                        setState(() {});
                                      },
                                      child: Container(
                                        decoration: subscriptionType == SubscriptionType.ALL_ACCESS ? linearBoxDecoration() : solidBoxDecoration(),
                                        width: 375,
//                        height: 115,

                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            subscriptionType == SubscriptionType.ALL_ACCESS ? selectedCheckCircle() : unSelectedCheckCircle(),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Container(
                                                  padding: EdgeInsets.only(left: 10, right: 10),
                                                  width: 300,
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Text(
                                                            "\$9.99 ",
                                                            style: TextStyle(fontSize: 14, color: Colors.white),
                                                          ),
                                                          Text(
                                                            "per month - ",
                                                            style: TextStyle(fontSize: 14, color: Colors.white),
                                                          ),
                                                        ],
                                                      ),
                                                      Text(
                                                        "All Access",
                                                        style: TextStyle(fontSize: 14, color: Colors.white),
                                                      ),

                                                    ],
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 25,
                                                ),
                                                Container(
                                                  padding: EdgeInsets.only(left: 10, right: 10),
                                                  child: Text("All Sports and Rule Sets", style: TextStyle(fontSize: 17, fontStyle: FontStyle.italic)),
                                                ),
                                                SizedBox(
                                                  height: 15,
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    ConstrainedBox(
                                      constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width * 0.5),
                                      child: RaisedButton(
                                        shape: CommonConstant.roundedRectangleBorder,
                                        color: Theme.of(context).accentColor,
                                        padding: EdgeInsets.all(15),
                                        child: Text(
                                          'Start Free Trial\n& Subscribe',
                                          style: TextStyle(fontSize: 16),
                                          textAlign: TextAlign.center,
                                        ),
                                        onPressed: isTapped
                                            ? null
                                            : () async {
                                                refreshTap(true);
                                                // SubscriptionType mSubscriptionType = SubscriptionType.NONE;
                                                if (subscriptionType == SubscriptionType.SINGLE_RULESET) {
                                                  if (_selectedItemSport.value == CHOOSE_SPORT || _selectedItemSport.value == prefix0.REQUEST_ANOTHER_SPORT) {
                                                    String msg = prefix0.str_choose_sport;
                                                    Fluttertoast.showToast(msg: msg, toastLength: Toast.LENGTH_LONG, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
                                                    refreshTap(false);
                                                    return;
                                                  } else if (_selectedItemRuleSet.value == prefix0.REQUEST_ANOTHER_RULESET) {
                                                    String msg = prefix0.str_choose_rule_set;
                                                    Fluttertoast.showToast(msg: msg, toastLength: Toast.LENGTH_LONG, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
                                                    refreshTap(false);
                                                    return;
                                                  } else {
                                                    subscriptionType = SubscriptionType.SINGLE_RULESET;
                                                  }
                                                } else if (subscriptionType == SubscriptionType.SINGLE_SPORT) {
                                                  if (_selectedItemSport.value == prefix0.CHOOSE_SPORT || _selectedItemSport.value == prefix0.REQUEST_ANOTHER_SPORT) {
                                                    String msg = prefix0.str_choose_sport;
                                                    Fluttertoast.showToast(msg: msg, toastLength: Toast.LENGTH_LONG, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
                                                    refreshTap(false);
                                                    return;
                                                  } else if (_selectedItemRuleSet.value == prefix0.CHOOSE_RULESET || _selectedItemRuleSet.value == prefix0.REQUEST_ANOTHER_RULESET) {
                                                    String msg = prefix0.str_choose_rule_set;
                                                    Fluttertoast.showToast(msg: msg, toastLength: Toast.LENGTH_LONG, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
                                                    refreshTap(false);
                                                    return;
                                                  } else {
                                                    subscriptionType = SubscriptionType.SINGLE_SPORT;
                                                  }
                                                } else if (subscriptionType == SubscriptionType.ALL_ACCESS) {
                                                  /*if (_selectedItemSport.value == prefix0.CHOOSE_SPORT || _selectedItemSport.value == prefix0.REQUEST_ANOTHER_SPORT) {
                                                    String msg = prefix0.str_choose_sport;
                                                    Fluttertoast.showToast(msg: msg, toastLength: Toast.LENGTH_LONG, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
                                                    refreshTap(false);
                                                    return;
                                                  } else if (_selectedItemRuleSet.value == prefix0.CHOOSE_RULESET || _selectedItemRuleSet.value == prefix0.REQUEST_ANOTHER_RULESET) {
                                                    String msg = prefix0.str_choose_rule_set;
                                                    Fluttertoast.showToast(msg: msg, toastLength: Toast.LENGTH_LONG, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
                                                    refreshTap(false);
                                                    return;
                                                  } else {
                                                    subscriptionType = SubscriptionType.ALL_ACCESS;
                                                  }*/

                                                  subscriptionType = SubscriptionType.ALL_ACCESS;
                                                }

                                                if (subscriptionType == SubscriptionType.NONE) {
                                                  Fluttertoast.showToast(msg: "Please select any quiz", toastLength: Toast.LENGTH_LONG, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
                                                  refreshTap(false);
                                                  return;
                                                } else {
                                                  Map<String, dynamic> sport ;
                                                  Map<String, dynamic> ruleSet ;
                                                  if (subscriptionType == SubscriptionType.ALL_ACCESS) {
                                                    sport = Sports.toJson(_oldSelectedSport);
                                                    ruleSet = Ruleset.toJson(_oldSelectedRuleset);
                                                  } else {
                                                    if (subscriptionType == SubscriptionType.SINGLE_SPORT && _selectedItemSport.value == prefix0.REQUEST_ANOTHER_SPORT) {
                                                      Fluttertoast.showToast(msg: prefix0.str_choose_sport, toastLength: Toast.LENGTH_LONG, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
                                                      refreshTap(false);
                                                      return;
                                                    }
                                                    if (subscriptionType == SubscriptionType.SINGLE_RULESET && _selectedItemRuleSet.value == prefix0.REQUEST_ANOTHER_SPORT) {
                                                      Fluttertoast.showToast(msg: "Choose Ruel Set", toastLength: Toast.LENGTH_LONG, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
                                                      refreshTap(false);
                                                      return;
                                                    }
                                                    sport = Sports.toJson(_selectedItemSport);
                                                    ruleSet = Ruleset.toJson(_selectedItemRuleSet);
                                                  }



                                                  debugPrint("asdfa");
                                                  DatabaseReference dbRef = FirebaseDatabase.instance.reference();
                                                  String strSubscriptionType = UserSubscription.getSubscriptionTypeString(subscriptionType);

                                                  var connectivityResult = await (Connectivity().checkConnectivity());
                                                  if (connectivityResult == ConnectivityResult.none) {
                                                    await FirebaseDatabase.instance.goOffline();

                                                    final mUserId = await Auth().currentUser();
                                                    debugPrint("mUserId : ${mUserId}");
                                                    dbRef.child(prefix0.users).child(mUserId).update({
                                                      prefix0.subscriptionType: strSubscriptionType,
                                                      prefix0.sports: sport,
                                                      prefix0.ruleset: ruleSet,
                                                    });
                                                  } else {
                                                    await FirebaseDatabase.instance.goOnline();
                                                    final mUserId = await Auth().currentUser();
                                                    debugPrint("mUserId : ${mUserId}");
                                                    await dbRef.child(prefix0.users).child(mUserId).update({
                                                      prefix0.subscriptionType: strSubscriptionType,
                                                      prefix0.sports: sport,
                                                      prefix0.ruleset: ruleSet,
                                                    });
                                                  }



                                                  SubscriptionType savedSubscriptionType = UserSubscription.getSubscriptionTypeEnum(strSubscriptionType);
                                                  debugPrint("subScriptionType : ${savedSubscriptionType}");

                                                  selProductID = strSubscriptionType;

                                                  DataSnapshot snapshotSlides = await FirebaseDatabase.instance.reference().child(prefix0.rules_new).once();
                                                  List<dynamic> valuesSlides = snapshotSlides.value;
                                                  List<Question> allQuizQuestions = valuesSlides.map((data) => Question.fromJSON(Map<String, dynamic>.from(data))).toList();

                                                  debugPrint("sport : ${sport}");
                                                  debugPrint("ruleset : ${ruleSet}");

                                                  debugPrint("sport --- : ${Sports.toJson(_selectedItemSport)}");
                                                  debugPrint("ruleset --- : ${Ruleset.toJson(_selectedItemRuleSet)}");

                                                  List<Question> temp = allQuizQuestions.where((element) => element.sport == sport[prefix0.value] && element.ruleset == ruleSet[prefix0.value]).toList();
                                                  allQuizQuestions = temp;
                                                  debugPrint("total question : ${allQuizQuestions.length}");
                                                  DatabaseReference ref = FirebaseDatabase.instance.reference();
                                                  String uid = await Auth().currentUser();
                                                  var users = await ref.child(prefix0.users).child(uid).once();
                                                  UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));
                                                  refreshChooseQuizNavigator(flaggedQuestions: widget.flaggedQuestions,
                                                      selectedItemRuleSet: _selectedItemRuleSet,
                                                      selectedItemSport: _selectedItemSport,
                                                      currentUser: currentUser);
                                                  setState(() {
                                                    // _isLoadingSportsAndRuleSet = false;
                                                    isTapped = true;
                                                  });
                                                  allQuizQuestions.shuffle();

                                                  /*Navigator.of(context).pushReplacement(
                                                    MaterialPageRoute(builder: (context) => chooseQuiz),
                                                  );
                                                  return;*/

                                                  List<ProductDetails> selProduct = _products.where((element) => element.id == selProductID).toList();
                                                  if (selProduct.length > 0) {
                                                    final ProductDetails mProdDetails = selProduct[0];
                                                    debugPrint("Sel Product : ${mProdDetails.id}, ${mProdDetails.title}");
                                                    debugPrint("Sel Product : ${mProdDetails.toString()}");

                                                    debugPrint("products : ${_products.length}");
                                                    PurchaseParam purchaseParam;
                                                    if (Platform.isIOS) {
                                                      purchaseParam = PurchaseParam(productDetails: mProdDetails, applicationUserName: null, sandboxTesting: true);
                                                    } else {
                                                      purchaseParam = PurchaseParam(productDetails: mProdDetails, applicationUserName: null);
                                                    }
                                                    _connection.buyNonConsumable(purchaseParam: purchaseParam);
                                                  }
                                                }
                                              },
                                      ),
                                    ),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 10, right: 10),
                                      child: RichText(
                                          textAlign: TextAlign.center,
                                          text: TextSpan(children: <TextSpan>[
                                            TextSpan(
                                                text:
                                                    'This subscription automatically renews through your iTunes account for the monthly price chosen above after the 7 day free trial. You can cancel anytime but must cancel at least 24 hours before the end of the current period to avoid automatically renewing for the next period. \nBy signing up for a free trial, you agree to RefHQ\'s '),
                                            TextSpan(
                                              text: 'Terms of Service',
                                              style: TextStyle(color: Colors.blue),
                                              recognizer: TapGestureRecognizer()
                                                ..onTap = () {
                                                  launch('https://sites.google.com/u/1/d/19gZ72KDvBi4dbd8OvrAOacou7BI96o_w/edit');
                                                },
                                            ),
                                            TextSpan(text: ' and '),
                                            TextSpan(
                                              text: 'Privacy Policy',
                                              style: TextStyle(color: Colors.blue),
                                              recognizer: TapGestureRecognizer()
                                                ..onTap = () {
                                                  launch('https://sites.google.com/u/1/d/1PrUnXPvPoIhoIhjkpiotgGXJFowOfEdk/edit');
                                                },
                                            ),
                                            TextSpan(text: '.'),
                                          ])),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
  }

  void refreshTap(bool flag) {
    setState(() {
      isTapped = flag;
    });
  }

  void filterRuleSet() {
    List<Ruleset> newListRuleset = List();
    listDropdownItemsRuleSet.forEach((element) {
      if (element.sports == _selectedItemSport.value) {
        newListRuleset.add(element);
      }
    });
    newListRuleset.add(Ruleset(REQUEST_ANOTHER_RULESET, prefix0.str_request_another_rule_set, REQUEST_ANOTHER_RULESET, true));
    _dropdownMenuItemsRuleSet = buildDropDownMenuItemsRuleSet(newListRuleset);
    _selectedItemRuleSet = _dropdownMenuItemsRuleSet[0].value;
  }

  /*inappsection*/

  Future<void> initStoreInfo() async {
    final bool isAvailable = await _connection.isAvailable();
    debugPrint("isAvailable : $isAvailable");
    if (!isAvailable) {
      setState(() {
        _isAvailable = isAvailable;
        _products = [];
        _purchases = [];
        _notFoundIds = [];
        _consumables = [];
        _purchasePending = false;
        // _loading = false;
      });
      return;
    }

    /*try {
      var _auth = Auth();
      currentUser = await _auth.currentUser();
    } catch (e) {
      print(e);
    }*/

    ProductDetailsResponse productDetailResponse = await _connection.queryProductDetails(Utility.productIds.toSet());
    debugPrint("productDetailResponse : $productDetailResponse");
    if (productDetailResponse.error != null) {
      debugPrint("productDetailResponse 1");
      setState(() {
        _queryProductError = productDetailResponse.error.message;
        _isAvailable = isAvailable;
        _products = productDetailResponse.productDetails;
        _purchases = [];
        _notFoundIds = productDetailResponse.notFoundIDs;
        _consumables = [];
        _purchasePending = false;
        // _loading = false;

        debugPrint("_isAvailable : $productDetailResponse");
        debugPrint("_products : $_products");
        _products.forEach((element) {
          debugPrint("_products : ${element.toString()}");
        });
      });
      return;
    }
    debugPrint("productDetailResponse 2: ");
    if (productDetailResponse.productDetails.isEmpty) {
      debugPrint("productDetailResponse 3: ");
      setState(() {
        _queryProductError = null;
        _isAvailable = isAvailable;
        _products = productDetailResponse.productDetails;
        _purchases = [];
        _notFoundIds = productDetailResponse.notFoundIDs;
        _consumables = [];
        _purchasePending = false;
        // _loading = false;
      });
      return;
    }
    debugPrint("productDetailResponse 4: ");
    final QueryPurchaseDetailsResponse purchaseResponse = await _connection.queryPastPurchases();
    if (purchaseResponse.error != null) {
      // handle query past purchase error..
    }
    final List<PurchaseDetails> verifiedPurchases = [];
    for (PurchaseDetails purchase in purchaseResponse.pastPurchases) {
      debugPrint("productDetailResponse 5: ");
      bool valid = await _verifyPurchase(purchase);
      if (valid) {
        verifiedPurchases.add(purchase);
      }
      if (Platform.isIOS) {
        // Mark that you've delivered the purchase. Only the App Store requires
        // this final confirmation.
        InAppPurchaseConnection.instance.completePurchase(purchase);
      }
    }
    List<String> consumables = await ConsumableStore.load();
    setState(() {
      _isAvailable = isAvailable;
      _products = productDetailResponse.productDetails;
      _purchases = verifiedPurchases;
      _notFoundIds = productDetailResponse.notFoundIDs;
      _consumables = consumables;
      _purchasePending = false;
      // _loading = false;
      debugPrint("productDetailResponse 6: ");
      _products.forEach((element) {
        Map<String, dynamic> mapProduct = Map();
        mapProduct['title'] = element.title;
        mapProduct['description'] = element.description;
        mapProduct['id'] = element.id;
        mapProduct['price'] = element.price;
        mapProduct['skProduct'] = element.skProduct;
        mapProduct['skuDetail'] = element.skuDetail;
        debugPrint("_products : ${mapProduct.toString()}");
      });

      debugPrint("purchase DetailResponse 7: ${_purchases.length} ");
      _purchases.forEach((element) {
        debugPrint("1");
      });
    });
  }

  String selProductID;

  void _listenToPurchaseUpdated(List<PurchaseDetails> purchaseDetailsList,BuildContext myContext) {
    debugPrint("_listenToPurchaseUpdated-----");
    purchaseDetailsList.forEach((PurchaseDetails purchaseDetails) async {
      MyLog.printInappLog("_listenToPurchaseUpdated-productID-----${purchaseDetails.productID}");
      MyLog.printInappLog("_listenToPurchaseUpdated-Selected productID-----${selProductID}");
      if (selProductID == purchaseDetails.productID) {
        if (purchaseDetails.status == PurchaseStatus.pending) {
          MyLog.printInappLog("_listenToPurchaseUpdated-----pending");
          showPendingUI();
        } else {
          if (purchaseDetails.status == PurchaseStatus.error) {
            MyLog.printInappLog("_listenToPurchaseUpdated-----error");
            handleError(purchaseDetails.error);
            return;
          } else if (purchaseDetails.status == PurchaseStatus.purchased) {
            MyLog.printInappLog("_listenToPurchaseUpdated-----purchased");
            bool valid = await _verifyPurchase(purchaseDetails);
            if (valid) {
              MyLog.printInappLog("_listenToPurchaseUpdated-----valid-----");
              deliverProduct(purchaseDetails);
              Map<String, dynamic> mapPurchaseDetails = Utility.mapFromPurchaseDetail(purchaseDetails);
              MyLog.printInappLog("_listenToPurchaseUpdated-----deliverProduct-----${mapPurchaseDetails}");
              MyLog.printInappLog("_listenToPurchaseUpdated-----_verifyPurchase");

              try {
                DatabaseReference dbRef = FirebaseDatabase.instance.reference();
                MyLog.printInappLog("_listenToPurchaseUpdated-----1");
                String uid = await Auth().currentUser();
                MyLog.printInappLog("_listenToPurchaseUpdated-----2");
                //TODO Need to work on after "verify Purchase"
                await dbRef.child(prefix0.users).child(uid).update({
                  prefix0.purchaseDetail: mapPurchaseDetails,
                });

                MyLog.printInappLog("_listenToPurchaseUpdated-----3");
                MyLog.printInappLog("deliverProduct : Payment success");
                Fluttertoast.showToast(msg: "Payment success", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.green, textColor: Colors.white, fontSize: 16.0);
                Navigator.of(myContext).pushReplacement(
                  MaterialPageRoute(builder: (context) => chooseQuiz),
                );
              } catch (e) {
                MyLog.printInappLog("_listenToPurchaseUpdated-----4");
                // displaySnackBar('Error updating user', context);
                displaySnackBarWithKey(e.toString());
              }
            } else {
              MyLog.printInappLog("_listenToPurchaseUpdated-----invalid-----");
              _handleInvalidPurchase(purchaseDetails);
            }
          }
          if (Platform.isIOS) {
            InAppPurchaseConnection.instance.completePurchase(purchaseDetails);
          } else if (Platform.isAndroid) {
            /*if (!kAutoConsume && purchaseDetails.productID == _kConsumableId) {
            InAppPurchaseConnection.instance.consumePurchase(purchaseDetails);
          }*/
          }
        }
      }
    });
  }

  void showPendingUI() {
    // setState(() {
    MyLog.printInappLog("_listenToPurchaseUpdated-----showPendingUI");
    _purchasePending = true;
    MyLog.printInappLog("showPendingUI : Purchase Pending");
    Fluttertoast.showToast(msg: "Purchase Pending", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.orange, textColor: Colors.white, fontSize: 16.0);
    // });
  }

  void handleError(IAPError error) {
    // setState(() {
    _purchasePending = false;
    // });
    String errorMessage = error.message;
    MyLog.printInappLog("_listenToPurchaseUpdated-----handleError----- ${errorMessage.toString()}");
    switch (errorMessage) {
      case 'BillingResponse.userCanceled':
        MyLog.printInappLog('BillingResponse.userCanceled');
        break;
      case 'BillingResponse.error':
        MyLog.printInappLog('Billing Error. Purchase failed.');
        displaySnackBarWithKey('Billing Error. Purchase failed.');
        break;
      default:
        displaySnackBarWithKey('membership_screen : default: $errorMessage');
    }
  }

  Future<bool> _verifyPurchase(PurchaseDetails purchaseDetails) async {
    // IMPORTANT!! Always verify a purchase before delivering the product.
    // For the purpose of an example, we directly return true.

    return Future<bool>.value(true);
  }

  void deliverProduct(PurchaseDetails purchaseDetails) async {
    // IMPORTANT!! Always verify a purchase purchase details before delivering the product.
    setState(() {
      _purchases.add(purchaseDetails);
      _purchasePending = false;
    });
  }

  void _handleInvalidPurchase(PurchaseDetails purchaseDetails) {
    // handle invalid purchase here if  _verifyPurchase` failed.
    MyLog.printInappLog("_listenToPurchaseUpdated-----_handleInvalidPurchase-----${Utility.mapFromPurchaseDetail(purchaseDetails)}");
    Fluttertoast.showToast(msg: 'Invalid purchase', toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
  }

  void displaySnackBarWithKey(String s) {
    MyLog.printInappLog("displaySnackBarWithKey : ${s}");
    Fluttertoast.showToast(msg: s, toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
  }

  showActiveMessage(PurchaseDetails item) {
    String strMsg = '';
    if (item == null) {
      strMsg = "showActiveMessage : No subscription";
    } else {
      strMsg = "showActiveMessage : Active subscription is : ${Utility.mapFromPurchaseDetail(item)}";
    }
    MyLog.printInappLog(strMsg);
  }

  bool isActiveSubscription = false;

  refreshChooseQuizNavigator({List<Question> flaggedQuestions,Sports selectedItemSport,Ruleset selectedItemRuleSet,UserModel currentUser}){
    chooseQuiz = ChooseQuiz(flaggedQuestions: flaggedQuestions,
      selectedItemRuleSet: selectedItemRuleSet,
      selectedItemSport: selectedItemSport,
      currentUser: currentUser,
    );
    setState(() {

    });
  }
  activeSubscription(BuildContext context) async {
    DatabaseReference ref = FirebaseDatabase.instance.reference();
    String uid = await Auth().currentUser();
    var users = await ref.child(prefix0.users).child(uid).once();
    UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));

    // By pass purchase screen : code start
    /*chooseQuiz = ChooseQuiz(flaggedQuestions: widget.flaggedQuestions,
      selectedItemRuleSet: _selectedItemRuleSet,
      selectedItemSport: _selectedItemSport,
      currentUser: currentUser,
    );
    StateContainer.of(context).updateSubscriptionType(Utility.productIds[1]);
    isActiveSubscription = true;
    return;*/
    // By pass purchase screen : code end


    isActiveSubscription = false;
    MyLog.printInappLog("Total purchased : ${_purchases.length}");
    String purchased = '';
    for (PurchaseDetails item in _purchases) {
      purchased += '${item.productID}, ';
    }
    MyLog.printInappLog("Purchased Items : ${purchased}");

    Map<String, dynamic> sport = Sports.toJson(_selectedItemSport);
    Map<String, dynamic> ruleSet = Ruleset.toJson(_selectedItemRuleSet);

    await FirebaseDatabase.instance.goOnline();

    DataSnapshot snapshotSlides = await FirebaseDatabase.instance.reference().child(prefix0.rules_new).once();
    List<dynamic> valuesSlides = snapshotSlides.value;
    List<Question> allQuizQuestions = valuesSlides.map((data) => Question.fromJSON(Map<String, dynamic>.from(data))).toList();

    debugPrint("sport : ${sport[prefix0.value]}");
    debugPrint("ruleset : ${sport[prefix0.value]}");
    List<Question> temp = allQuizQuestions.where((element) => element.sport == sport[prefix0.value] && element.ruleset == ruleSet[prefix0.value]).toList();
    allQuizQuestions = temp;
    debugPrint("total question : ${allQuizQuestions.length}");



    // _isLoadingSportsAndRuleSet = false;
    allQuizQuestions.shuffle();

    /*quizQuestionRoute = QuizQuestion(
      sport: sport,
      ruleset: ruleSet,
      title: *//*sport['name']*//* 'QUIZ ALL QUESTIONS',
      questions: allQuizQuestions,
      quizType: QuizType.All,
      user: currentUser,
    );*/
    /*chooseQuiz = ChooseQuiz(flaggedQuestions: widget.flaggedQuestions,
      selectedItemRuleSet: _selectedItemRuleSet,
      selectedItemSport: _selectedItemSport,
      currentUser: currentUser,
    );*/
    refreshChooseQuizNavigator(flaggedQuestions: widget.flaggedQuestions,
        selectedItemRuleSet: _selectedItemRuleSet,
        selectedItemSport: _selectedItemSport,
        currentUser: currentUser);
    /*Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => ChooseQuiz(flaggedQuestions: flaggedQuestions,
          selectedItemRuleSet: _selectedItemRuleSet,
          selectedItemSport: _selectedItemSport,
          currentUser: currentUser,
        )));*/

    for (PurchaseDetails item in _purchases) {
      if (item.productID == Utility.productIds[0]) {
        showActiveMessage(item);
        StateContainer.of(context).updateSubscriptionType(Utility.productIds[1]);
        isActiveSubscription = true;
        setState(() {});
        return;
      } else if (item.productID == Utility.productIds[1]) {
        showActiveMessage(item);
        StateContainer.of(context).updateSubscriptionType(item.productID);
        isActiveSubscription = true;
        setState(() {});
        return;
      } else if (item.productID == Utility.productIds[2]) {
        showActiveMessage(item);
        StateContainer.of(context).updateSubscriptionType(item.productID);
        isActiveSubscription = true;
        setState(() {});
        return;
      } else if (item.productID == Utility.productIds[3]) {
        showActiveMessage(item);
        StateContainer.of(context).updateSubscriptionType(item.productID);
        isActiveSubscription = true;
        setState(() {});
        return;
      }
    }
    setState(() {
      /*StateContainer.of(context).updateSubscriptionType(Utility.kProductIds[1]);
      isActiveSubscription = true;*/

      showActiveMessage(null);
      isActiveSubscription = false;
    });

    // return false;
  }

/*-----*/
  Future<void> _showTestDialog(BuildContext myContext) async {
    return showDialog<void>(
      context: myContext,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.transparent,
          // title: Text('AlertDialog Title'),
          content: Container(
            child: Text("Testing"),
          ),
          /*actions: <Widget>[
          TextButton(
            child: Text('Approve'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],*/
        );
      },
    );
  }
  // Sports selectedDialogSport;
  // Ruleset selectedDialogRuleset;

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.transparent,
          // title: Text('AlertDialog Title'),
          content: Container(
            padding: EdgeInsets.only(top: 10, bottom: 10, left: 30, right: 30),
            decoration: BoxDecoration(shape: BoxShape.rectangle, border: Border.all(width: 2, color: Colors.black), borderRadius: BorderRadius.circular(10), color: MyColors.btnGoColor),
            child: SingleChildScrollView(
              child: ListBody(
                /*children: <Widget>[
                Text('This is a demo alert dialog.'),
                Text('Would you like to approve of this message?'),
                listDropdownItemsSport
              ],*/
                children: listSportRuleSet.asMap().entries.map((sportRulesetEntries) {
                  Sports sport;
                  Ruleset ruleset;
                  String type = sportRulesetEntries.value['type'];
                  if (type == prefix0.sports) {
                    sport = sportRulesetEntries.value['data'];
                  } else if (type == prefix0.ruleset) {
                    sport = sportRulesetEntries.value[prefix0.sports];
                    ruleset = sportRulesetEntries.value['data'];
                  }
                  return Builder(
                    builder: (context) {
                      return GestureDetector(
                        onTap: () {},
                        child: Container(
                          child: type == prefix0.sports
                              ? Container(child: Text("${sport.name}", style: TextStyle(fontSize: 18, decoration: TextDecoration.underline, height: 3)))
                              : GestureDetector(
                                  onTap: () {
                                    _selectedItemSport = sport;
                                    _selectedItemRuleSet = ruleset;
                                    setState(() {});
                                    Navigator.of(context).pop();
                                  },
                                  child: Container(
                                    child: Text("     ${ruleset.name}", style: TextStyle(fontSize: 18, height: 2)),
                                  ),
                                ),
                        ),
                      );
                    },
                  );
                }).toList(),
              ),
            ),
          ),
          /*actions: <Widget>[
          TextButton(
            child: Text('Approve'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],*/
        );
      },
    );
  }
}
// #1 #2 #3 #5 need to test in iOS with latest build

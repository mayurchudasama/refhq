import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:ref_hq/widget/status_bar_height.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: SafeArea(
        top: false,
        bottom: false,
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/background.jpg'),
                fit: BoxFit.fill),
          ),
          child: Column(
            children: [
              StatusBarHeight(),
              Row(
                children: [
                  GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: SizedBox(
                          width: 55,
                          height: 55,
                          child: Icon(Icons.arrow_back_ios))),
                  Expanded(
                    child: Text("About",textAlign: TextAlign.center,style: TextStyle(
                        fontSize: 20
                    ),),
                  ),SizedBox(
                    width: 55,
                    height: 55,)
                ],
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ConstrainedBox(
                        constraints: BoxConstraints(
                            minWidth: MediaQuery.of(context).size.width),
                        child: RichText(
                          text: TextSpan(children: <TextSpan>[
                            TextSpan(text: "Contact: "),
                            TextSpan(
                              text: "RefHQSports@gmail.com",
                              style: TextStyle(color: Colors.blue),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  launch('mailto:RefHQSports@gmail.com?');
                                },
                            )
                          ]),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(height: 16),
                      ConstrainedBox(
                        constraints: BoxConstraints(
                            minWidth: MediaQuery.of(context).size.width),
                        child: RichText(
                          text: TextSpan(children: <TextSpan>[
                            TextSpan(text: "Instagram: "),
                            TextSpan(
                              text: "@RefHQ",
                              style: TextStyle(color: Colors.blue),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  launch('https://www.instagram.com/refhq/');
                                },
                            )
                          ]),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(height: 16),
                      ConstrainedBox(
                        constraints: BoxConstraints(
                            minWidth: MediaQuery.of(context).size.width),
                        child: Text(
                          'Note: Leaderboard streaks are accumulated by correctly answering consecutive questions in the "Quiz All Questions" category only.',
                          style: TextStyle(fontStyle: FontStyle.italic),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:ref_hq/constants.dart';
import 'package:ref_hq/data_models/user_model.dart';
import 'package:ref_hq/home/all_question_layout.dart';
import 'package:ref_hq/home/home_screen.dart';
import 'package:ref_hq/home/review_flagged_question_layout.dart';
import 'package:ref_hq/home/rulewise_question_layout.dart';

import '../data_models/question.dart';

class QuizQuestion extends StatelessWidget {
  final List<Question> questions;
  final String title;
  final QuizType quizType;
  final UserModel user;
  final sport;
  final ruleset;

  QuizQuestion({Key key, this.title, this.questions, this.quizType, @required this.user, this.sport, this.ruleset}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        debugPrint("willPopScope called");
        if (this.quizType == QuizType.Flagged) {
          Navigator.of(context).pop();
        } else {
          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => HomeScreen()), (route) => false);
        }
        return true;
      },
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/background.jpg'),
                fit: BoxFit.fill),
          ),
          constraints: BoxConstraints(
            minWidth: MediaQuery.of(context).size.width,
            minHeight: MediaQuery.of(context).size.height,
          ),
          child: this.quizType == QuizType.Flagged
              ? ReviewFlaggedQuestionLayout(
            title: title,
            ruleset: this.ruleset,
            sport: this.sport,
            questions: this.questions,
            quizType: this.quizType,
            user: this.user,
          )
              : this.quizType == QuizType.All
              ? AllQuestionLayout(
            title: title,
            ruleset: this.ruleset,
            sport: this.sport,
            questions: this.questions,
            quizType: this.quizType,
            user: this.user,
          )
              : RulewiseQuestionLayout(
            title: title,
            ruleset: this.ruleset,
            sport: this.sport,
            questions: this.questions,
            quizType: this.quizType,
            user: this.user,
          )
        ),
      ),
    );
  }
}

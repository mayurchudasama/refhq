import 'dart:async';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ref_hq/auth.dart';
import 'package:ref_hq/constants.dart' as constant;
import 'package:ref_hq/ref_theme.dart';
import 'package:ref_hq/widget/display_sports_ruleset_view.dart';
import 'package:ref_hq/widget/status_bar_height.dart';

import '../constants.dart';
import '../data_models/question.dart';
import '../data_models/user_model.dart';

class AllQuestionLayout extends StatefulWidget {
  List<Question> questions;
  final QuizType quizType;
  UserModel user;
  final sport;
  final ruleset;
  final title;

  AllQuestionLayout({Key key,this.title, this.questions, this.quizType, this.user, this.sport, this.ruleset}) : super(key: key);

  @override
  _AllQuestionLayoutState createState() => _AllQuestionLayoutState();
}

class _AllQuestionLayoutState extends State<AllQuestionLayout> {
  Question currentQuestion;
  String correctAnswer;
  Color button1Color, button2Color, button3Color, button4Color;
  int maxNumber;

  //index = 0;
  int valTotalQuestionAttended = 0;
  int lastAttendedCount = 0;


  DatabaseReference ref = FirebaseDatabase.instance.reference();
  ScrollController _scrollController = ScrollController();

  bool isLoading = true;
  StreamSubscription<ConnectivityResult> subscription;
  String mSportRulesetStreak;
  @override
  void initState() {
    super.initState();
    mSportRulesetStreak = '${widget.sport['name']} - ${widget.ruleset['name']}';
    initConnectionAndQuestion();
  }

  void initConnectionAndQuestion() async {
    subscription = Connectivity().onConnectivityChanged.listen((ConnectivityResult result) async {
      // Got a new connectivity status!
      initQuizData(result);
    });

    var connectivityResult = await (Connectivity().checkConnectivity());
    initQuizData(connectivityResult);
  }

  void initQuizData(ConnectivityResult result) async {
    debugPrint("Connectivity Result : ${result}");
    if (result == ConnectivityResult.none) {
      await FirebaseDatabase.instance.goOffline();
      getQuiz();
    } else {
      await FirebaseDatabase.instance.goOnline();
      getQuiz();
    }
  }

  void getQuiz() async{
    if (widget.questions.length != 0) {
      if (widget.questions[0].viewMode == ViewMode.ReviewFlagged) {
        _setUpQuestionForReview();
      } else {
        setState(() {
          isLoading = true;
        });
        await _getCurrentUser();
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  @override
  dispose() {
    super.dispose();

    subscription.cancel();
  }
  int mCurrentStreak=0;
  @override
  Widget build(BuildContext context) {

    return SafeArea(
        top: false,
        bottom: false,
        child: Padding(
          padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
          child: isLoading
              ? Scaffold(
            extendBody: true,
            extendBodyBehindAppBar: true,
            body: SafeArea(
              top: Platform.isIOS ? false : true,
              bottom: Platform.isIOS ? false : true,
              child: LinearProgressIndicator(),
            ),
          )
              : Column(
                  children: [
                    StatusBarHeight(),
                    Row(
                      children: [
                        GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: SizedBox(
                                width: 55,
                                height: 55,
                                child: Icon(Icons.arrow_back_ios))),
                        Expanded(
                          child: Text(widget.title,textAlign: TextAlign.center,style: TextStyle(
                              fontSize: 20
                          ),),
                        ),SizedBox(
                          width: 55,
                          height: 55,)
                      ],
                    ),
                    DisplaySportsRuleSetView(
                      mapSport: widget.sport,
                      mapRuleSet: widget.ruleset,
                    ),
                    isLoading?Container():Expanded(
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          ///This is Streak and Question Number
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                (widget.quizType == QuizType.All || widget.quizType == QuizType.Flagged)
                                    ? AutoSizeText(
                                        valTotalQuestionAttended == maxNumber ? '${valTotalQuestionAttended}/$maxNumber' : '${valTotalQuestionAttended + 1}/$maxNumber',
                                        style: Theme.of(context).textTheme.subtitle.merge(
                                              TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                                            ),
                                      )
                                    : SizedBox(),
                                (widget.quizType == QuizType.All)
                                    ? AutoSizeText(
                                        'Streak: $mCurrentStreak',
                                        style: Theme.of(context).textTheme.subtitle.merge(
                                              TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                      )
                                    : SizedBox(),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),

                          ///These are the Question and Answers.
                          Expanded(child: (currentQuestion == null || currentQuestion.question == null)
                              ? Container()
                              : Container(
                            padding: EdgeInsets.only(top: 20,bottom: 20),
                            // color: Colors.red,
                            child: Scrollbar(
                              child: SingleChildScrollView(
                                controller: _scrollController,
                                padding: EdgeInsets.all(8),
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: <Widget>[
                                    Text(
                                      "${currentQuestion.question}",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(fontSize: 18),
                                      overflow: TextOverflow.visible,
                                    ),
                                    SizedBox(height: 20,),
                                    Scrollbar(
                                      child: Column(
                                        children: <Widget>[
                                          (currentQuestion.answersList.length > 0)
                                              ? RaisedButton(
                                            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                            color: (currentQuestion.viewMode == ViewMode.Quiz) ? null : button1Color,
                                            padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
                                            child: ConstrainedBox(
                                              constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                              child: Wrap(
                                                direction: Axis.horizontal,
                                                children: [
                                                  Text(
                                                    currentQuestion.answersList[0],
                                                    style: TextStyle(
                                                        fontSize: 16
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ],
                                              ),
                                            ),
                                            onPressed: () {
                                              //mtest
                                              if (currentQuestion.viewMode == ViewMode.Quiz) {
                                                debugPrint("answerlist : ${currentQuestion.answersList[0]}");
                                                selectedAnswer(chosenAnswer: currentQuestion.answersList[0], answerNumber: SelectedAnswer.Answer1, viewMode: ViewMode.Selected);
                                              }
                                            },
                                          )
                                              : SizedBox(height: 0),
                                          SizedBox(height: 16),
                                          (currentQuestion.answersList.length > 1)
                                              ? RaisedButton(
                                            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                            color: (currentQuestion.viewMode == ViewMode.Quiz) ? null : button2Color,
                                            padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
                                            child: ConstrainedBox(
                                              constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                              child: Wrap(
                                                direction: Axis.horizontal,
                                                children: [
                                                  Text(
                                                    currentQuestion.answersList[1],
                                                    style: TextStyle(
                                                        fontSize: 16
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ],
                                              ),
                                            ),
                                            onPressed: () {
                                              if (currentQuestion.viewMode == ViewMode.Quiz) {
                                                selectedAnswer(chosenAnswer: currentQuestion.answersList[1], answerNumber: SelectedAnswer.Answer2, viewMode: ViewMode.Selected);
                                              }
                                            },
                                          )
                                              : SizedBox(height: 0),
                                          SizedBox(height: 16),
                                          (currentQuestion.answersList.length > 2)
                                              ? RaisedButton(
                                            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                            color: (currentQuestion.viewMode == ViewMode.Quiz) ? null : button3Color,
                                            padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
                                            child: ConstrainedBox(
                                              constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                              child: Wrap(
                                                direction: Axis.horizontal,
                                                children: [
                                                  Text(
                                                    currentQuestion.answersList[2],
                                                    style: TextStyle(
                                                        fontSize: 16
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ],
                                              ),
                                            ),
                                            onPressed: () {
                                              if (currentQuestion.viewMode == ViewMode.Quiz) {
                                                selectedAnswer(chosenAnswer: currentQuestion.answersList[2], answerNumber: SelectedAnswer.Answer3, viewMode: ViewMode.Selected);
                                              }
                                            },
                                          )
                                              : SizedBox(height: 0),
                                          SizedBox(height: 16),
                                          (currentQuestion.answersList.length > 3)
                                              ? RaisedButton(
                                            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                            color: (currentQuestion.viewMode == ViewMode.Quiz) ? null : button4Color,
                                            padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
                                            child: ConstrainedBox(
                                              constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                              child: Wrap(
                                                direction: Axis.horizontal,
                                                children: [
                                                  Text(
                                                    currentQuestion.answersList[3],
                                                    style: TextStyle(
                                                        fontSize: 16
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ],
                                              ),
                                            ),
                                            onPressed: () {
                                              if (currentQuestion.viewMode == ViewMode.Quiz) {
                                                selectedAnswer(chosenAnswer: currentQuestion.answersList[3], answerNumber: SelectedAnswer.Answer4, viewMode: ViewMode.Selected);
                                              }
                                            },
                                          )
                                              : SizedBox(height: 0),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),),

                          /// These are navigation, next question, and Flag Question buttons.
                          Container(
                            height: 80,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                ///These are navigation buttons
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    ConstrainedBox(
                                      constraints: BoxConstraints(maxWidth: 50),
                                      child: IconButton(
                                        padding: EdgeInsets.all(0),
                                        alignment: Alignment.center,
                                        icon: Icon(Icons.chevron_left),
                                        iconSize: 48,
                                        onPressed: (valTotalQuestionAttended <= 0)
                                            ? null
                                            : () {
                                                if (valTotalQuestionAttended == maxNumber) {
                                                  valTotalQuestionAttended--;
                                                }
                                                _goToPreviousQuestion();
                                              },
                                      ),
                                    ),
                                    ConstrainedBox(
                                      constraints: BoxConstraints(maxWidth: 40),
                                      child: IconButton(
                                          padding: EdgeInsets.all(0),
                                          alignment: Alignment.center,
                                          icon: Icon(Icons.chevron_right),
                                          iconSize: 48,
                                          onPressed: (valTotalQuestionAttended == maxNumber) ? null : (valTotalQuestionAttended != widget.questions.length - 1 && currentQuestion.viewMode != ViewMode.Selected) ? (currentQuestion.viewMode != ViewMode.Quiz) ? () => _goToNextQuestion() : null : null),
                                    ),
                                  ],
                                ),

                                ///These is Next Question Button and Rule Text Widget
                                Container(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      SizedBox(height: 4),
                                      (currentQuestion.viewMode != ViewMode.Quiz)
                                          ? AutoSizeText(
                                              (currentQuestion.rule == '-') ? "" : "Rule: ${currentQuestion.rule}",
                                              style: TextStyle(fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),
                                              minFontSize: 5,
                                            )
                                          : SizedBox(),
                                      SizedBox(height: 8),
                                      // (valTotalQuestionAttended == widget.questions.length - 1)?Text("Done"):Container(),
                                      (/*mTotalQuestionAttended*/ valTotalQuestionAttended < (widget.questions.length-1) /*true*/)
                                          ? (currentQuestion.viewMode == ViewMode.Selected)
                                              ? Container(
                                                height: 40,
                                                child: RaisedButton(
                                                    onPressed: () async {
                                                      debugPrint("next question");
                                                      debugPrint("--testing ---1");
                                                      debugPrint("valTotalQuestionAttended : ${valTotalQuestionAttended}");
                                                      if((valTotalQuestionAttended+1) == maxNumber){
                                                        debugPrint("--testing ---3");
                                                        _goToFirstQuestion();
                                                      }
                                                      else/* (valTotalQuestionAttended < (maxNumber *//*- 1*//*)) */{
                                                        debugPrint("next question-1");
                                                        debugPrint("--testing ---2");
                                                        lastAttendedCount = valTotalQuestionAttended;

                                                        debugPrint("listAttendedQuestions--1--before : ${listAttendedQuestions.length}");
                                                        currentQuestion = widget.questions[lastAttendedCount];
                                                        addQuestionToAttended(currentQuestion.toJson());

                                                        var connectivityResult = await (Connectivity().checkConnectivity());
                                                        if (connectivityResult == ConnectivityResult.none) {
                                                          // I am connected to a mobile network.
                                                          // mSportRulesetStreak
                                                          ref.child(users).child(widget.user.key).child(constant.attendedQuestions).update({
                                                            mSportRulesetStreak: listAttendedQuestions
                                                          });
                                                          debugPrint("next question-2 offline");
                                                          _goToNextQuestion();
                                                        } else {
                                                          // I am connected to a network.
                                                          await ref.child(users).child(widget.user.key).child(constant.attendedQuestions).update({
                                                            mSportRulesetStreak: listAttendedQuestions
                                                          });
                                                          debugPrint("next question-2 online");
                                                          _goToNextQuestion();
                                                        }
                                                      }

                                                    },
                                                    child: Text('Next Question')),
                                              )
                                              : SizedBox(height: 40)
                                          : SizedBox(
                                              height:
                                                  40),
                                      SizedBox(height: 4),
                                    ],
                                  ),
                                ),

                                ///This is Flag question Icon and Text Widget.
                                Container(
                                  alignment: Alignment.bottomCenter,
                                  child: (widget.quizType == QuizType.Flagged || currentQuestion.viewMode == ViewMode.ReviewFlagged) ? _removeFlagQuestion() : _flagQuestion(),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
        ),
      );
  }

/*  TODO*/

  List<dynamic> listAttendedQuestions = List();

  Future<void> _getCurrentUser() async {

    // DatabaseReference ref = FirebaseDatabase.instance.reference();
    String uid = await Auth().currentUser();

    if(widget.user.sport_ruleset_streak == null ){

      await FirebaseDatabase.instance.reference().child(constant.users).child(uid)
          .child(constant.sport_ruleset_streak)
          .child(mSportRulesetStreak)
          .update({currentStreak: '0',constant.streak:'0'});
    }else if(widget.user.sport_ruleset_streak[mSportRulesetStreak] == null){
      await FirebaseDatabase.instance.reference().child(constant.users).child(uid)
          .child(constant.sport_ruleset_streak)
          .child(mSportRulesetStreak)
          .update({currentStreak: '0',constant.streak:'0'});
    }


    DataSnapshot users = await FirebaseDatabase.instance.reference().child(constant.users).child(uid).once();

    Map<String, dynamic> mapUser = Map<String, dynamic>.from(users.value);
    debugPrint("mapUser : ${mapUser}");
    widget.user = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));
    if (mCurrentStreak == null && widget.user.sport_ruleset_streak != null) {
      // mCurrentStream = int.parse(widget.user.currentStreak);
      mCurrentStreak = int.parse(widget.user.sport_ruleset_streak[mSportRulesetStreak][constant.currentStreak]);
    }

    /* This comment is just to print unique question list
    List<Question> tempList = List.from(widget.questions);
    List<Question> uniqueList = List();
    tempList.forEach((elementQ) {
      final result2 = uniqueList.where((element) => element.qid == elementQ.qid).toList();
      if (result2.length == 0) {
        uniqueList.add(elementQ);
      }
    });
    debugPrint("unique list : ${uniqueList.length}");*/
    // });
    dynamic tmpAttendedQuestion = mapUser[constant.attendedQuestions];

    List<dynamic> tmpListQuestion;
    if (tmpAttendedQuestion == null) {
      tmpListQuestion = List();
    }else if(tmpAttendedQuestion[mSportRulesetStreak] == null ){
      tmpListQuestion = List();
    }else {
      tmpListQuestion = mapUser[constant.attendedQuestions][mSportRulesetStreak];
    }

    List<dynamic> listQuestion = tmpListQuestion;
//     final strQuestionAttended = mapUser[constant.totalQuestionAttended];
    if (listQuestion.length != 0) {
      valTotalQuestionAttended = listQuestion.length;
      lastAttendedCount = valTotalQuestionAttended;
      debugPrint("valTotalQuestionAttended ${valTotalQuestionAttended}");
      debugPrint("lastAttendedCount ${lastAttendedCount}");

      List<Question> filteredQuestinos = List();
      List<Question> actualQuestinos = List.from(widget.questions);
      List<Question> newQuestionList = List();
      List<Question> notAddedQuestionList = List();
      debugPrint("actual length : ${actualQuestinos.length}");
      debugPrint("visited length : ${listQuestion.length} ");
      int notAdded = 0;
      await Future.forEach(actualQuestinos, (elementQ) {
        final result = listQuestion.where((element) => element[constant.QID] == elementQ.qid).toList();
        if (result.length == 0) {
          filteredQuestinos.add(elementQ);
        } else {
          notAdded++;
          notAddedQuestionList.add(elementQ);
        }
      });

      debugPrint("filtered length : ${filteredQuestinos.length}");
      debugPrint("not added : ${notAdded}");
      List<Question> duplicateCheckLis = List.from(notAddedQuestionList);
      listAttendedQuestions = List.from(listQuestion);
      await Future.forEach(listQuestion, (element) {

        Question question = Question.fromDynamicJSON(element);

        question.answersList.clear();
        if (question.answer1 != "null" && question.answer1 != "-") question.answersList.add(question.answer1);
        if (question.answer2 != "null" && question.answer2 != "-") question.answersList.add(question.answer2);
        if (question.answer3 != "null" && question.answer3 != "-") question.answersList.add(question.answer3);
        if (question.answer4 != "null" && question.answer4 != "-") question.answersList.add(question.answer4);
        if (question.flag == 1) question.answersList.shuffle();
        constant.SelectedAnswer mSelectedAnswer;
        switch (question.correct) {
          case 1:
            mSelectedAnswer = constant.SelectedAnswer.Answer1;
            break;
          case 2:
            mSelectedAnswer = constant.SelectedAnswer.Answer2;
            break;
          case 3:
            mSelectedAnswer = constant.SelectedAnswer.Answer3;
            break;
          case 4:
            mSelectedAnswer = constant.SelectedAnswer.Answer4;
            break;
        }

        question.selectedAnswerString = question.answersList[question.correct - 1];
        question.selectedAnswer = mSelectedAnswer;
        bool isAnswerCorrect = question.answersList[question.correct - 1] == correctAnswer;
        // if (viewMode != null) {
        question.viewMode = ViewMode.Selected;
        if (widget.quizType == QuizType.All) calculateCurrentStreak(isAnswerCorrect);
        // }
        var newButtonColor = (isAnswerCorrect) ? correctAnswerColor : Theme.of(context).accentColor;
        //If answer correct, change button to green else red.

        switch (mSelectedAnswer) {
          case SelectedAnswer.Answer1:
            button1Color = newButtonColor;
            break;
          case SelectedAnswer.Answer2:
            button2Color = newButtonColor;
            break;
          case SelectedAnswer.Answer3:
            button3Color = newButtonColor;
            break;
          case SelectedAnswer.Answer4:
            button4Color = newButtonColor;
            break;
        }
        int correctAnswerIndexPlus1;
        for (int i = 0; i < question.answersList.length; i++) {
          if (question.answersList[i] == correctAnswer) {
            correctAnswerIndexPlus1 = i + 1;
          }
        }
        var newButtonColor1 = correctAnswerColor;
        switch (correctAnswerIndexPlus1) {
          case 1:
            button1Color = newButtonColor1;
            break;
          case 2:
            button2Color = newButtonColor1;
            break;
          case 3:
            button3Color = newButtonColor1;
            break;
          case 4:
            button4Color = newButtonColor1;
            break;
        }
        newQuestionList.add(question);
      });
      newQuestionList.addAll(filteredQuestinos);
      widget.questions = newQuestionList;
    }
    debugPrint("---valTotalQuestionAttended : ${valTotalQuestionAttended}");
    debugPrint("---listAttendedQuestions : ${listAttendedQuestions}");
    debugPrint("---total question : ${listQuestion.length}");
    debugPrint("---attended question : ${valTotalQuestionAttended}");
    // if(listQuestion.length < widget.questions.length) {
    await _setUpQuestion();
  }

  void addAnswersToListAndShuffle() {
    currentQuestion.answersList.clear();
    if (currentQuestion.answer1 != "null" && currentQuestion.answer1 != "-") currentQuestion.answersList.add(currentQuestion.answer1);
    if (currentQuestion.answer2 != "null" && currentQuestion.answer2 != "-") currentQuestion.answersList.add(currentQuestion.answer2);
    if (currentQuestion.answer3 != "null" && currentQuestion.answer3 != "-") currentQuestion.answersList.add(currentQuestion.answer3);
    if (currentQuestion.answer4 != "null" && currentQuestion.answer4 != "-") currentQuestion.answersList.add(currentQuestion.answer4);
    randomizeAnswerList();
  }

  void randomizeAnswerList() {
    //Randomizes only when the flag is found to be 1.
    if (currentQuestion.flag == 1) currentQuestion.answersList.shuffle();
  }

  String determineCorrectAnswer() {
    switch (currentQuestion.correct) {
      case 1:
        return currentQuestion.answer1;
        break;
      case 2:
        return currentQuestion.answer2;
        break;
      case 3:
        return currentQuestion.answer3;
        break;
      case 4:
        return currentQuestion.answer4;
        break;
      default:
        return null;
    }
  }

  void selectedAnswer({@required String chosenAnswer, @required SelectedAnswer answerNumber, ViewMode viewMode}) {
    currentQuestion.selectedAnswerString = chosenAnswer;
    currentQuestion.selectedAnswer = answerNumber;
    bool isAnswerCorrect = chosenAnswer == correctAnswer;
    if (viewMode != null) {
      currentQuestion.viewMode = viewMode;
      if (widget.quizType == QuizType.All) calculateCurrentStreak(isAnswerCorrect);
    }
    var newButtonColor = (isAnswerCorrect) ? correctAnswerColor : Theme.of(context).accentColor;
    //If answer correct, change button to green else red.

    switch (answerNumber) {
      case SelectedAnswer.Answer1:
        button1Color = newButtonColor;
        break;
      case SelectedAnswer.Answer2:
        button2Color = newButtonColor;
        break;
      case SelectedAnswer.Answer3:
        button3Color = newButtonColor;
        break;
      case SelectedAnswer.Answer4:
        button4Color = newButtonColor;
        break;
    }
    markTheCorrectAnswer(chosenAnswer);
    setState(() {});
  }

  void markTheCorrectAnswer(String chosenAnswer) {
    int correctAnswerIndexPlus1;
    for (int i = 0; i < currentQuestion.answersList.length; i++) {
      if (currentQuestion.answersList[i] == correctAnswer) {
        correctAnswerIndexPlus1 = i + 1;
      }
    }
    var newButtonColor = correctAnswerColor;
    switch (correctAnswerIndexPlus1) {
      case 1:
        button1Color = newButtonColor;
        break;
      case 2:
        button2Color = newButtonColor;
        break;
      case 3:
        button3Color = newButtonColor;
        break;
      case 4:
        button4Color = newButtonColor;
        break;
    }
  }

  _goToPreviousQuestion() {
    setState(() {
      if (valTotalQuestionAttended > 0) valTotalQuestionAttended -= 1;
      (currentQuestion.viewMode == ViewMode.ReviewFlagged) ? _setUpQuestionForReview() : /*_setVisitedQuestion()*/ _setUpQuestion();
    });
  }
  _goToFirstQuestion() async {
    debugPrint("_goToFirstQuestion");
    valTotalQuestionAttended = 0;

    (currentQuestion.viewMode == ViewMode.ReviewFlagged) ? _setUpQuestionForReview() : _setUpQuestion();
    // _resetButtonColorValues();
    setState(() {});
  }
  _goToNextQuestion() async {
    if (valTotalQuestionAttended < (maxNumber - 1)) {
      valTotalQuestionAttended += 1;
    }

    (currentQuestion.viewMode == ViewMode.ReviewFlagged) ? _setUpQuestionForReview() : _setUpQuestion();
    setState(() {});
  }


  void _setUpQuestionForReview() {
    debugPrint("_goToPreviousQuestion : _setUpQuestionForReview");
    _resetButtonColorValues();
    maxNumber = widget.questions.length;
    currentQuestion = widget.questions[valTotalQuestionAttended];
    if (currentQuestion.answersList.length == 0) addAnswersToListAndShuffle();
    correctAnswer = determineCorrectAnswer();
    markTheCorrectAnswer(correctAnswer);
    if (_scrollController.hasClients) _scrollController.animateTo(0, duration: Duration(milliseconds: 100), curve: Curves.linear);
  }

  void addQuestionToAttended(currentQuestion) {
    Map<String, dynamic> mapQuestion = Map();
    mapQuestion[constant.answer1] = currentQuestion[constant.answer1];
    mapQuestion[constant.answer2] = currentQuestion[constant.answer2];
    mapQuestion[constant.answer3] = currentQuestion[constant.answer3];
    mapQuestion[constant.answer4] = currentQuestion[constant.answer4];
    mapQuestion[constant.correct] = currentQuestion[constant.correct];
    mapQuestion[constant.flag] = currentQuestion[constant.flag];
    mapQuestion[constant.question] = currentQuestion[constant.question];
    mapQuestion[constant.rule] = currentQuestion[constant.rule];
    mapQuestion[constant.QID] = currentQuestion[constant.QID];
    mapQuestion[constant.SPORT] = currentQuestion[constant.SPORT];
    mapQuestion[constant.RULESET] = currentQuestion[constant.RULESET];
    listAttendedQuestions.add(mapQuestion);

    debugPrint("listAttendedQuestions--1 : ${listAttendedQuestions.length}");
  }

  // asd
  Future<void> _setUpQuestion() async {
    debugPrint("_goToPreviousQuestion : _setUpQuestion");
    _resetButtonColorValues();
    maxNumber = widget.questions.length;
    if (valTotalQuestionAttended == maxNumber) {
      currentQuestion = widget.questions[valTotalQuestionAttended - 1];
    } else {
      currentQuestion = widget.questions[valTotalQuestionAttended];
    }

    debugPrint("currentQuestion : ${currentQuestion.toJson()}");
    /*TODO*/

    if (currentQuestion.answersList.length == 0) {
      debugPrint("answerList ${currentQuestion.answersList}");
      addAnswersToListAndShuffle();
    }
    correctAnswer = determineCorrectAnswer();
    //Lock the question in review mode so that the answers don't shuffle anymore.
    currentQuestion.viewMode = (currentQuestion.selectedAnswer == null) ? ViewMode.Quiz : ViewMode.Review;
    debugPrint("viewMode ${currentQuestion.viewMode}");
    if (currentQuestion.viewMode == ViewMode.Review) {
      selectedAnswer(chosenAnswer: currentQuestion.selectedAnswerString, answerNumber: currentQuestion.selectedAnswer);
    }
    if (_scrollController.hasClients) {
      _scrollController.animateTo(0, duration: Duration(milliseconds: 100), curve: Curves.linear);
    }
  }

  void _resetButtonColorValues() {
    button1Color = null;
    button2Color = null;
    button3Color = null;
    button4Color = null;
  }

  void calculateCurrentStreak(bool isAnswerCorrect) async {
    //If correct answer, mark wasPreviousQuestionCorrect as True
    if (isAnswerCorrect)
      mCurrentStreak += 1;
    else
      mCurrentStreak = 0;

    await ref.child(users).child(widget.user.key)
        .child(constant.sport_ruleset_streak)
        .child(mSportRulesetStreak)
        .update({currentStreak: '$mCurrentStreak'});

// af
    int tmpStreak = int.parse(widget.user.sport_ruleset_streak[mSportRulesetStreak][constant.streak]);
    if (mCurrentStreak > /*widget.user.streak*/tmpStreak) {
      /*widget.user.streak*/widget.user.sport_ruleset_streak[mSportRulesetStreak][constant.streak] = '$mCurrentStreak';

    //   widget.sport,
    // mapRuleSet: widget.ruleset,
      await ref.child(users).child(widget.user.key)
            .child(constant.sport_ruleset_streak)
            .child(mSportRulesetStreak)
            .update({constant.streak: '$mCurrentStreak'});

      /*await ref.child(users).child(widget.user.key).update({constant.streak: '$streak'});*/
    }
  }

  void _flagThisQuestion(String key) async {
    setState(() {
      currentQuestion.flagType = FlagType.AttemptingFlag;
    });
    var questionString = currentQuestion.toJson();
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      try {
        await ref.child(users).child(widget.user.key).child(savedQuestions).child(mSportRulesetStreak).push().set(questionString);
      } catch (e) {
        setState(() {
          debugPrint("error");
          currentQuestion.flagType = FlagType.UnFlagged;
        });
      }
      setState(() {
        currentQuestion.flagType = FlagType.Flagged;
      });
    } else {
      try {
        await ref.child(users).child(widget.user.key).child(savedQuestions).child(mSportRulesetStreak).push().set(questionString);
      } catch (e) {
        setState(() {
          debugPrint("error");
          currentQuestion.flagType = FlagType.UnFlagged;
        });
      }
      setState(() {
        currentQuestion.flagType = FlagType.Flagged;
      });
    }
    try {
      await ref.child(users).child(widget.user.key).child(savedQuestions).child(mSportRulesetStreak).push().set(questionString);
    } catch (e) {
      setState(() {
        debugPrint("error");
        currentQuestion.flagType = FlagType.UnFlagged;
      });
    }
    setState(() {
      currentQuestion.flagType = FlagType.Flagged;
    });
  }

  _flagQuestion() {

    return Column(
      children: <Widget>[
        IconButton(
          icon: (currentQuestion.flagType == FlagType.UnFlagged)
              ? Icon(Icons.outlined_flag)
              : (currentQuestion.flagType == FlagType.AttemptingFlag)
                  ? Icon(Icons.flag)
                  : Icon(
                      Icons.flag,
                      color: Theme.of(context).accentColor,
                    ),
          iconSize: 24,
          padding: EdgeInsets.all(0),
          onPressed: (currentQuestion.flagType == FlagType.UnFlagged)
              ? () {
                  debugPrint('flag this question');
                  _flagThisQuestion(widget.user.key);
                }
              : null,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: Text('Flag Question', style: TextStyle(fontSize: 12)),
        ),
      ],
    );
  }

  _removeFlagQuestion() {
    return Column(
      children: <Widget>[
        IconButton(
            icon: Icon(
              Icons.remove_circle,
              color: Colors.red,
            ),
            iconSize: 24,
            padding: EdgeInsets.all(0),
            onPressed: () {
              _removeThisQuestion(widget.user.key);
            }),
        Padding(
          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: Text('Remove Flag', style: TextStyle(fontSize: 12)),
        ),
      ],
    );
  }

  void _removeThisQuestion(String key) async {
    await ref.child(users).child(widget.user.key).child(savedQuestions).child(mSportRulesetStreak).child(currentQuestion.key).set(null);
    widget.questions.removeAt(valTotalQuestionAttended);
    maxNumber = widget.questions.length;
    if (widget.questions.length == 0) {
      Navigator.pop(context);
    } else if ((widget.questions.length) == valTotalQuestionAttended /*mTotalQuestionAttended*/) {
      valTotalQuestionAttended -= 1;
      setState(() {
        (currentQuestion.viewMode == ViewMode.ReviewFlagged) ? _setUpQuestionForReview() : _setUpQuestion();
      });
    } else {
      setState(() {
        (currentQuestion.viewMode == ViewMode.ReviewFlagged) ? _setUpQuestionForReview() : _setUpQuestion();
      });
    }
  }
}

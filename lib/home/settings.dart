
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:keyboard_actions/keyboard_actions_config.dart';
import 'package:launch_review/launch_review.dart';
import 'package:package_info/package_info.dart';
import 'package:ref_hq/auth.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:ref_hq/data_models/subscription_type.dart';
import 'package:ref_hq/data_models/user_model.dart';
import 'package:ref_hq/data_models/user_subscription.dart';
import 'package:ref_hq/main.dart';
import 'package:ref_hq/state_container/state_container.dart';
import 'package:ref_hq/utils/app_style.dart';
import 'package:ref_hq/utils/email_sender.dart';
import 'package:ref_hq/widget/status_bar_height.dart';
import 'package:url_launcher/url_launcher.dart';

import '../misc_widgets.dart';

class Settings extends StatefulWidget {


  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  UserModel currentUser;
  FirebaseUser firebaseUser;
  String email='';
  bool isGetting = true;
  SubscriptionType subscriptionType = SubscriptionType.NONE;

  final FocusNode _nodeText1 = FocusNode();
  final FocusNode _nodeText2 = FocusNode();
  final FocusNode _nodeText3 = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initFirebaseUser();

  }
  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.black,

      // nextFocus: true,
      actions: [
        KeyboardActionsItem(
          focusNode: _nodeText1,
        ),
        KeyboardActionsItem(
          focusNode: _nodeText2,
        ),
        KeyboardActionsItem(
          focusNode: _nodeText3,
        ),

      ],
    );
  }
  void initFirebaseUser() async{
    firebaseUser = await Auth().getFirebaseUser();
    email = firebaseUser.email;
    debugPrint("user id : ${firebaseUser.uid}");
    _getCurrentUser();
  }
  _getCurrentUser() async {
    DatabaseReference ref = FirebaseDatabase.instance.reference();
    String uid = await Auth().currentUser();
    var users = await ref.child(prefix0.users).child(uid).once();
    currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));

    setState(() {
      _firstNameController.text = currentUser.firstName;
      _lastNameController.text = currentUser.lastName;
      _hometownController.text = currentUser.homeTown;
      isGetting = false;
      String strSubscriptionType = currentUser.subscriptionType;
      if (strSubscriptionType != null) {
        subscriptionType = UserSubscription.getSubscriptionTypeEnum(strSubscriptionType);
      }
    });





  }
  bool isChanges = false;
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _hometownController = TextEditingController();
  bool isUpdatingProfile = false;
  @override
  Widget build(BuildContext context) {
    double heightProgress = 4.0;
    return Scaffold(
     extendBody: true,
      extendBodyBehindAppBar: true,
      body: Builder(
        builder: (BuildContext context) {
          return SafeArea(
            child: Container(
              constraints: BoxConstraints(
                minHeight: MediaQuery.of(context).size.height,
                minWidth: MediaQuery.of(context).size.width
              ),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/background.jpg'),
                    fit: BoxFit.fill),
              ),
              child: KeyboardActions(
                config: _buildConfig(context),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      StatusBarHeight(),
                      Row(
                        children: [
                          GestureDetector(
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                              child: SizedBox(
                                  width: 55,
                                  height: 55,
                                  child: Icon(Icons.arrow_back_ios))),
                          Expanded(
                            child: Text("",textAlign: TextAlign.center,style: TextStyle(
                                fontSize: 20
                            ),),
                          ),SizedBox(
                            width: 55,
                            height: 55,)
                        ],
                      ),
                      isGetting
                          ? Container(height: heightProgress, child: LinearProgressIndicator()): Container(
                        height: heightProgress,
                      ),
                      isUpdatingProfile?Container(
                        width: double.infinity,
                        height: heightProgress,
                        child: LinearProgressIndicator(),
                      ):SizedBox(height: heightProgress,),
                      Container(
                        padding: const EdgeInsets.only(left: 24,right: 24),
                        height: 65,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Email"),
                            Text(email)
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 24,right: 24),
//                    height: 65,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("First Name"),
                            SizedBox(width: 20,),
                            Expanded(
                              child: TextField(
                                focusNode: _nodeText1,
                                onChanged: (text){
                                  isChanges = true;
                                  setState(() {});
                                },
                                textAlign: TextAlign.end,
                                controller: _firstNameController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  hintText: 'Enter First Name',
                                  hintStyle: TextStyle(
                                    fontStyle: FontStyle.italic,
                                    fontSize: 16
                                  ),
                                  errorStyle: AppStyles.errorStyle,
                                  border: InputBorder.none,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: 65,
                        padding: const EdgeInsets.only(left: 24,right: 24),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Last Name"),
                            SizedBox(width: 20,),
                            Expanded(
                              child: TextField(
                                focusNode: _nodeText2,
                                onChanged: (text){
                                  isChanges = true;
                                  setState(() {});
                                },
                                textAlign: TextAlign.end,
                                controller: _lastNameController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  hintText: 'Enter Last Name',
                                  hintStyle: TextStyle(
                                      fontStyle: FontStyle.italic,
                                      fontSize: 16
                                  ),
                                  errorStyle: AppStyles.errorStyle,
                                  border: InputBorder.none,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 24,right: 24),
                        height: 65,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Hometown"),
                            SizedBox(width: 20,),
                            Expanded(
                              child: TextField(
                                focusNode: _nodeText3,
                                onChanged: (text){
                                  isChanges = true;
                                  setState(() {});
                                },
                                textAlign: TextAlign.end,
                                controller: _hometownController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  hintText: 'Enter Hometown',
                                  hintStyle: TextStyle(
                                      fontStyle: FontStyle.italic,
                                      fontSize: 16
                                  ),
                                  errorStyle: AppStyles.errorStyle,
                                  border: InputBorder.none,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8,right: 8),
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                            minHeight: 65,
                            minWidth: MediaQuery
                                .of(context)
                                .size
                                .width,
                          ),
                          child: FlatButton(
                            color: Colors.transparent,
                            splashColor: Colors.black26,
                            onPressed: () {
                              resetPassword(email: email,context: context);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("Reset Password"),
                                Icon(Icons.chevron_right)
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8,right: 8),
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                            minHeight: 65,
                            minWidth: MediaQuery
                                .of(context)
                                .size
                                .width,
                          ),
                          child: FlatButton(
                            color: Colors.transparent,
                            splashColor: Colors.black26,
                            onPressed: () async {
                              await EmailSender.contactRefHQSupport();
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("Contact RefHQ Support"),
                                Icon(Icons.chevron_right)
                              ],
                            ),
                          ),
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(left: 8,right: 8),
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                            minHeight: 65,
                            minWidth: MediaQuery
                                .of(context)
                                .size
                                .width,
                          ),
                          child: FlatButton(
                            color: Colors.transparent,
                            splashColor: Colors.black26,
                            onPressed: () async{
                              await EmailSender.requestAdditionalSportsOrRules();
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("Request Additional Sports or Rules"),
                                Icon(Icons.chevron_right)
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8,right: 8),
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                            minHeight: 65,
                            minWidth: MediaQuery
                                .of(context)
                                .size
                                .width,
                          ),
                          child: FlatButton(
                            color: Colors.transparent,
                            splashColor: Colors.black26,
                            onPressed: () async{
                              final PackageInfo info = await PackageInfo.fromPlatform();
                              String packageName = info.packageName;
                              DatabaseReference ref = FirebaseDatabase.instance.reference();
                              String uid = await Auth().currentUser();
                              var users = await ref.child(prefix0.users).child(uid).once();
                              UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));
                              // String sku = currentUser.subscriptionType;
                              String sku = StateContainer.of(context).subscriptionType;
                              String launchUrl = Platform.isIOS ? "https://apps.apple.com/account/subscriptions" : "https://play.google.com/store/account/subscriptions?sku=$sku&package=$packageName";
                              launch(launchUrl);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("Manage Subscription"),
                                Icon(Icons.chevron_right)
                              ],
                            ),
                          ),
                        ),
                      ),
//                      (subscriptionType == SubscriptionType.ALL_ACCESS)?Container():Padding(
//                        padding: EdgeInsets.only(left: 8,right: 8),
//                        child: ConstrainedBox(
//                          constraints: BoxConstraints(
//                            minHeight: 65,
//                            minWidth: MediaQuery
//                                .of(context)
//                                .size
//                                .width,
//                          ),
//                          child: FlatButton(
//                            color: Colors.transparent,
//                            splashColor: Colors.black26,
//                            onPressed: () async{
//                              /*Navigator.of(context).push(MaterialPageRoute(
//                                  builder: (context) => MembershipProScreen()));*/
//                            },
//                            child: Row(
//                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                              children: <Widget>[
//                                Text("Upgrade to Pro"),
//                                Icon(Icons.chevron_right)
//                              ],
//                            ),
//                          ),
//                        ),
//                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8,right: 8),
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                            minHeight: 65,
                            minWidth: MediaQuery
                                .of(context)
                                .size
                                .width,
                          ),
                          child: FlatButton(
                            color: Colors.transparent,
                            splashColor: Colors.black26,
                            onPressed: () {
//                               LaunchReview.launch();
                              if (Platform.isAndroid) {
                                LaunchReview.launch(androidAppId: "com.refhq.app");
                              } else if (Platform.isIOS) {
                                LaunchReview.launch(iOSAppId: "1441702159");
                              }
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("Rate Us"),
                                Icon(Icons.chevron_right)
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8,right: 8),
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                            minHeight: 65,
                            minWidth: MediaQuery
                                .of(context)
                                .size
                                .width,
                          ),
                          child: FlatButton(
                            color: Colors.transparent,
                            splashColor: Colors.black26,
                            onPressed: () {
                              signOut(context);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("Log Out"),
                                Icon(Icons.chevron_right)
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 24,),
                      /*isChanges?*/Padding(
                        padding: EdgeInsets.only(left: 24,right: 24,),
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                            minHeight: 50,
                            minWidth: MediaQuery
                                .of(context)
                                .size
                                .width,
                          ),
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50),
                              side: BorderSide(width: 0.70,color: Colors.white)
                            ),
                            color: Colors.transparent,
                            splashColor: Colors.white24,
                            onPressed: () async{
                              if(_firstNameController.text == ""){
                                displaySnackBar('First Name is required', context);
                                return ;
                              }
                              if(_lastNameController.text == ""){
                                displaySnackBar('Last Name is required', context);
                                return;
                              }

                              try {
                                setState(() {
                                  isUpdatingProfile = true;
                                });
                                DatabaseReference dbRef = FirebaseDatabase.instance.reference();
                                String uid = await Auth().currentUser();
                                await dbRef.child(prefix0.users).child(uid).update({
                                  prefix0.firstName: _firstNameController.text,
                                  prefix0.lastName: _lastNameController.text,
                                  prefix0.homeTown: _hometownController.text
                                });
                                setState(() {
                                  isUpdatingProfile = false;
                                  isChanges = false;
                                });
                                displaySnackBar('Profile updated successfully', context);
                              }catch(e){
                                displaySnackBar('Error updating user', context);
                              }
                            },
                            child: Container(
                              alignment: Alignment.center,
                              height: 50,
                              width: double.infinity,
                              child: Text("Save"),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 36,),

                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  void resetPassword({String email, @required BuildContext context}) async {
    try {
      await Auth().resetPassword(email: email);
      displaySnackBar('Password reset email sent.', context);
    } catch (e) {
      print(e);
      var connectivityResult = await (Connectivity().checkConnectivity());
      String snackbarErrorMessage;
      if (connectivityResult == ConnectivityResult.none) {
        snackbarErrorMessage = 'No Internet. Try again';
      } else if (e == NoSuchMethodError) {
        snackbarErrorMessage = 'Unknown Error';
      } else {
        switch (e.code) {
          case 'ERROR_INVALID_EMAIL':
            snackbarErrorMessage = 'Invalid email';
            break;
          case 'ERROR_USER_NOT_FOUND':
            snackbarErrorMessage = 'User not found';
        }
      }
      displaySnackBar(snackbarErrorMessage, context);
    }
  }

  void signOut(BuildContext context) async {
    try {
      await Auth().signOut();

      displaySnackBar('Signed Out', context);
      Future.delayed(
        const Duration(milliseconds: 1000),
            () {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => SubscriptionPage(title: 'RefHQ')), (
              Route<dynamic> route) => false);
        },
      );
    } catch (e) {
      print(e);
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.none) {
        displaySnackBar('No Internet. Try again', context);
      } else {
        displaySnackBar('Unknown Error', context);
      }
    }
  }
}

import 'dart:convert';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:ref_hq/auth.dart';
import 'package:ref_hq/constants.dart';
import 'package:ref_hq/data_models/user_model.dart';
import 'package:ref_hq/loginorsignup/user_signin.dart';
import 'package:ref_hq/utils/app_preferences.dart';
import 'package:ref_hq/utils/common_constant.dart';
import 'package:ref_hq/utils/my_colors.dart';
import 'package:ref_hq/widget/display_sports_ruleset_view.dart';
import 'package:ref_hq/widget/status_bar_height.dart';

class LeaderBoard extends StatefulWidget {
  final Auth auth = Auth();

  @override
  _LeaderBoardState createState() => _LeaderBoardState();
}

enum AuthStatus { notSignedIn, signedIn }

class _LeaderBoardState extends State<LeaderBoard> {
  AuthStatus _authStatus = AuthStatus.notSignedIn;
  List<UserModel> list = new List();

  Map<dynamic, dynamic> mapSport;
  Map<dynamic, dynamic> mapRuleSet;

  bool isGetting = true;
  bool isGettingLeaderBoard = true;
  bool isGettingSortsAndRuleset = true;
  String mSportRulesetStreak;
  @override
  void initState() {
    super.initState();

    widget.auth.currentUser().then((userId) async {
      _authStatus = userId == null ? AuthStatus.notSignedIn : AuthStatus.signedIn;
      setState(() {});
      await FirebaseDatabase.instance.goOnline();

      getSportsAndRuleset();

      setState(() {
        isGetting = false;
      });
    });
  }

  void getSportsAndRuleset() async {
    setState(() {
      isGettingSortsAndRuleset = true;
    });
    DatabaseReference ref = FirebaseDatabase.instance.reference();
    String uid = await Auth().currentUser();
    var users = await ref.child(prefix0.users).child(uid).once();
    UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));
    // prefSports = prefs.getString(AppPreferences.pref_sports);
    // mapSport = json.decode(prefSports);
    mapSport = currentUser.sports;
    // prefRuleSet = prefs.getString(AppPreferences.pref_rule_set);
    // mapRuleSet = json.decode(prefRuleSet);
    mapRuleSet = currentUser.ruleset;
    mSportRulesetStreak = '${mapSport['name']} - ${mapRuleSet['name']}';
    setState(() {
      isGettingSortsAndRuleset = false;
    });
    getLeaderBoard();
  }

  List<UserModel> getSportsAndRulesetWise(List<UserModel> list){
    List<UserModel> tmpListModel = List();
    list.forEach((element) {
      if(element.sports !=null && element.ruleset !=null){
        tmpListModel.add(element);
      }
    });
    List<UserModel> listFiltered = tmpListModel.where((element) => element.sports[prefix0.SPORT] == mapSport[prefix0.value]
        && element.ruleset[prefix0.SPORT] == mapRuleSet[prefix0.value]
    ).toList();
    return listFiltered;

  }
  void getLeaderBoard() async {
    setState(() {
      isGettingLeaderBoard = true;
    });
     var snapshotLeaderboard = await FirebaseDatabase.instance
        .reference()
        .child(users)
        // .orderByChild(prefix0.streak)
        // .limitToFirst(20)
        .once();

     var map = Map<dynamic, dynamic>.from(snapshotLeaderboard.value);
     list = await getSortedList(map);
    /* list.forEach((element) {
       debugPrint("Element key : ${element.key} Name : ${element.firstName} ${element.lastName} ${element.homeTown}");
     });*/
     // list = getSportsAndRulesetWise(list);

     setState(() {
       isGettingLeaderBoard = false;
     });
  }

  @override
  Widget build(BuildContext context) {
    switch (_authStatus) {
      case AuthStatus.notSignedIn:
        return UserSignIn(
          onSignedIn: _signedIn,
        );
        break;
      case AuthStatus.signedIn:
        return Scaffold(
          body: SafeArea(
            top: false,
            bottom: false,
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage('assets/images/background.jpg'), fit: BoxFit.fill),
              ),
              constraints: BoxConstraints(minHeight: MediaQuery
                  .of(context)
                  .size
                  .height, minWidth: MediaQuery
                  .of(context)
                  .size
                  .width),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    StatusBarHeight(),
                    Row(
                      children: [
                        GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: SizedBox(
                                width: 55,
                                height: 55,
                                child: Icon(Icons.arrow_back_ios))),
                        Expanded(
                          child: Text("All-Time Top Streaks",textAlign: TextAlign.center,style: TextStyle(
                              fontSize: 20
                          ),),
                        ),SizedBox(
                          width: 55,
                          height: 55,)
                      ],
                    ),
                    (isGetting || isGettingLeaderBoard || isGettingSortsAndRuleset)
                        ? LinearProgressIndicator()
                        : Container(
                      height: 4,
                    ),
                    isGettingSortsAndRuleset
                        ? Container() : DisplaySportsRuleSetView(mapSport: mapSport,mapRuleSet: mapRuleSet,),
                    isGettingLeaderBoard
                        ? Container()
                        : ListView.separated(
                      primary: false,
                      shrinkWrap: true,
                      itemCount: (list.length>20)?20:list.length,
                      itemBuilder: (context, index) => _buildListItem(context, list, index),
                      separatorBuilder: (context, index) => Divider(),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
    }
  }

  void _signedIn() {
    setState(() {
      _authStatus = AuthStatus.signedIn;
    });
  }

  _buildListItem(BuildContext context, List<UserModel> leaders, int index) {
    var rank = index + 1;
    var leader = leaders[index];
    return Stack(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text('$rank. ${leader.firstName} ${leader.lastName} (${leader.homeTown})'),
        ),
        Positioned(
          right: 0,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text('${leader.sport_ruleset_streak[mSportRulesetStreak]['streak']}'),
          ),
        ),
      ],
    );
  }

  Future<List<UserModel>> getSortedList(Map map) {
    list.clear();
    for (int i = 0; i < map.length; i++) {
      String key = map.keys.elementAt(i);
      var user = Map<dynamic, dynamic>.from(map[key]);
      final tmpSportRulesetStreak = user[prefix0.sport_ruleset_streak];
      if(tmpSportRulesetStreak!=null) {
        final tmpMSportRulesetStreak = user[prefix0.sport_ruleset_streak][mSportRulesetStreak];
        if(tmpMSportRulesetStreak != null) {
          // String mStreak = user['streak'];
          String mStreak = user[prefix0.sport_ruleset_streak][mSportRulesetStreak]['streak'];
          if (mStreak !=null && mStreak != '0') {
            UserModel userModel = UserModel.fromSnapshot(key, user);
            if ((userModel.firstName != '' && userModel.lastName != 'null' && userModel.homeTown != '')) {
              list.add(userModel);
            }
          }
        }
      }
    }
    list.forEach((element) {
      debugPrint('----1111----${element.sport_ruleset_streak}');
    });
    //Sorting
    list.sort((b, a) => int.parse(a.sport_ruleset_streak[mSportRulesetStreak]['streak']).compareTo(int.parse(b.sport_ruleset_streak[mSportRulesetStreak]['streak'])));
    return Future.value(list);
  }
}

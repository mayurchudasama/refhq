import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:package_info/package_info.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:ref_hq/constants.dart';
import 'package:ref_hq/data_models/ruleset.dart';
import 'package:ref_hq/data_models/sports.dart';
import 'package:ref_hq/data_models/subscription_type.dart';
import 'package:ref_hq/data_models/user_model.dart';
import 'package:ref_hq/data_models/user_subscription.dart';
import 'package:ref_hq/home/about.dart';
import 'package:ref_hq/home/leaderboard.dart';
import 'package:ref_hq/home/membership_screen.dart';
import 'package:ref_hq/home/quiz_questions.dart';
import 'package:ref_hq/home/settings.dart';
import 'package:ref_hq/newsletter/newsletter_year_screen.dart';
import 'package:ref_hq/pregame/pregame_screen.dart';
import 'package:ref_hq/ref_theme.dart';
import 'package:ref_hq/state_container/state_container.dart';
import 'package:ref_hq/utils/common_constant.dart';
import 'package:ref_hq/utils/email_sender.dart';
import 'package:ref_hq/utils/my_colors.dart';
import 'package:ref_hq/utils/my_toast.dart';
import 'package:ref_hq/utils/utility.dart';
import 'package:ref_hq/widget/status_bar_height.dart';
import 'package:url_launcher/url_launcher.dart';

import '../auth.dart';
import '../data_models/question.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool _loading = true;
  UserModel currentUser = UserModel();

  List<Question> flaggedQuestions = List();

  List<DropdownMenuItem<Sports>> _dropdownMenuItemsSport;
  Sports _selectedItemSport;
  List<Sports> listDropdownItemsSport = List();

  bool showIAABlogo = false;
  bool showNewsLetter = false;
  List<DropdownMenuItem<Ruleset>> _dropdownMenuItemsRuleSet;
  Ruleset _selectedItemRuleSet;
  List<Ruleset> listDropdownItemsRuleSet = List();

  // double dropdownRuleSetItemHeight = 55.0;

  @override
  void initState() {
    FirebaseAnalytics().setAnalyticsCollectionEnabled(true);
    super.initState();
    debugPrint("created called ");
    initPrefs();
    Utility.cacheAppData();
  }

  Map<dynamic, dynamic> mapSport = Map();
  Map<dynamic, dynamic> mapRuleSet = Map();

  SubscriptionType subscriptionType = SubscriptionType.NONE;

  bool enableSports = true;
  bool enableRuleSet = true;
  bool enableChooseRuleSet = true;

  void initPrefs() async {
    await _getCurrentUser();

    DatabaseReference ref = FirebaseDatabase.instance.reference();
    String uid = await Auth().currentUser();
    var users = await ref.child(prefix0.users).child(uid).once();
    UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));

    String strSubscriptionType = currentUser.subscriptionType;

    if (strSubscriptionType != null) {
      subscriptionType = UserSubscription.getSubscriptionTypeEnum(strSubscriptionType);
      if (subscriptionType == SubscriptionType.SINGLE_SPORT) {
        enableSports = false;
        enableRuleSet = true;
      } else if (subscriptionType == SubscriptionType.SINGLE_RULESET) {
        enableSports = false;
        enableRuleSet = false;
      } else if (subscriptionType == SubscriptionType.ALL_ACCESS) {
        enableSports = true;
        enableRuleSet = true;
      }
    }
    debugPrint("str Subscription Type : ${strSubscriptionType}");
    debugPrint("Enum Subscription Type : ${subscriptionType}");

    listDropdownItemsSport = StateContainer.of(context).sports;
    _dropdownMenuItemsSport = buildDropDownMenuItemsSports(listDropdownItemsSport);

    listDropdownItemsRuleSet = StateContainer.of(context).ruleSet;
    _dropdownMenuItemsRuleSet = buildDropDownMenuItemsRuleSet(listDropdownItemsRuleSet);

//last change
    // prefSports = prefs.getString(AppPreferences.pref_sports);
    // mapSport = json.decode(prefSports);
    mapSport = currentUser.sports;
    debugPrint("mapSport : $mapSport");
    if (mapSport != null) {
      List<DropdownMenuItem<Sports>> mList = _dropdownMenuItemsSport.where((element) => element.value.name == mapSport['name'] && element.value.value == mapSport['value']).toList();
      if (mList.length > 0) {
        _selectedItemSport = mList[0].value;
      } else {
        _selectedItemSport = _dropdownMenuItemsSport[1].value;
      }
    } else {
      enableSports = true;
      _selectedItemSport = _dropdownMenuItemsSport[0].value;
    }

    // prefRuleSet = prefs.getString(AppPreferences.pref_rule_set);
    // mapRuleSet = json.decode(prefRuleSet);
    mapRuleSet = currentUser.ruleset;
    debugPrint("mapRuleset : $mapRuleSet");
    if (mapRuleSet != null) {
      filterRuleSet();
      List<DropdownMenuItem<Ruleset>> mList = _dropdownMenuItemsRuleSet.where((element) => element.value.name == mapRuleSet['name'] && element.value.value == mapRuleSet['value'] && element.value.sports == mapRuleSet['sports']).toList();

      if (mList.length > 0) {
        _selectedItemRuleSet = mList[0].value;
      } else {
        _selectedItemRuleSet = _dropdownMenuItemsRuleSet[1].value;
      }
    } else {
      enableRuleSet = true;
      _selectedItemRuleSet = _dropdownMenuItemsRuleSet[0].value;
    }

    await checkForIAABLogo();
    await checkForNewsLetterButton();
    _loading = false;
    setState(() {});
  }

  Future<void> checkForNewsLetterButton() {
    showNewsLetter = false;

    if (_selectedItemSport.value == prefix0.VALUE_BASKETBALL) {
      showNewsLetter = true;
    } else {
      showNewsLetter = false;
    }
  }

  Future<void> checkForIAABLogo() {
    bool isSportBasketBall = false;
    if (_selectedItemSport.value == prefix0.VALUE_BASKETBALL) {
      isSportBasketBall = true;
    }
    bool isRuleSetNFHC = false;
    if (_selectedItemRuleSet.value == prefix0.VALUE_NFHC_2020_21) {
      isRuleSetNFHC = true;
    }
    showIAABlogo = (isSportBasketBall /*&& isRuleSetNFHC*/) ? true : false;
    /*if (_selectedItemRuleSet.name == "NFHS (2020-21)") {
      dropdownRuleSetItemHeight = 55;
    } else {
      dropdownRuleSetItemHeight = 55;
    }*/
  }

  List<DropdownMenuItem<Sports>> buildDropDownMenuItemsSports(List listItems) {
    List<DropdownMenuItem<Sports>> items = List();
    for (Sports listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: Text(
                listItem.name,
                style: TextStyle(fontStyle: (listItem.name == prefix0.str_request_another_sport) ? FontStyle.italic : FontStyle.normal, color: Colors.white, fontSize: dropDownTextSize, decoration: listItem.name == prefix0.str_choose_sport ? TextDecoration.underline : TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem<Ruleset>> buildDropDownMenuItemsRuleSet(List listItems) {
    List<DropdownMenuItem<Ruleset>> items = List();
    for (Ruleset listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              // color: Colors.grey,
              child: Text(
                listItem.name,
                style: TextStyle(fontStyle: (listItem.name == prefix0.str_request_another_rule_set) ? FontStyle.italic : FontStyle.normal, color: Colors.white, fontSize: dropDownTextSize, decoration: listItem.name == prefix0.str_choose_rule_set ? TextDecoration.underline : TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
      /*}*/
    }
    return items;
  }

  double dropDownIconSize = 20;
  double dropDownHeight = 48;
  double dropDownTextSize = 18;

  @override
  Widget build(BuildContext context) {
    return (_loading)
        ? Scaffold(
            extendBody: true,
            extendBodyBehindAppBar: true,
            body: SafeArea(
              top: Platform.isIOS ? false : true,
              bottom: Platform.isIOS ? false : true,
              child: LinearProgressIndicator(),
            ),
          )
        : Scaffold(
            extendBody: true,
            extendBodyBehindAppBar: true,
            body: SafeArea(
              top: false,
              bottom: false,
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage('assets/images/background.jpg'), fit: BoxFit.fill),
                ),
                child: Column(
                  children: [
                    StatusBarHeight(),
                    // Setting button
                    Container(
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(),
                          ),
                          Container(
                            padding: EdgeInsets.only(right: 10),
                            child: IconButton(
                              icon: Icon(Icons.settings),
                              onPressed: () {
                                debugPrint("Yes");
                                Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => Settings(),
                                ));
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 48, right: 48),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: SingleChildScrollView(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Container(width: MediaQuery.of(context).size.width / 2.25, child: Image.asset('assets/images/logo.png')),
                                    showIAABlogo
                                        ? Container(
                                            width: 180,
                                            // height: 50,
                                            alignment: Alignment.center,
                                            margin: EdgeInsets.only(top: 0, bottom: 10),
                                            child: Image.asset('assets/images/iaab_logo.png'))
                                        : Container(),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Container(
                                      width: 300,
                                      padding: EdgeInsets.only(left: 10, right: 10),
                                      decoration: BoxDecoration(shape: BoxShape.rectangle, border: Border.all(width: CommonConstant.borderDropdown, color: Colors.black), borderRadius: BorderRadius.circular(10), color: MyColors.dropdownBackgroudColor),
                                      child: DropdownButtonHideUnderline(
                                        child: DropdownButton(
                                            itemHeight: dropDownHeight,
                                            icon: Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.white,
                                              size: dropDownIconSize,
                                            ),
                                            value: _selectedItemSport,
                                            items: _dropdownMenuItemsSport,
                                            onChanged: (value) async {
                                              setState(() {
                                                enableChooseRuleSet = true;
                                                _selectedItemSport = value;
                                              });
                                              if (_selectedItemSport.value == CHOOSE_SPORT) {
                                                filterRuleSet();
                                                checkForIAABLogo();
                                                checkForNewsLetterButton();
                                                enableChooseRuleSet = false;
                                                setState(() {});
                                                return;
                                              }
                                              if (_selectedItemSport.value == REQUEST_ANOTHER_SPORT) {
                                                await EmailSender.requestAnotherSport(_selectedItemSport);
                                                return;
                                              }
                                              setState(() {
                                                filterRuleSet();
                                                checkForIAABLogo();
                                                checkForNewsLetterButton();
                                                // saveSportsAndRulese();
                                              });
                                            }),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Container(
                                      width: 300,
                                      // constraints: BoxConstraints(minHeight: 40),
                                      padding: EdgeInsets.only(left: 10, right: 10),
                                      decoration: BoxDecoration(shape: BoxShape.rectangle, border: Border.all(width: CommonConstant.borderDropdown, color: Colors.black), borderRadius: BorderRadius.circular(10), color: MyColors.dropdownBackgroudColor),
                                      // height: 45,
                                      child: DropdownButtonHideUnderline(
                                        child: DropdownButton(
                                            itemHeight: dropDownHeight,
                                            icon: Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.white,
                                              size: dropDownIconSize,
                                            ),
                                            value: _selectedItemRuleSet,
                                            items: _dropdownMenuItemsRuleSet,
                                            onChanged: (value) async {
                                              setState(() {
                                                _selectedItemRuleSet = value;
                                                /*if (_selectedItemRuleSet.name == "NFHS (2020-21)") {
                                                        dropdownRuleSetItemHeight = 55;
                                                      } else {
                                                        dropdownRuleSetItemHeight = 55;
                                                      }*/
                                              });
                                              if (_selectedItemRuleSet.value == prefix0.REQUEST_ANOTHER_RULESET) {
                                                await EmailSender.requestAnotherRuleSet(_selectedItemRuleSet);
                                              }
                                              setState(() {
                                                checkForIAABLogo();
                                                // saveSportsAndRulese();
                                              });
                                            }),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    ConstrainedBox(
                                      constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                      child: RaisedButton(
                                        shape: CommonConstant.roundedRectangleBorder,
                                        color: Theme.of(context).accentColor,
                                        padding: EdgeInsets.all(12),
                                        child: Text(
                                          'Start Quiz',
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        onPressed: () async {

                                          /*DatabaseReference ref = FirebaseDatabase.instance.reference();
                                          await ref.child(users).child('fu8bMHHvcOWj8b2md5vaNkn9NOv1').update({attendedQuestions: null});
                                          return;*/
                                          debugPrint('subscriptionType : ${currentUser.subscriptionType}');

                                          // return;
                                          if (_selectedItemSport.value == prefix0.REQUEST_ANOTHER_SPORT) {
                                            await EmailSender.requestAnotherSport(_selectedItemSport);
                                            return;
                                          }
                                          if (_selectedItemRuleSet.value == prefix0.REQUEST_ANOTHER_RULESET) {
                                            await EmailSender.requestAnotherRuleSet(_selectedItemRuleSet);
                                            return;
                                          }

                                          if (!isSportRulesetAvailable()) {
                                            return;
                                          }

                                          debugPrint("current User subscription type : ${currentUser.subscriptionType}");
                                          if(currentUser.subscriptionType != null) {
                                            if (currentUser.subscriptionType == Utility.productIds[1]) {
                                              // debugPrint('----- : true : ${_selectedItemSport.value} ${mapSport[prefix0.value]}');
                                              // debugPrint('----- : true : ${_selectedItemRuleSet.value} ${mapRuleSet[prefix0.value]}');
                                              if ((_selectedItemSport.value != mapSport[prefix0.value])
                                                  || (_selectedItemRuleSet.value != mapRuleSet[prefix0.value])) {
                                                showUpgradeAlert(context, currentUser.subscriptionType);
                                                return;
                                              }
                                            } else if (currentUser.subscriptionType == Utility.productIds[2]) {
                                              if (_selectedItemSport.value != mapSport[prefix0.value]) {
                                                showUpgradeAlert(context, currentUser.subscriptionType);
                                                return;
                                                // return;
                                              }
                                            }
                                          }

                                          await _getFlaggedQuestions();
                                          Navigator.of(context).push(MaterialPageRoute(
                                              builder: (context) => MembershipScreen(
                                                    flaggedQuestions: flaggedQuestions,
                                                  )));
                                        },
                                      ),
                                    ),
                                    SizedBox(height: 8),
                                    ConstrainedBox(
                                      constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                      child: RaisedButton(
                                        shape: CommonConstant.roundedRectangleBorder,
                                        color: lightButtonColor,
                                        padding: EdgeInsets.all(12),
                                        child: Text(
                                          'Review Flagged Questions',
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        onPressed: () async {
                                          if (_selectedItemSport.value == prefix0.REQUEST_ANOTHER_SPORT) {
                                            await EmailSender.requestAnotherSport(_selectedItemSport);
                                            return;
                                          }
                                          if (_selectedItemRuleSet.value == prefix0.REQUEST_ANOTHER_RULESET) {
                                            await EmailSender.requestAnotherRuleSet(_selectedItemRuleSet);
                                            return;
                                          }
                                          if (!isSportRulesetAvailable()) {
                                            return;
                                          }

                                          if(currentUser.subscriptionType == Utility.productIds[1]){
                                            // debugPrint('----- : true : ${_selectedItemSport.value} ${mapSport[prefix0.value]}');
                                            // debugPrint('----- : true : ${_selectedItemRuleSet.value} ${mapRuleSet[prefix0.value]}');
                                            if ((_selectedItemSport.value != mapSport[prefix0.value])
                                                || (_selectedItemRuleSet.value != mapRuleSet[prefix0.value])) {
                                              showErrorAlert(context,'Single Rule Set',currentUser.subscriptionType);
                                              return;
                                            }

                                          }else if(currentUser.subscriptionType == Utility.productIds[2]){
                                            if (_selectedItemSport.value != mapSport[prefix0.value]) {
                                              showErrorAlert(context,'Single Sport',currentUser.subscriptionType);
                                              return;
                                              // return;
                                            }
                                          }

                                          navigateToReviewFlaggedQuestions();
                                        },
                                      ),
                                    ),

                                    ///The Audio RuleBook Section has been commented out for the
                                    ///time being and the MP3 files are also being removed.
                                    ///In the future, when this section has to be reintroduced,
                                    ///new MP3 files will be required for this section to work.
                                    /*SizedBox(height: 8),
                        ConstrainedBox(
                          constraints: BoxConstraints(
                              minWidth: MediaQuery.of(context).size.width),
                          child: RaisedButton(
                            color: lightButtonColor,
                            padding: EdgeInsets.all(16),
                            child: Text('Audio Rulebook'),
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => AudioRulebook()));
                            },
                          ),
                        ),*/
                                    SizedBox(height: 8),
                                    ConstrainedBox(
                                      constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                      child: RaisedButton(
                                        shape: CommonConstant.roundedRectangleBorder,
                                        color: lightButtonColor,
                                        padding: EdgeInsets.all(12),
                                        child: Text(
                                          'Leaderboard',
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        onPressed: () async {
                                          if (_selectedItemSport.value == prefix0.REQUEST_ANOTHER_SPORT) {
                                            await EmailSender.requestAnotherSport(_selectedItemSport);
                                            return;
                                          }
                                          if (_selectedItemRuleSet.value == prefix0.REQUEST_ANOTHER_RULESET) {
                                            await EmailSender.requestAnotherRuleSet(_selectedItemRuleSet);
                                            return;
                                          }

                                          if (!isSportRulesetAvailable()) {
                                            return;
                                          }
                                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => LeaderBoard()));
                                        },
                                      ),
                                    ),
                                    /*showIAABlogo*/ true
                                        ? Column(
                                            children: <Widget>[
                                              SizedBox(height: 8),
                                              ConstrainedBox(
                                                constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                                child: RaisedButton(
                                                  shape: CommonConstant.roundedRectangleBorder,
                                                  color: lightButtonColor,
                                                  padding: EdgeInsets.all(12),
                                                  child: Text(
                                                    'Pregame',
                                                    style: TextStyle(fontSize: 16),
                                                  ),
                                                  onPressed: () {
                                                    if (!isSportRulesetAvailable()) {
                                                      return;
                                                    }

//                                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => LeaderBoard()));
                                                    Navigator.of(context).push(MaterialPageRoute(
                                                      builder: (context) => PregameScreen(),
                                                    ));
                                                  },
                                                ),
                                              ),
                                            ],
                                          )
                                        : Container(),

                                    showIAABlogo
                                        ? Column(
                                            children: [
                                              SizedBox(height: 8),
                                              ConstrainedBox(
                                                constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                                child: RaisedButton(
                                                  shape: CommonConstant.roundedRectangleBorder,
                                                  color: lightButtonColor,
                                                  padding: EdgeInsets.all(12),
                                                  child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                                    Text('Inside the lines', style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic)),
                                                  ]),
                                                  onPressed: () async {
                                                    const url = 'https://iaabo.org/ITL.html';
                                                    if (await canLaunch(url)) {
                                                      await launch(url);
                                                    } else {
                                                      MyToast.showToast('Could not launch $url');
                                                    }
                                                  },
                                                ),
                                              ),
                                            ],
                                          )
                                        : Container(),
                                    showNewsLetter
                                        ? Column(
                                            children: <Widget>[
                                              SizedBox(height: 8),
                                              ConstrainedBox(
                                                constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                                child: RaisedButton(
                                                  shape: CommonConstant.roundedRectangleBorder,
                                                  color: lightButtonColor,
                                                  padding: EdgeInsets.all(12),
//                                                  child: Text(
//                                                    'The Match-Up Newsletter',
//                                                    style: TextStyle(fontSize: 16),
//                                                  ),
                                                  child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[Text('The Match-Up ', style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic)), Text("Newsletter", style: TextStyle(fontSize: 16))]),

                                                  onPressed: () async {
                                                    if (_selectedItemSport.value == prefix0.REQUEST_ANOTHER_SPORT) {
                                                      await EmailSender.requestAnotherSport(_selectedItemSport);
                                                      return;
                                                    }
                                                    if (_selectedItemRuleSet.value == prefix0.REQUEST_ANOTHER_RULESET) {
                                                      await EmailSender.requestAnotherRuleSet(_selectedItemRuleSet);
                                                      return;
                                                    }

                                                    if (!isSportRulesetAvailable()) {
                                                      return;
                                                    }

                                                    Navigator.of(context).push(MaterialPageRoute(
                                                      builder: (context) => NewsletterYearScreen(),
                                                    ));
                                                  },
                                                ),
                                              ),
                                            ],
                                          )
                                        : Container(),
                                    SizedBox(height: 8),
                                    ConstrainedBox(
                                      constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                      child: RaisedButton(
                                        shape: CommonConstant.roundedRectangleBorder,
                                        color: lightButtonColor,
                                        padding: EdgeInsets.all(12),
                                        child: Text(
                                          'About',
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        onPressed: () async {
                                          /*try {
                                            final result = await ApiRequest.fetchCategories();
                                            debugPrint(" verify result : ${result}");
                                          }catch(e){
                                            debugPrint("Error : ${e.toString()}");
                                          }*/
                                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => AboutScreen()));
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: Platform.isIOS ? const EdgeInsets.symmetric(vertical: 20) : const EdgeInsets.symmetric(vertical: 0),
                              child: Text("© 2020"),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
  }
  showErrorAlert(BuildContext context,String subType,String sku) {


    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed:  () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("RefHQ",style: TextStyle(
          color: Colors.red
      ),),
      content: /*Text("You have purchased ${subType} subscription. So other quiz are not accecible for this subscription. Please click on subscribe to upgrade subscription.")*/
      RichText(
        text: new TextSpan(
          // Note: Styles for TextSpans must be explicitly defined.
          // Child text spans will inherit styles from parent
          style: new TextStyle(
              fontSize: 18.0,
              color: Colors.white,
              height: 1.5
          ),
          children: <TextSpan>[
            TextSpan(text: 'You do not currently have access to the quiz features for this '),
            TextSpan(text: '${mapSport['name']} / ${mapRuleSet['name']}.', style: new TextStyle(color:Colors.red,fontWeight: FontWeight.bold)),
            // TextSpan(text: ' . If you would like to change your access pass, please click "Manage Subscription"'),

          ],
        ),
      ),
      actions: [
        cancelButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showUpgradeAlert(BuildContext context,String sku) {
    debugPrint("sku : ${sku}");

    String sTypeString;
    if(sku == Utility.productIds[1]){
      sTypeString = 'Single Rule Set';
    }else if(sku == Utility.productIds[2]){
      sTypeString = 'Single Sport';
    }else if(sku == Utility.productIds[3]){
      sTypeString = 'All Access';
    }
    debugPrint('sTypeString : ${sTypeString}');
    // set up the buttons
    Widget cancelButton = FlatButton(
      color: Colors.grey,
      child: Text("Cancel"),
      onPressed:  () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      color: Colors.red,
      child: Text("Manage Subscription",style: TextStyle(
        fontSize: 16
      ),),
      onPressed:  () async{
        final PackageInfo info = await PackageInfo.fromPlatform();
        String packageName = info.packageName;
        DatabaseReference ref = FirebaseDatabase.instance.reference();
        String uid = await Auth().currentUser();
        var users = await ref.child(prefix0.users).child(uid).once();
        UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));
        String launchUrl = Platform.isIOS ? "https://apps.apple.com/account/subscriptions" : "https://play.google.com/store/account/subscriptions?sku=$sku&package=$packageName";
        launch(launchUrl);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      /*title: Text("RefHQ",style: TextStyle(
        color: Colors.red
      ),),*/
      content:
      Container(
        height: 280,
        child: Column(
          children: [
            RichText(
              text: new TextSpan(
                // Note: Styles for TextSpans must be explicitly defined.
                // Child text spans will inherit styles from parent
                style: new TextStyle(
                  fontSize: 18.0,
                  color: Colors.white,
                  height: 1.5
                ),
                children: <TextSpan>[
                  TextSpan(text: 'You are currently subscribed to the $sTypeString '),
                  TextSpan(text: '${mapSport['name']} / ${mapRuleSet['name']}', style: new TextStyle(color:Colors.red,fontWeight: FontWeight.bold)),
                  TextSpan(text: ' access pass. To access the quiz features for other sports and rulesets, Please tap Manage Subscription.'),
                ],

              ),

            ),
            SizedBox(height: 15,),
            Column (
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  continueButton, // button 2
                  SizedBox(height: 5,),
                  cancelButton,
                ]
            )
          ],
        ),
      ),
      /*actions: [
        cancelButton,
        continueButton,
      ],*/
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  bool isSportRulesetAvailable() {
    if (_selectedItemSport.value == prefix0.CHOOSE_SPORT || _selectedItemRuleSet.value == prefix0.CHOOSE_RULESET) {
      if (_selectedItemSport.value == prefix0.CHOOSE_SPORT && _selectedItemRuleSet.value == prefix0.CHOOSE_RULESET) {
        Fluttertoast.showToast(msg: "Select Sport & Rule set", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
      } else if (_selectedItemSport.value == prefix0.CHOOSE_SPORT) {
        Fluttertoast.showToast(msg: "Select Sport", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
      } else if (_selectedItemRuleSet.value == prefix0.CHOOSE_RULESET) {
        Fluttertoast.showToast(msg: "Select Rule set", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1, backgroundColor: Colors.red, textColor: Colors.white, fontSize: 16.0);
      }
      return false;
    } else {
      return true;
    }
  }

  /*void saveSportsAndRulese() async {
    *//*Map<String, dynamic> sport = Sports.toJson(_selectedItemSport);
    Map<String, dynamic> ruleSet = Ruleset.toJson(_selectedItemRuleSet);

    await prefs.setString(AppPreferences.pref_sports, json.encode(sport));
    await prefs.setString(AppPreferences.pref_rule_set, json.encode(ruleSet));
    debugPrint("sport : ${json.decode(prefs.getString(AppPreferences.pref_sports))}");
    debugPrint("ruleset : ${json.decode(prefs.getString(AppPreferences.pref_rule_set))}");*//*

    Map<String, dynamic> mSport = Sports.toJson(_selectedItemSport);
    Map<String, dynamic> mRuleSet = Ruleset.toJson(_selectedItemRuleSet);
    DatabaseReference dbRef = FirebaseDatabase.instance.reference();
    final uId = await Auth().currentUser();

    await dbRef.child(prefix0.users).child(uId).update({
      prefix0.sports: mSport,
      prefix0.ruleset: mRuleSet,
    });
  }*/

  void filterRuleSet() {
    List<Ruleset> newListRuleset = List();
    newListRuleset.add(
      Ruleset(CHOOSE_RULESET, prefix0.str_choose_rule_set, CHOOSE_RULESET, true),
    );
    listDropdownItemsRuleSet.forEach((element) {
      if (element.sports == _selectedItemSport.value) {
        newListRuleset.add(element);
      }
    });
    newListRuleset.add(Ruleset(REQUEST_ANOTHER_RULESET, prefix0.str_request_another_rule_set, REQUEST_ANOTHER_RULESET, true));
    _dropdownMenuItemsRuleSet = buildDropDownMenuItemsRuleSet(newListRuleset);
    _selectedItemRuleSet = _dropdownMenuItemsRuleSet[0].value;
  }


  _getCurrentUser() async {
    DatabaseReference ref = FirebaseDatabase.instance.reference();
    String uid = await Auth().currentUser();
    debugPrint("Current user ID : ${uid}");
    var users = await ref.child(prefix0.users).child(uid).once();
    debugPrint("Current user map : ${users.value}");
    currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));

    // _getFlaggedQuestions();
  }

  Future<void> _getFlaggedQuestions() async {
    List<Question> internalList = List();
    Map<String, dynamic> sport = Sports.toJson(_selectedItemSport);
    Map<String, dynamic> ruleSet = Ruleset.toJson(_selectedItemRuleSet);
    String childName = '${sport['name']} - ${ruleSet['name']}';
    DatabaseReference ref = FirebaseDatabase.instance.reference();
    String uid = await Auth().currentUser();
    debugPrint("Current user ID : ${uid}");
    var mSavedQuestions = await ref.child(prefix0.users).child(uid).child(prefix0.savedQuestions).child(childName).once();
    debugPrint("test ${mSavedQuestions.value}");
    if (mSavedQuestions.value == null) {
      flaggedQuestions = List();
      return;
    }
    Map<String, dynamic> questionMap = Map<String, dynamic>.from(mSavedQuestions.value);
    questionMap.forEach((key, value) {
      internalList.add(Question.flaggedFromJSON(key, Map<String, dynamic>.from(value)));
    });
    for (var item in internalList) {
      item.viewMode = prefix0.ViewMode.ReviewFlagged;
    }
    // flaggedQuestions = internalList;
    List<Question> tmpFlaggedQuestions = internalList;
    flaggedQuestions.clear();
    tmpFlaggedQuestions.forEach((elementQ) {
      List result = flaggedQuestions.where((element) => element.qid == elementQ.qid).toList();
      if (result.length == 0) {
        flaggedQuestions.add(elementQ);
      }
    });

    debugPrint("Flagged Question : ${currentUser.firstName} ${currentUser.lastName}");
    debugPrint("Flagged Question : ${flaggedQuestions.length}");
    // setState(() {

    // });
  }

  void navigateToReviewFlaggedQuestions() async {
    await _getFlaggedQuestions();
    final filteredFlaggedQuestions = flaggedQuestions /*flaggedQuestions.where((element) => element.sport == _selectedItemSport.value && element.ruleset == _selectedItemRuleSet.value).toList()*/;

    Map<String, dynamic> sport = Sports.toJson(_selectedItemSport);
    Map<String, dynamic> ruleSet = Ruleset.toJson(_selectedItemRuleSet);
    // _getFlaggedQuestions();
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => QuizQuestion(
          sport: sport,
          ruleset: ruleSet,
          title: /*_selectedItemSport.name*/ 'Flagged Questions',
          questions: filteredFlaggedQuestions,
          quizType: prefix0.QuizType.Flagged,
          user: currentUser,
        ),
      ),
    );
  }
}

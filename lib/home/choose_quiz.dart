import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ref_hq/auth.dart';
import 'package:ref_hq/constants.dart';
import 'package:ref_hq/data_models/categories_model.dart';
import 'package:ref_hq/data_models/question.dart';
import 'package:ref_hq/data_models/rule_and_questions.dart';
import 'package:ref_hq/data_models/ruleset.dart';
import 'package:ref_hq/data_models/sports.dart';
import 'package:ref_hq/data_models/user_model.dart';
import 'package:ref_hq/home/home_screen.dart';
import 'package:ref_hq/home/quiz_questions.dart';
import 'package:ref_hq/loginorsignup/user_signin.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:ref_hq/utils/common_constant.dart';
import 'package:ref_hq/utils/my_toast.dart';
import 'package:ref_hq/widget/status_bar_height.dart';

class ChooseQuiz extends StatefulWidget {
  final Auth auth = Auth();
  List<Question> flaggedQuestions;
  Sports selectedItemSport;
  Ruleset selectedItemRuleSet;
  UserModel currentUser;

  ChooseQuiz({Key key, this.flaggedQuestions, this.selectedItemSport, this.selectedItemRuleSet, this.currentUser}) : super(key: key);

  @override
  _ChooseQuizState createState() => _ChooseQuizState();
}

enum AuthStatus { notSignedIn, signedIn }

class _ChooseQuizState extends State<ChooseQuiz> {
  List<Question> allQuizQuestions = List();
  // AuthStatus _authStatus = AuthStatus.notSignedIn;
  // List<Rules> rulesList = List();
  List<Categories> categoriesList = List();
  String uid;

 /* void _signedIn() {
    setState(() {
      _authStatus = AuthStatus.signedIn;
    });
  }*/

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        debugPrint("willPopScope called");
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => HomeScreen()), (route) => false);
        return true;
      },
      child: Scaffold(
        body: SafeArea(
          top: false,
          bottom: false,
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/background.jpg'),
                  fit: BoxFit.fill),
            ),
            constraints: BoxConstraints(
              minWidth: MediaQuery.of(context).size.width,
              minHeight: MediaQuery.of(context).size.height,
            ),
            child: Column(
              children: [
                StatusBarHeight(),
                Row(
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => HomeScreen()), (route) => false);
                        },
                        child: SizedBox(
                            width: 55,
                            height: 55,
                            child: Icon(Icons.arrow_back_ios))),
                    Expanded(
                      child: Text("Choose Quiz",textAlign: TextAlign.center,style: TextStyle(
                          fontSize: 20
                      ),),
                    ),SizedBox(
                      width: 55,
                      height: 55,)
                  ],
                ),
                Expanded(
                  child: SingleChildScrollView(
                    physics: ScrollPhysics(),
                    child: Column(
                      children: [
                        StreamBuilder(
                          stream: FirebaseDatabase.instance.reference().onValue,
                          builder: (BuildContext context, AsyncSnapshot<Event> event) {
                            if (!event.hasData) {
                              return LinearProgressIndicator();
                            } else {
                              /*List<Rules> rulesList = new List();
                                  var jsonResponse = Map<String, dynamic>.from(event.data.snapshot.value);
                                  rulesList.clear();
                                  rulesList = getAllRulesList(jsonResponse);
                                  UserModel user = _getUserProfile(jsonResponse);
                                  return ListView.builder(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: (rulesList.length + 2),
                                    itemBuilder: (context, index) => _buildListItem(rulesList, index, user, jsonResponse),
                                  );*/

                              List<Categories> catList = new List();
                              var jsonResponse = Map<String, dynamic>.from(event.data.snapshot.value);
                              catList.clear();
                              catList = getAllCategoryList(jsonResponse);
                              UserModel user = _getUserProfile(jsonResponse);
                              return ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: (catList.length + 2),
                                itemBuilder: (context, index) => _buildListItem(catList, index, user, jsonResponse),
                              );
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  String mSportRulesetStreak;
  @override
  void initState() {
    super.initState();
    mSportRulesetStreak = '${widget.selectedItemSport.name} - ${widget.selectedItemRuleSet.name}';
    widget.auth.currentUser().then((userId) {
      setState(() {
        uid = userId;
      });
    });
  }

  /*List<Rules> getAllRulesList(Map<String, dynamic> jsonResponse) {
    rulesList.clear();

    RulesList internalRuleList = RulesList.fromJSON(jsonResponse);
    rulesList.addAll(internalRuleList.rulesList);
    return rulesList;
  }*/

  List<Categories> getAllCategoryList(Map<String, dynamic> jsonResponse) {
    categoriesList.clear();

    final catNew = jsonResponse['categories_sports_rulset_wise'];
    List<dynamic> jsonNewResp = catNew['${widget.selectedItemSport.name} - ${widget.selectedItemRuleSet.name} Categories'] as List;

    if(jsonNewResp==null || jsonNewResp.length==0){
      return List<Categories>();
    }

    CategoryList internalRuleList = CategoryList.fromList(jsonNewResp);
    categoriesList.addAll(internalRuleList.categoryList);
    return categoriesList;
  }

  _buildListItem(List<Categories> catList, int index, UserModel user, Map<String, dynamic> jsonResponse) {
    String title;
    if (index > 1) title = catList[index - 2].title;
    switch (index) {
      case 0:
        return Padding(
          padding: const EdgeInsets.only(left: 8,bottom: 8,right: 8),
          child: InkWell(
            onTap: () async {
              /*Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) =>
                      QuizQuestion(
                        title: 'QUIZ ALL QUESTIONS',
                        questions: _gatherAllQuizQuestions(),
                        quizType: QuizType.All,
                        user: _getUserProfile(jsonResponse),
                      ),
                ),
              );*/

              String childName = '${widget.selectedItemSport.name} - ${widget.selectedItemRuleSet.name}';
              List<Question> allQuizQuestions = List();
              DataSnapshot snapshotQuestionCat = await FirebaseDatabase.instance.reference()
                  .child('questions_sports_rulset_wise')
                  .child(childName)
                  // .limitToFirst(10)
                  .once();

              // DataSnapshot snapshotQuestionCat = await FirebaseDatabase.instance.reference().child('questions').once();
              List<dynamic> valuesQueCat = snapshotQuestionCat.value;
              if(valuesQueCat == null){
                MyToast.showToast("No Questions");
              }
              allQuizQuestions = valuesQueCat.map((data) => Question.fromJSON(Map<String, dynamic>.from(data))).toList();
              /*allQuizQuestions.clear();
              for (var rule in valuesQueCat) {
                debugPrint("rule : ${rule}");
                var title = rule['title'];
                debugPrint("title : ${title}");
                List<dynamic> mQuestion = rule['questions'];
                if (mQuestion != null && mQuestion.length > 0) {
                  allQuizQuestions.addAll(_fetchQuestionsForTheTitle(title));
                }
              }*/
              debugPrint("total question before filter : ${allQuizQuestions.length}");
              /*final mSports = Sports.toJson(widget.selectedItemSport);
              final mRuleset = Ruleset.toJson(widget.selectedItemRuleSet);
              List<Question> temp = allQuizQuestions.where((element) => element.sport == mSports[prefix0.value] && element.ruleset == mRuleset[prefix0.value]).toList();

              allQuizQuestions = temp;*/
              debugPrint("total question : ${allQuizQuestions.length}");
              // return;

              DatabaseReference ref = FirebaseDatabase.instance.reference();
              String uid = await Auth().currentUser();
              var users = await ref.child(prefix0.users).child(uid).once();
              UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));

              // _isLoadingSportsAndRuleSet = false;
              allQuizQuestions.shuffle();

              QuizQuestion quizQuestionRoute = QuizQuestion(
                sport: Sports.toJson(widget.selectedItemSport),
                ruleset: Ruleset.toJson(widget.selectedItemRuleSet),
                title: /*sport['name']*/ 'QUIZ ALL QUESTIONS',
                questions: allQuizQuestions,
                quizType: QuizType.All,
                user: currentUser,
              );

              Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => quizQuestionRoute),
              );
              return;
            },
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(10),
                  // border: Border.all(width: 1),
                  color: Theme.of(context).accentColor
              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Column(
                // alignment: Alignment.center,
                children: <Widget>[
                  Text(
                    'Quiz All Questions',
                    style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ( (user.sport_ruleset_streak!=null) && (user.sport_ruleset_streak[mSportRulesetStreak] != null) && (user.sport_ruleset_streak[mSportRulesetStreak][currentStreak]) != null)?
                      Text("Current Streak: ${user.sport_ruleset_streak[mSportRulesetStreak][currentStreak]}"):Text("Current Streak: 0"),
                      ( (user.sport_ruleset_streak!=null) && (user.sport_ruleset_streak[mSportRulesetStreak] != null) && (user.sport_ruleset_streak[mSportRulesetStreak][streak]) != null)?
                      Text("My Record: ${user.sport_ruleset_streak[mSportRulesetStreak][streak]}"):Text("My Record: 0")
                    ],
                  )
                ],
              ),
            ),
          ),
        );
        break;
      case 1:
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: ConstrainedBox(
            constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width, minHeight: 50),
            child: RaisedButton(
              shape: CommonConstant.roundedRectangleBorder,
              color: Colors.grey[600],
              onPressed: () {
                final filteredFlaggedQuestions = widget.flaggedQuestions;/*widget.flaggedQuestions.where((element) => element.sport == widget.selectedItemSport.value && element.ruleset == widget.selectedItemRuleSet.value).toList()*/;

                Map<String, dynamic> sport = Sports.toJson(widget.selectedItemSport);
                Map<String, dynamic> ruleSet = Ruleset.toJson(widget.selectedItemRuleSet);
                // _getFlaggedQuestions();
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => QuizQuestion(
                      sport: sport,
                      ruleset: ruleSet,
                      title: /*_selectedItemSport.name*/ 'Flagged Questions',
                      questions: filteredFlaggedQuestions,
                      quizType: prefix0.QuizType.Flagged,
                      user: widget.currentUser,
                    ),
                  ),
                );
              },
              child: FittedBox(fit: BoxFit.fitWidth, child: Text('Quiz Flagged Questions',style: TextStyle(fontSize: 16))),
            ),
          ),
        );
        break;
      default:
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: ConstrainedBox(
            constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width, minHeight: 50),
            child: RaisedButton(
              shape: CommonConstant.roundedRectangleBorder,
              color: Colors.grey[800],
              onPressed: () async {
                int realIndex = index - 2;
                String searchCatId = '${catList[realIndex].id}';
                debugPrint("searchCatId : ${catList[realIndex].id}");
                /*Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) =>
                        QuizQuestion(
                          title: title,
                          questions: _fetchQuestionsForTheTitle(title),
                          quizType: QuizType.Rulewise,
                          user: _getUserProfile(jsonResponse),
                        ),
                  ),
                );*/
                String childName = '${widget.selectedItemSport.name} - ${widget.selectedItemRuleSet.name}';
                List<Question> allQuizQuestions = List();
                DataSnapshot snapshotQuestionCat = await FirebaseDatabase.instance.reference()
                    .child('questions_sports_rulset_wise')
                    .child(childName).once();
                List<dynamic> valuesQueCat = snapshotQuestionCat.value;
                debugPrint("total questions : ${valuesQueCat.length}");
                debugPrint("allQuestions : ${valuesQueCat}");
                valuesQueCat.forEach((elementQuestion) {
                  String strCategory = elementQuestion['Category'];
                  List<String> spliteResult = strCategory.split(',');
                  debugPrint('strCategory : ${strCategory}');
                  spliteResult.forEach((elementCategory) {
                    String trimmedString = elementCategory.trim();
                    String searchQuestion = trimmedString;
                    if(searchQuestion == searchCatId){
                      Question question = Question.fromDynamicJSON(elementQuestion);
                      allQuizQuestions.add(question);
                    }
                  });
                  debugPrint("catID : ${catList[realIndex].id}  -  title ${catList[realIndex].title}  -  spliteResult : ${spliteResult}");
                });
                debugPrint("Filtered question : ${allQuizQuestions.length}");
                if(allQuizQuestions.length>0){
                  debugPrint("total question before filter : ${allQuizQuestions.length}");
                  /*final mSports = Sports.toJson(widget.selectedItemSport);
                  final mRuleset = Ruleset.toJson(widget.selectedItemRuleSet);
                  List<Question> temp = allQuizQuestions.where((element) => element.sport == mSports[prefix0.value] && element.ruleset == mRuleset[prefix0.value]).toList();

                  allQuizQuestions = temp;*/
                  debugPrint("total question : ${allQuizQuestions.length}");

                  if (allQuizQuestions.length > 0) {
                    DatabaseReference ref = FirebaseDatabase.instance.reference();
                    String uid = await Auth().currentUser();
                    var users = await ref.child(prefix0.users).child(uid).once();
                    UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));

                    // _isLoadingSportsAndRuleSet = false;
                    allQuizQuestions.shuffle();

                    QuizQuestion quizQuestionRoute = QuizQuestion(
                      sport: Sports.toJson(widget.selectedItemSport),
                      ruleset: Ruleset.toJson(widget.selectedItemRuleSet),
                      title: title /*'QUIZ ALL QUESTIONS'*/,
                      questions: allQuizQuestions,
                      quizType: QuizType.Rulewise,
                      user: currentUser,
                    );

                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => quizQuestionRoute),
                    );
                  }else{
                    MyToast.showToast("No Questions");
                  }
                }else{
                  Fluttertoast.showToast(msg: "No questions available for this category");
                }
                return;

                /*DataSnapshot */snapshotQuestionCat = await FirebaseDatabase.instance.reference().child(prefix0.rules_category).once();
                /*List<dynamic> */valuesQueCat = snapshotQuestionCat.value;
                debugPrint("valuesQueCat : ${valuesQueCat}");
                // List<Question> allQuizQuestions = valuesQueCat.map((data) => Question.fromJSON(Map<String, dynamic>.from(data))).toList();
                /*List<Question>*/ allQuizQuestions = List();
                allQuizQuestions.clear();
                List<dynamic> mCatQ = valuesQueCat.where((element) => element['title'] == title).toList();
                debugPrint("mCatQ : ${mCatQ}");

                if (mCatQ.length > 0) {
                  Map<dynamic, dynamic> mapCat = mCatQ[0];
                  debugPrint("mapCat : ${mapCat}");
                  List<dynamic> correspondingJson = mapCat['questions'];
                  if (correspondingJson != null && correspondingJson.length > 0) {
                    debugPrint("correspondingJson : ${correspondingJson}");
                    allQuizQuestions = _parseQuestionJson(correspondingJson);
                    debugPrint("total question before filter : ${allQuizQuestions.length}");
                    final mSports = Sports.toJson(widget.selectedItemSport);
                    final mRuleset = Ruleset.toJson(widget.selectedItemRuleSet);
                    List<Question> temp = allQuizQuestions.where((element) => element.sport == mSports[prefix0.value] && element.ruleset == mRuleset[prefix0.value]).toList();

                    allQuizQuestions = temp;
                    debugPrint("total question : ${allQuizQuestions.length}");

                    if (allQuizQuestions.length > 0) {
                      DatabaseReference ref = FirebaseDatabase.instance.reference();
                      String uid = await Auth().currentUser();
                      var users = await ref.child(prefix0.users).child(uid).once();
                      UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));

                      // _isLoadingSportsAndRuleSet = false;
                      allQuizQuestions.shuffle();

                      QuizQuestion quizQuestionRoute = QuizQuestion(
                        sport: Sports.toJson(widget.selectedItemSport),
                        ruleset: Ruleset.toJson(widget.selectedItemRuleSet),
                        title: title /*'QUIZ ALL QUESTIONS'*/,
                        questions: allQuizQuestions,
                        quizType: QuizType.Rulewise,
                        user: currentUser,
                      );

                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => quizQuestionRoute),
                      );
                    }else{
                      MyToast.showToast("No Questions");
                    }
                  } else {
                    MyToast.showToast("No Questions");
                  }
                }
              },
              child: FittedBox(fit: BoxFit.fitWidth, child: Text(title,style: TextStyle(fontSize: 16))),
            ),
          ),
        );
        break;
    }
  }

 /* _buildListItem(List<Rules> rulesList, int index, UserModel user, Map<String, dynamic> jsonResponse) {
    String title;
    if (index > 1) title = rulesList[index - 2].title;
    switch (index) {
      case 0:
        return Padding(
          padding: const EdgeInsets.only(left: 8,bottom: 8,right: 8),
          child: InkWell(
            onTap: () async {
              *//*Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) =>
                      QuizQuestion(
                        title: 'QUIZ ALL QUESTIONS',
                        questions: _gatherAllQuizQuestions(),
                        quizType: QuizType.All,
                        user: _getUserProfile(jsonResponse),
                      ),
                ),
              );*//*

              DataSnapshot snapshotQuestionCat = await FirebaseDatabase.instance.reference().child(prefix0.rules_category).once();
              List<dynamic> valuesQueCat = snapshotQuestionCat.value;

              // List<Question> allQuizQuestions = valuesQueCat.map((data) => Question.fromJSON(Map<String, dynamic>.from(data))).toList();
              List<Question> allQuizQuestions = List();
              allQuizQuestions.clear();
              for (var rule in valuesQueCat) {
                debugPrint("rule : ${rule}");
                var title = rule['title'];
                debugPrint("title : ${title}");
                List<dynamic> mQuestion = rule['questions'];
                if (mQuestion != null && mQuestion.length > 0) {
                  allQuizQuestions.addAll(_fetchQuestionsForTheTitle(title));
                }
              }
              debugPrint("total question before filter : ${allQuizQuestions.length}");
              final mSports = Sports.toJson(widget.selectedItemSport);
              final mRuleset = Ruleset.toJson(widget.selectedItemRuleSet);
              List<Question> temp = allQuizQuestions.where((element) => element.sport == mSports[prefix0.value] && element.ruleset == mRuleset[prefix0.value]).toList();

              allQuizQuestions = temp;
              debugPrint("total question : ${allQuizQuestions.length}");
              // return;

              DatabaseReference ref = FirebaseDatabase.instance.reference();
              String uid = await Auth().currentUser();
              var users = await ref.child(prefix0.users).child(uid).once();
              UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));

              // _isLoadingSportsAndRuleSet = false;
              allQuizQuestions.shuffle();

              QuizQuestion quizQuestionRoute = QuizQuestion(
                sport: Sports.toJson(widget.selectedItemSport),
                ruleset: Ruleset.toJson(widget.selectedItemRuleSet),
                title: *//*sport['name']*//* 'QUIZ ALL QUESTIONS',
                questions: allQuizQuestions,
                quizType: QuizType.All,
                user: currentUser,
              );

              Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => quizQuestionRoute),
              );
              return;
            },
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(10),
                // border: Border.all(width: 1),
                color: Colors.red
              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Column(
                // alignment: Alignment.center,
                children: <Widget>[
                  Text(
                    'Quiz All Questions',
                    style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Current Streak: ${user.currentStreak}"),
                      Text("My Record: ${user.streak}")
                    ],
                  )
                ],
              ),
            ),
          ),
        );
        break;
      case 1:
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: ConstrainedBox(
            constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width, minHeight: 50),
            child: RaisedButton(
              shape: CommonConstant.roundedRectangleBorder,
              color: Colors.grey[600],
              onPressed: () {
                final filteredFlaggedQuestions = widget.flaggedQuestions.where((element) => element.sport == widget.selectedItemSport.value && element.ruleset == widget.selectedItemRuleSet.value).toList();

                Map<String, dynamic> sport = Sports.toJson(widget.selectedItemSport);
                Map<String, dynamic> ruleSet = Ruleset.toJson(widget.selectedItemRuleSet);
                // _getFlaggedQuestions();
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => QuizQuestion(
                      sport: sport,
                      ruleset: ruleSet,
                      title: *//*_selectedItemSport.name*//* 'Flagged Questions',
                      questions: filteredFlaggedQuestions,
                      quizType: prefix0.QuizType.Flagged,
                      user: widget.currentUser,
                    ),
                  ),
                );
              },
              child: FittedBox(fit: BoxFit.fitWidth, child: Text('Quiz Flagged Questions',style: TextStyle(fontSize: 16))),
            ),
          ),
        );
        break;
      default:
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: ConstrainedBox(
            constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width, minHeight: 50),
            child: RaisedButton(
              shape: CommonConstant.roundedRectangleBorder,
              color: Colors.grey[800],
              onPressed: () async {
                *//*Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) =>
                        QuizQuestion(
                          title: title,
                          questions: _fetchQuestionsForTheTitle(title),
                          quizType: QuizType.Rulewise,
                          user: _getUserProfile(jsonResponse),
                        ),
                  ),
                );*//*
                DataSnapshot snapshotQuestionCat = await FirebaseDatabase.instance.reference().child(prefix0.rules_category).once();
                List<dynamic> valuesQueCat = snapshotQuestionCat.value;

                // List<Question> allQuizQuestions = valuesQueCat.map((data) => Question.fromJSON(Map<String, dynamic>.from(data))).toList();
                List<Question> allQuizQuestions = List();
                allQuizQuestions.clear();
                List<dynamic> mCatQ = valuesQueCat.where((element) => element['title'] == title).toList();
                debugPrint("mCatQ : ${mCatQ}");

                if (mCatQ.length > 0) {
                  Map<dynamic, dynamic> mapCat = mCatQ[0];
                  debugPrint("mapCat : ${mapCat}");
                  List<dynamic> correspondingJson = mapCat['questions'];
                  if (correspondingJson != null && correspondingJson.length > 0) {
                    debugPrint("correspondingJson : ${correspondingJson}");
                    allQuizQuestions = _parseQuestionJson(correspondingJson);
                    debugPrint("total question before filter : ${allQuizQuestions.length}");
                    final mSports = Sports.toJson(widget.selectedItemSport);
                    final mRuleset = Ruleset.toJson(widget.selectedItemRuleSet);
                    List<Question> temp = allQuizQuestions.where((element) => element.sport == mSports[prefix0.value] && element.ruleset == mRuleset[prefix0.value]).toList();

                    allQuizQuestions = temp;
                    debugPrint("total question : ${allQuizQuestions.length}");

                    if (allQuizQuestions.length > 0) {
                      DatabaseReference ref = FirebaseDatabase.instance.reference();
                      String uid = await Auth().currentUser();
                      var users = await ref.child(prefix0.users).child(uid).once();
                      UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));

                      // _isLoadingSportsAndRuleSet = false;
                      allQuizQuestions.shuffle();

                      QuizQuestion quizQuestionRoute = QuizQuestion(
                        sport: Sports.toJson(widget.selectedItemSport),
                        ruleset: Ruleset.toJson(widget.selectedItemRuleSet),
                        title: title *//*'QUIZ ALL QUESTIONS'*//*,
                        questions: allQuizQuestions,
                        quizType: QuizType.Rulewise,
                        user: currentUser,
                      );

                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => quizQuestionRoute),
                      );
                    }else{
                      MyToast.showToast("No Questions");
                    }
                  } else {
                    MyToast.showToast("No Questions");
                  }
                }
              },
              child: FittedBox(fit: BoxFit.fitWidth, child: Text(title,style: TextStyle(fontSize: 16))),
            ),
          ),
        );
        break;
    }
  }*/

  /*_fetchQuestionsForTheTitle(String ruleTitle) {
    var correspondingJson = getCorrespondingJson(ruleTitle);
    List<Question> questionList = _parseQuestionJson(correspondingJson);
    questionList.shuffle();
    return questionList;
  }*/

  /*getCorrespondingJson(String ruleTitle) {
    dynamic correspondingQuestionJson;
    for (var rule in categoriesList) {
      if (rule.title == ruleTitle) {
        correspondingQuestionJson = rule.questions;
        return correspondingQuestionJson;
      }
    }
  }*/

  List<Question> _parseQuestionJson(correspondingJson) {
    QuestionList questions = QuestionList.fromJSON(correspondingJson);
    return questions.questionList;
  }

  UserModel _getUserProfile(Map<String, dynamic> jsonResponse) {
    var userMap = Map<String, dynamic>.from(jsonResponse[users]);
    Map<String, dynamic> userJson;
    userMap.forEach((key, value) {
      if (key == uid) {
        userJson = Map<String, dynamic>.from(value);
      }
    });
    return UserModel.fromSnapshot(uid, userJson);
  }

  /*List<Question> _gatherAllQuizQuestions() {
    allQuizQuestions.clear();
    for (var rule in rulesList) {
      var title = rule.title;
      allQuizQuestions.addAll(_fetchQuestionsForTheTitle(title));
    }
    allQuizQuestions.shuffle();
    return allQuizQuestions;
  }*/

  List<Question> _gatherFlaggedQuizQuestions(UserModel user) {
    List<Question> internalQuestionList = List();
    if (user.savedQuestions != null) {
      Map<String, dynamic> flaggedQuestionMap = Map<String, dynamic>.from(user.savedQuestions);
      flaggedQuestionMap.forEach((key, value) {
        internalQuestionList.add(Question.flaggedFromJSON(key, Map<String, dynamic>.from(value)));
      });
    }
    return internalQuestionList;
  }
}

import 'dart:convert';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:photo_view/photo_view.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:ref_hq/constants.dart';
import 'package:ref_hq/data_models/ruleset.dart';
import 'package:ref_hq/data_models/sports.dart';
import 'package:ref_hq/data_models/user_model.dart';
import 'package:ref_hq/home/fill_screenview_from_network.dart';
import 'package:ref_hq/misc_widgets.dart';
import 'package:ref_hq/ref_theme.dart';
import 'package:ref_hq/state_container/state_container.dart';
import 'package:ref_hq/utils/app_preferences.dart';
import 'package:ref_hq/utils/my_colors.dart';
import 'package:ref_hq/utils/utility.dart';
import 'package:ref_hq/widget/status_bar_height.dart';


class ViewPregameCards extends StatefulWidget {
  String title;
  List<dynamic> listCards;
  ViewPregameCards({this.title,this.listCards});
  @override
  _ViewPregameCardsState createState() => _ViewPregameCardsState();
}

class _ViewPregameCardsState extends State<ViewPregameCards> {
  bool _isLoading = true;
  int scrollIndex = 0;
  @override
  void initState() {
    FirebaseAnalytics().setAnalyticsCollectionEnabled(true);
    super.initState();
    debugPrint("Cards : ${widget.listCards}");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: false,
        bottom: false,
        child: SingleChildScrollView(
          child: Container(
            // color: Colors.black,
            decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage('assets/images/background.jpg'), fit: BoxFit.fill),
            ),
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height,
              minWidth: MediaQuery.of(context).size.width,
            ),
            child: Column(
              children: [
                StatusBarHeight(),
                Row(
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: SizedBox(
                            width: 55,
                            height: 55,
                            child: Icon(Icons.arrow_back_ios))),
                    Expanded(
                      child: Text(widget.title,textAlign: TextAlign.center,style: TextStyle(
                          fontSize: 20
                      ),),
                    ),SizedBox(
                      width: 55,
                      height: 55,)
                  ],
                ),
                CarouselSlider(
                  options: CarouselOptions(
                    autoPlay: false,
                    enlargeCenterPage: true,
                    viewportFraction: 1,
                    aspectRatio: 2.0,
                    initialPage: 0,
                    height: MediaQuery.of(context).size.height,
                    enableInfiniteScroll: false,
                    onPageChanged: (index, reason) {
                      scrollIndex = index;
                      // mScrollController.animateTo(index * MediaQuery.of(context).size.width/4.5, duration: new Duration(seconds: 2), curve: Curves.ease);
                    },
                  ),
                  items: widget.listCards.asMap().entries.map((entries) {
                    int index = entries.key;
                    return Builder(
                      builder: (context) {
                        return Container(
                          color: Colors.black,
                          constraints: BoxConstraints(
                            minHeight: MediaQuery.of(context).size.height,
                            minWidth: MediaQuery.of(context).size.width,
                          ),
                          child: Stack(
                            children: [
                              Container(
                                  constraints: BoxConstraints(
                                    minHeight: MediaQuery.of(context).size.height,
                                    minWidth: MediaQuery.of(context).size.width,
                                  ),
                                  child: Center(child: CircularProgressIndicator())
                              ),
                              PhotoView(
                                onTapDown: (context, details, controllerValue) {
                                  debugPrint("onTapDown : ${widget.listCards}");
                                  List<String> mListCards = List();
                                  widget.listCards.forEach((element) {
                                    mListCards.add(element['url']);
                                  });
                                  debugPrint("mListCards : ${mListCards.toString()}");
                                  Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => FullScreenViewFromNetwork(listSlides: mListCards,index: scrollIndex,),
                                  ));
                                },
                                imageProvider: NetworkImage(entries.value['url'])/*FancyShimmerImage(
                                  imageUrl: entries.value['url'],
                                  shimmerBaseColor: Colors.white,
                                  shimmerHighlightColor:Colors.grey,
                                  // config.Colors().mainColor(1),
                                  shimmerBackColor: Colors.green,
                                  boxFit: BoxFit.cover,
                                )*/,
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  }).toList()
                  ,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}

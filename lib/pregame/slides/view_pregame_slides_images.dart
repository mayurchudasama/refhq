import 'package:carousel_slider/carousel_slider.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:ref_hq/home/fill_screenview_from_local.dart';
import 'package:ref_hq/home/fill_screenview_from_network.dart';
import 'package:ref_hq/utils/my_toast.dart';
import 'package:ref_hq/widget/status_bar_height.dart';


class ViewPregameSlidesImages extends StatefulWidget {
  String title;
  List<dynamic> listSlides;
  ViewPregameSlidesImages({this.title,this.listSlides});
  @override
  _ViewPregameSlidesImagesState createState() => _ViewPregameSlidesImagesState();
}

class _ViewPregameSlidesImagesState extends State<ViewPregameSlidesImages> {
  bool _isLoading = true;
  int scrollIndex = 0;
  ScrollController mScrollController = ScrollController();
  CarouselController buttonCarouselController = CarouselController();
  @override
  void initState() {
    FirebaseAnalytics().setAnalyticsCollectionEnabled(true);
    super.initState();
    debugPrint("Slides : ${widget.listSlides}");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: SafeArea(
        top: false,
        bottom: false,
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage('assets/images/background.jpg'), fit: BoxFit.fill),
          ),
          constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height,
            minWidth: MediaQuery.of(context).size.width,
          ),
          child: Column(
            children: [
              StatusBarHeight(),
              Row(
                children: [
                  GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: SizedBox(
                          width: 55,
                          height: 55,
                          child: Icon(Icons.arrow_back_ios))),
                  Expanded(
                    child: Text(widget.title,textAlign: TextAlign.center,style: TextStyle(
                        fontSize: 20
                    ),),
                  ),SizedBox(
                    width: 55,
                    height: 55,)
                ],
              ),
              Expanded(
                flex: 8,
                child: CarouselSlider(
                  carouselController: buttonCarouselController,
                  options: CarouselOptions(
                    autoPlay: false,
                    enlargeCenterPage: true,
                    viewportFraction: 1,
                    aspectRatio: 1.0,
                    initialPage: 0,
                    height: MediaQuery.of(context).size.height,
                    enableInfiniteScroll: false,
                    onPageChanged: (index, reason) {
                      setState(() {
                        debugPrint("pageChanged ${scrollIndex}");
                        scrollIndex = index;
                        mScrollController.animateTo(index * ((MediaQuery.of(context).size.width/4.5)+20), duration: new Duration(milliseconds: 500), curve: Curves.ease);
                      });

                    },
                  ),
                  // aspectRatio: 2.0,
                  items: widget.listSlides.asMap().entries.map((entries) {
                    int index = entries.key;
                    return Builder(
                      builder: (context) {
                        return Container(
                          color: Colors.black,
                          constraints: BoxConstraints(
                            minHeight: MediaQuery.of(context).size.height,
                            minWidth: MediaQuery.of(context).size.width,
                          ),
                          child: Stack(
                            children: [
                              Container(
                                  constraints: BoxConstraints(
                                    minHeight: MediaQuery.of(context).size.height,
                                    minWidth: MediaQuery.of(context).size.width,
                                  ),
                                  child: Center(child: CircularProgressIndicator())
                              ),
                              PhotoView(
                                onTapDown: (context, details, controllerValue) {

                                  Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => FullScreenViewFromNetwork(listSlides: widget.listSlides,index: scrollIndex,),
                                  ));
                                },
                                imageProvider: NetworkImage(entries.value),
                              ),
                              /*FancyShimmerImage(
                                  imageUrl: entries.value['url'],
                                  shimmerBaseColor: Colors.white,
                                  shimmerHighlightColor:Colors.grey,
                                  // config.Colors().mainColor(1),
                                  shimmerBackColor: Colors.green,
                                  boxFit: BoxFit.cover,
                                )*/
                            ],
                          ),
                        );
                      },
                    );
                  }).toList()
                  ,
                ),
              ),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10,right: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      RaisedButton(
                        onPressed: () => buttonCarouselController.previousPage(
                            duration: Duration(milliseconds: 300), curve: Curves.linear),
                        child: Icon(Icons.arrow_back),
                      ),
                      Text('${scrollIndex+1} / ${widget.listSlides.length}'),
                      RaisedButton(
                        onPressed: () => buttonCarouselController.nextPage(
                            duration: Duration(milliseconds: 300), curve: Curves.linear),
                        child: Icon(Icons.arrow_forward),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  controller: mScrollController,
                    itemBuilder:(context, index) {
                      return GestureDetector(
                        onTap: (){
                          setState(() {
                            scrollIndex = index;
                            debugPrint("onClick ${scrollIndex}");
                            buttonCarouselController.jumpToPage(index);
                          });

                        },
                        child: (index == scrollIndex)?Container(
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.red,width: 2)
                          ),
                          margin: EdgeInsets.all(5),
                          width: MediaQuery.of(context).size.width/4.5,
                          height: double.infinity,
                          child: FancyShimmerImage(
                            imageUrl: widget.listSlides[index],
                            shimmerBaseColor: Colors.white,
                            shimmerHighlightColor:Colors.grey,
                            // config.Colors().mainColor(1),
                            shimmerBackColor: Colors.green,
                            boxFit: BoxFit.fill,
                          ),
                        ):Container(
                          margin: EdgeInsets.all(5),
                          color: Colors.white,
                          width: MediaQuery.of(context).size.width/4.5,
                          height: double.infinity,
                          child: FancyShimmerImage(
                            imageUrl: widget.listSlides[index],
                            shimmerBaseColor: Colors.white,
                            shimmerHighlightColor:Colors.grey,
                            // config.Colors().mainColor(1),
                            shimmerBackColor: Colors.green,
                            boxFit: BoxFit.fill,
                          ),
                        ),
                      );
                    } ,
                    separatorBuilder:(context, index) {
                      return SizedBox(width: 10);
                    } ,
                    itemCount: widget.listSlides.length
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

}

import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:ref_hq/data_models/user_model.dart';
import 'package:ref_hq/pregame/cards/view_pregame_cards.dart';
import 'package:ref_hq/pregame/notes/pregame_notes_folder_screen.dart';
import 'package:ref_hq/pregame/slides/view_pregame_slides_images.dart';
import 'package:ref_hq/pregame/uniforms_equipment_apparel/view_uniforms_equipment_apparel.dart';
import 'package:ref_hq/ref_theme.dart';
import 'package:ref_hq/utils/my_colors.dart';
import 'package:ref_hq/widget/display_sports_ruleset_view.dart';
import 'package:ref_hq/widget/status_bar_height.dart';

import '../auth.dart';

class PregameScreen extends StatefulWidget {
  @override
  _PregameScreenState createState() => _PregameScreenState();
}

class _PregameScreenState extends State<PregameScreen> {
  UserModel currentUser = UserModel();

  Map<dynamic, dynamic> mapSport = Map();
  Map<dynamic, dynamic> mapRuleSet = Map();

  @override
  void initState() {
    FirebaseAnalytics().setAnalyticsCollectionEnabled(true);
    super.initState();

    initPrefs();
  }

  bool displayScreenIfBasketballNFHS = false;
  void initPrefs() async {
    setState(() {
      isGetting = true;
    });

    DatabaseReference ref = FirebaseDatabase.instance.reference();
    String uid = await Auth().currentUser();
    var users = await ref.child(prefix0.users).child(uid).once();
    UserModel currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));
    // prefSports = prefs.getString(AppPreferences.pref_sports);
    // mapSport = json.decode(prefSports);
    mapSport = currentUser.sports;

    // prefRuleSet = prefs.getString(AppPreferences.pref_rule_set);
    // mapRuleSet = json.decode(prefRuleSet);
    mapRuleSet = currentUser.ruleset;
    if(this.mapRuleSet[prefix0.sports] == 0 && this.mapRuleSet[prefix0.value] == 2){
      displayScreenIfBasketballNFHS = true;
    }
    else{
      displayScreenIfBasketballNFHS = false;
    }
    setState(() {

    });
    if (displayScreenIfBasketballNFHS) {
      await getPregameSlidesAndCard();
      setState(() {
        isGetting = false;
      });
    }
    setState(() {
      isGetting = false;
    });
  }

  bool isGetting = false;
  List<dynamic> listPregameSlides = List();

  Future<void> getPregameSlidesAndCard() async {
    DataSnapshot snapshotSlides = await FirebaseDatabase.instance.reference().child(prefix0.pregame_slides).once();
    List<dynamic> valuesSlides = snapshotSlides.value;
    listPregameSlides.clear();
    valuesSlides.forEach((element) {
      debugPrint("values slides : ${element.toString()}");
      listPregameSlides.add(element);
    });
  }

  @override
  Widget build(BuildContext context) {
    _getCurrentUser();
    double heightProgress = 4.0;
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: SafeArea(
        top: false,
        bottom: false,
        child: Container(
          constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height,
            minWidth: MediaQuery.of(context).size.width,
          ),
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage('assets/images/background.jpg'), fit: BoxFit.fill),
          ),
          child: Column(
            children: [
              StatusBarHeight(),
              Row(
                children: [
                  GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: SizedBox(width: 55, height: 55, child: Icon(Icons.arrow_back_ios))),
                  Expanded(
                    child: Text(
                      "Pregame",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  SizedBox(
                    width: 55,
                    height: 55,
                  )
                ],
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      isGetting
                          ? Container(height: heightProgress, child: LinearProgressIndicator())
                          : Container(
                              height: heightProgress,
                            ),
                      isGetting
                          ? Container()
                          : DisplaySportsRuleSetView(
                              mapSport: mapSport,
                              mapRuleSet: mapRuleSet,
                            ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 30,right: 30),
                        child: Column(
                          children: [
                            ConstrainedBox(
                              constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width, minHeight: 60),
                              child: RaisedButton(
                                color: MyColors.pregameButtonColor,
                                child: Row(
                                  children: <Widget>[
//                        Image.asset('assets/icon/icon_folder.png'),
                                    Container(height: 30, alignment: Alignment.center, child: Image.asset('assets/icon/icon_folder.png')),
                                    Expanded(
                                      child: Text(
                                        'Pregame Notes Folder',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 18),
                                      ),
                                    ),
                                  ],
                                ),
                                onPressed: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) => PregameNotesFolderScreen(),
                                    ),
                                  );
                                },
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),

                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 30, right: 30),
                        child: Column(
                          children: [

                            displayScreenIfBasketballNFHS?ConstrainedBox(
                              constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width, minHeight: 60),
                              child: RaisedButton(
                                color: lightButtonColor,
                                padding: EdgeInsets.all(16),
                                child: Text(
                                  '2020-2021 Uniforms, Equipment & Apparel',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 18),
                                ),
                                onPressed: () async {
                                  navigateToUniformsEquipmentApparel();
                                },
                              ),
                            ):Container(),
                            SizedBox(
                              height: 10,
                            ),
                            isGetting
                                ? Container()
                                : displayScreenIfBasketballNFHS?Column(
                                    children: listPregameSlides.map((e) {
                                      return Column(
                                        children: [
                                          ConstrainedBox(
                                            constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width, minHeight: 60),
                                            child: RaisedButton(
                                              color: MyColors.pregameSlidesCardButtonColor,
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Text(
                                                      e['title'],
                                                      textAlign: TextAlign.center,
                                                      style: TextStyle(fontSize: 18),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              onPressed: () async {
                                                String type = e['type'];

                                                if (type == 'slides') {
                                                  Navigator.of(context).push(MaterialPageRoute(
                                                    // builder: (context) => ViewPregameSlides(title: e['title'],slideUrl: e['slideLink'],),
                                                    builder: (context) => ViewPregameSlidesImages(
                                                      title: e['title'],
                                                      listSlides: e['slides'],
                                                    ),
                                                  ));
                                                } else if (type == 'cards') {
                                                  Navigator.of(context).push(MaterialPageRoute(
                                                    builder: (context) => ViewPregameCards(
                                                      title: e['title'],
                                                      listCards: e['cards'],
                                                    ),
                                                  ));
                                                }
                                              },
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                        ],
                                      );
                                    }).toList(),
                                  ):Container(),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<String> list2020UniformEquipmentApparel = [
    'assets/20_21_uniforms_equipment_apparel/01.jpg',
    'assets/20_21_uniforms_equipment_apparel/02.jpg',
    'assets/20_21_uniforms_equipment_apparel/03.jpg',
    'assets/20_21_uniforms_equipment_apparel/04.jpg',
    'assets/20_21_uniforms_equipment_apparel/05.jpg',
    'assets/20_21_uniforms_equipment_apparel/06.jpg',
    'assets/20_21_uniforms_equipment_apparel/07.jpg',
    'assets/20_21_uniforms_equipment_apparel/08.jpg',
    'assets/20_21_uniforms_equipment_apparel/09.jpg',
    'assets/20_21_uniforms_equipment_apparel/10.jpg',
    'assets/20_21_uniforms_equipment_apparel/11.jpg',
    'assets/20_21_uniforms_equipment_apparel/12.jpg',
    'assets/20_21_uniforms_equipment_apparel/13.jpg',
    'assets/20_21_uniforms_equipment_apparel/14.jpg',
    'assets/20_21_uniforms_equipment_apparel/15.jpg',
    'assets/20_21_uniforms_equipment_apparel/16.jpg',
    'assets/20_21_uniforms_equipment_apparel/17.jpg',
    'assets/20_21_uniforms_equipment_apparel/18.jpg',
  ];

  void navigateToUniformsEquipmentApparel() async {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => ViewUniformsEquipmentApparel(
        title: '2020-2021 Uniforms, Equipment & Apparel',
        listSlides: list2020UniformEquipmentApparel,
      ),
    ));
  }

  _getCurrentUser() async {
    DatabaseReference ref = FirebaseDatabase.instance.reference();
    String uid = await Auth().currentUser();
    var users = await ref.child(prefix0.users).child(uid).once();
    currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));
  }
}

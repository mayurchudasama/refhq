import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:ref_hq/data_models/user_model.dart';
import 'package:ref_hq/pregame/notes/add_pregame_notes_screen.dart';
import 'package:ref_hq/pregame/notes/view_pregame_notes_screen.dart';
import 'package:ref_hq/utils/my_colors.dart';
import 'package:ref_hq/widget/status_bar_height.dart';

import '../../auth.dart';

class PregameNotesFolderScreen extends StatefulWidget {
  @override
  _PregameNotesFolderScreenState createState() => _PregameNotesFolderScreenState();
}

class _PregameNotesFolderScreenState extends State<PregameNotesFolderScreen> {

  UserModel currentUser = UserModel();
  List<Map<dynamic, dynamic>> listNotes = List();
  bool isGettingNotes = false;
  String uid;

  @override
  void initState() {
    FirebaseAnalytics().setAnalyticsCollectionEnabled(true);
    super.initState();

    initPrefs();

    getNotes();
  }

  void getNotes() async {
    setState(() {
      isGettingNotes = true;
    });
    listNotes.clear();
    DatabaseReference ref = FirebaseDatabase.instance.reference();
    String uid = await Auth().currentUser();
    DataSnapshot snapshotUser = await ref.child(prefix0.users).child(uid).once();
    currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(snapshotUser.value));
    // DataSnapshot snapshot = await FirebaseDatabase.instance.reference().child(prefix0.pregame_folder).once();
    Map<dynamic, dynamic> mapUsers = snapshotUser.value;
    List<dynamic> pregame_notes = mapUsers[prefix0.pregame_folder];
    // pregame_notes = pregame_notes.reversed.toList();
    if(pregame_notes != null){
      pregame_notes.forEach((element) {
        Map<String,dynamic> mapPregameNotes = Map();
        mapPregameNotes[prefix0.description] = element[prefix0.description];
        mapPregameNotes[prefix0.title] = element[prefix0.title];
        mapPregameNotes[prefix0.timestamp] = element[prefix0.timestamp];
        listNotes.add(mapPregameNotes);
      });
    }

    setState(() {
      isGettingNotes = false;
    });
  }

  void initPrefs() async {
    uid = await Auth().currentUser();
  }

  @override
  Widget build(BuildContext context) {
    double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
    double heightProgress = 4.0;
    double heightAddIcon = devicePixelRatio * 20;
    _getCurrentUser();
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: SafeArea(
        top: false,
        bottom: false,
        child: LayoutBuilder(
          builder: (context, constraints) {
            return Container(
              height: constraints.maxHeight,
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage('assets/images/background.jpg'), fit: BoxFit.fill),
              ),
              child: Column(
                children: <Widget>[
                  StatusBarHeight(),
                  Row(
                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: SizedBox(
                              width: 55,
                              height: 55,
                              child: Icon(Icons.arrow_back_ios))),
                      Expanded(
                        child: Text("Pregame Notes Folder",textAlign: TextAlign.center,style: TextStyle(
                            fontSize: 20
                        ),),
                      ),SizedBox(
                        width: 55,
                        height: 55,)
                    ],
                  ),
                  isGettingNotes
                      ? Container(height: heightProgress, child: LinearProgressIndicator())
                      : Container(
                          height: heightProgress,
                        ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                     /* Padding(
                        padding: const EdgeInsets.only(right: 20,top: 15,bottom: 15),
                        child: SizedBox(width: 30, height: 30, child: Image.asset("assets/icon/icon_add.png")),
                      )*/
                      Padding(
                        padding: const EdgeInsets.only(right: 10,top: 15,bottom: 5),
                        child: IconButton(
                          color: Colors.transparent,
                          icon:Image.asset("assets/icon/icon_add.png") ,
                          onPressed: () {
                            showNoteTitleDialog();
                          },
                        ),
                      )
                    ],
                  ),
                  Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      child: listNotes.length > 0
                          ? ReorderableListView(
                              onReorder: _onReorder,
                              scrollDirection: Axis.vertical,
                              children: List.generate(
                                listNotes.length,
                                (index) {
                                  return Dismissible(
                                    key: ValueKey("${listNotes[index][prefix0.timestamp]}"),
                                    direction: DismissDirection.endToStart,
                                    background: Container(
                                        height: 55,
                                        margin: EdgeInsets.only(top: 7, bottom: 7),
                                        color: Colors.red),
                                    onDismissed: (direction) async{
                                      int timestamp = listNotes[index][prefix0.timestamp];
                                      String title = listNotes[index][prefix0.title];
                                      listNotes.removeAt(index);
                                      DatabaseReference ref = FirebaseDatabase.instance.reference();
                                      String uid = await Auth().currentUser();
                                      await ref
                                          .child(prefix0.users)
                                          .child(uid)
                                          .update({prefix0.pregame_folder: listNotes});
                                      Scaffold.of(context).showSnackBar(SnackBar(content: Text("Deleted Note (${DateFormat("MM/dd").format(DateTime.fromMillisecondsSinceEpoch(timestamp))} - ${title} )")));
                                      getNotes();
                                    },
                                    child: Container(
                                      height: 55,
                                      margin: EdgeInsets.only(top: 7, bottom: 7),
                                      child: ConstrainedBox(
                                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                        child: RaisedButton(
                                          color: MyColors.pregameNotesListButtonColor,
                                          child: Container(
                                              alignment: Alignment.centerLeft,
                                              width: double.infinity,
                                              height: 55,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Text(
                                                    // "${DateFormat("MM/dd").format(DateTime.fromMillisecondsSinceEpoch(listNotes[index][prefix0.timestamp]))} - ${listNotes[index][prefix0.title]}",
                                                    "${listNotes[index][prefix0.title]}",
                                                    style: TextStyle(fontSize: 16),
                                                  ),
                                                  Icon(Icons.keyboard_arrow_right)
                                                ],
                                              )),
                                          onPressed: () async {
                                            bool reload = await Navigator.of(context).push(MaterialPageRoute(
                                              builder: (context) => ViewPregameNotesScreen(
                                                listNotes: listNotes,
                                                index: index,
                                                title: listNotes[index][prefix0.title],
                                                description: listNotes[index][prefix0.description],
                                                time: DateTime.fromMillisecondsSinceEpoch(listNotes[index][prefix0.timestamp]),
                                              ),
                                            ));
                                            if (reload) {
                                              setState(() {
                                                getNotes();
                                              });
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            )
                          : Container() /*FutureBuilder(
                        future: FirebaseDatabase.instance.reference().child(prefix0.pregame_folder).once(),
                        builder: (context, snapshot) {
                          isGettingNotes = true;
                          if (snapshot.hasData) {
                            isGettingNotes = false;
                            Map<dynamic,dynamic> valuesNotes = snapshot.data.value;
                            if(valuesNotes == null){
                              return Container();
                            }
                            debugPrint("All notes : ${valuesNotes.toString()}");
                            listNotes.clear();
                            valuesNotes.forEach((key, value) {
                              if (value[prefix0.userId] == uid) {
                                debugPrint("values : ${value.toString()}");
                                listNotes.add(value);
                              }
                            });
                            //Done swipe to delete
                            return ReorderableListView(
                              onReorder: _onReorder,
                              scrollDirection: Axis.vertical,
                              padding: const EdgeInsets.symmetric(vertical: 8.0),
                              children: List.generate(
                                listNotes.length,
                                    (index) {
                                  return Dismissible(
                                    key: ValueKey("${listNotes[index][prefix0.timestamp]}"),
                                    background: Container(color: Colors.red),
                                    onDismissed: (direction){
                                      FirebaseDatabase.instance.reference().child(prefix0.pregame_folder).child('${listNotes[index][prefix0.timestamp]}').remove().then((_) {
                                        print("Delete ${listNotes[index][prefix0.timestamp]} successfull");
                                        Scaffold.of(context)
                                            .showSnackBar(SnackBar(content: Text("Deleted Note (${DateFormat("MM/dd").format(DateTime.fromMillisecondsSinceEpoch(listNotes[index][prefix0.timestamp]))} - ${listNotes[index][prefix0.title]} )")));
                                      });
                                      // Then show a snackbar.

                                    },
                                    child: Container(
                                      height: 55,
                                      margin: EdgeInsets.only(top: 7,bottom: 7),
                                      child: ConstrainedBox(
                                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                        child: RaisedButton(

                                          color: MyColors.pregameNotesListButtonColor,
                                          child: Container(
                                              alignment: Alignment.centerLeft,
                                              width: double.infinity,
                                              height: 55,
                                              child: Text("${DateFormat("MM/dd").format(DateTime.fromMillisecondsSinceEpoch(listNotes[index][prefix0.timestamp]))} - ${listNotes[index][prefix0.title]}",
                                                style: TextStyle(fontSize: 16),)
                                          ),
                                          onPressed: () async{

                                            bool reload = await Navigator.of(context).push(MaterialPageRoute(
                                              builder: (context) => ViewPregameNotesScreen(
                                                childId: listNotes[index][prefix0.timestamp],
                                                title: listNotes[index][prefix0.title],
                                                description: listNotes[index][prefix0.description],
                                                time: DateTime.fromMillisecondsSinceEpoch(listNotes[index][prefix0.timestamp]),
                                              ),
                                            ));
                                            if(reload){
                                              setState(() {

                                              });
                                            }

                                          },
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            );
                            return ListView.separated(
                                shrinkWrap: true,
                                itemBuilder: (context, index) {
                                  final mNote = listNotes[index];
                                  return Dismissible(
                                    key: Key("${mNote[prefix0.timestamp]}"),
                                    background: Container(color: Colors.red),
                                    onDismissed: (direction){
                                      FirebaseDatabase.instance.reference().child(prefix0.pregame_folder).child('${mNote[prefix0.timestamp]}').remove().then((_) {
                                        print("Delete ${mNote[prefix0.timestamp]} successfull");
                                        Scaffold.of(context)
                                            .showSnackBar(SnackBar(content: Text("Deleted Note (${DateFormat("MM/dd").format(DateTime.fromMillisecondsSinceEpoch(mNote[prefix0.timestamp]))} - ${mNote[prefix0.title]} )")));
                                      });
                                      // Then show a snackbar.

                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(top: 7,bottom: 7),
                                      child: ConstrainedBox(
                                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                        child: RaisedButton(

                                          color: MyColors.pregameNotesListButtonColor,
                                          child: Container(
                                              alignment: Alignment.centerLeft,
                                              width: double.infinity,
                                              height: 55,
                                              child: Text("${DateFormat("MM/dd").format(DateTime.fromMillisecondsSinceEpoch(mNote[prefix0.timestamp]))} - ${mNote[prefix0.title]}",
                                                style: TextStyle(fontSize: 16),)
                                          ),
                                          onPressed: () async{

                                            bool reload = await Navigator.of(context).push(MaterialPageRoute(
                                              builder: (context) => ViewPregameNotesScreen(
                                                childId: mNote[prefix0.timestamp],
                                                title: mNote[prefix0.title],
                                                description: mNote[prefix0.description],
                                                time: DateTime.fromMillisecondsSinceEpoch(mNote[prefix0.timestamp]),
                                              ),
                                            ));
                                            if(reload){
                                              setState(() {

                                              });
                                            }

                                          },
                                        ),
                                      ),
                                    ),
                                  )
                                  ;
                                },
                                separatorBuilder: (context, index) {
                                  return SizedBox(
                                    height: 0,
                                  );
                                },
                                itemCount: listNotes.length);
                          }
                          else if(snapshot.hasError){
                            return Text(snapshot.error.toString());
                          }
                          else{
                            return Container();
                          }
                        },
                      )*/
                      ,
                    ),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  void _onReorder(int oldIndex, int newIndex) async {

        if (newIndex > oldIndex) {
          newIndex -= 1;
        }
        final item = listNotes.removeAt(oldIndex);
        listNotes.insert(newIndex, item);
        setState(() {

        });
        DatabaseReference ref = FirebaseDatabase.instance.reference();
        String uid = await Auth().currentUser();
        await ref
            .child(prefix0.users)
            .child(uid)
            .update({prefix0.pregame_folder: listNotes});

        getNotes();

  }

  GlobalKey<FormState> _formKey = GlobalKey();
  TextEditingController _controllerNoteTitle = TextEditingController();

  Future<void> showNoteTitleDialog() async {
    _controllerNoteTitle = TextEditingController();
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('New Note Title'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Form(
                  key: _formKey,
                  child: TextFormField(
                    controller: _controllerNoteTitle,
                    validator: (text) {
                      if (text.length == 0) {
                        return 'title is required';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                      border: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
                _controllerNoteTitle.text = "";
              },
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  Navigator.of(context).pop();

                  bool reload = await Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => AddPregameNotesScreen(
                        listNotes: listNotes,
                        time: DateTime.now(),
                        title: _controllerNoteTitle.text,
                      ),
                    ),
                  );
                  if (reload) {
                    setState(() {
                      getNotes();
                    });
                  }
                }
              },
            ),
          ],
        );
      },
    );
  }

  _getCurrentUser() async {
    DatabaseReference ref = FirebaseDatabase.instance.reference();
    String uid = await Auth().currentUser();
    var users = await ref.child(prefix0.users).child(uid).once();
    currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));
  }
}

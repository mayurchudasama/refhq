import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:keyboard_actions/keyboard_actions_config.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import 'package:ref_hq/constants.dart';
import 'package:ref_hq/data_models/user_model.dart';
import 'package:ref_hq/utils/my_toast.dart';
import 'package:ref_hq/widget/status_bar_height.dart';

import '../../auth.dart';

class AddPregameNotesScreen extends StatefulWidget {
  String title;
  DateTime time;
  List<Map<dynamic, dynamic>> listNotes = List();
  AddPregameNotesScreen({this.listNotes,this.time,this.title});
  @override
  _AddPregameNotesScreenState createState() => _AddPregameNotesScreenState();
}

class _AddPregameNotesScreenState extends State<AddPregameNotesScreen> {
  final FocusNode _nodeText1 = FocusNode();
  UserModel currentUser = UserModel();
  @override
  void initState() {
    FirebaseAnalytics().setAnalyticsCollectionEnabled(true);
    super.initState();

    initPrefs();
  }

  void initPrefs() async {

  }
  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.black,

      // nextFocus: true,
      actions: [
        KeyboardActionsItem(
          focusNode: _nodeText1,
        )

      ],
    );
  }
  TextEditingController _controllerNote = TextEditingController();

  bool isChanges = false;
  @override
  Widget build(BuildContext context) {
    _getCurrentUser();
    return WillPopScope(
      onWillPop: () async{
        Navigator.of(context).pop(false);
        return true;
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Builder(
          builder: (context) {
            return SafeArea(
              top: false,
              bottom: false,
              child: Container(
                constraints: BoxConstraints(
                    minHeight: MediaQuery.of(context).size.height,
                    minWidth: MediaQuery.of(context).size.width
                ),
                decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage('assets/images/background.jpg'), fit: BoxFit.fill),
                ),
                child: KeyboardActions(
                  config: _buildConfig(context),
                  child: Column(
                    children: <Widget>[
                      StatusBarHeight(),
                      Row(
                        children: [
                          GestureDetector(
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                              child: SizedBox(
                                  width: 55,
                                  height: 55,
                                  child: Icon(Icons.arrow_back_ios))),
                          Expanded(
                            child: Text("${DateFormat("MM/dd").format(widget.time)} - ${widget.title}",textAlign: TextAlign.center,style: TextStyle(
                                fontSize: 20
                            ),),
                          ),SizedBox(
                            width: 55,
                            height: 55,)
                        ],
                      ),
                      (_inProcess)?Container(
                          height: 4,
                          child: LinearProgressIndicator()):Container(height: 4,),
                      Container(
                        constraints: BoxConstraints(
                            minHeight: Platform.isIOS ? MediaQuery.of(context).size.height-(MediaQuery.of(context).size.height/3.5) : MediaQuery.of(context).size.height-(MediaQuery.of(context).size.height/5),
                            minWidth: MediaQuery.of(context).size.width
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextField(
                            focusNode: _nodeText1,
                            onChanged: (text){
                              isChanges = true;
                            },
                            // expands: true,
                            maxLines: null,
                            keyboardType: TextInputType.multiline,
                            controller: _controllerNote,
                            decoration: InputDecoration(
                                hintText: "Enter your note",
                                border: InputBorder.none
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 48,right: 48,bottom: 24,top: 12),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50),
                                side: BorderSide(width: 0.70,color: Colors.white)
                            ),
                            color: Colors.transparent,
                            splashColor: Colors.white24,
                            onPressed: () async{
                              if(isChanges && _controllerNote.text.length >0){
//                        MyToast.showToast("Note Saved");
//                        Navigator.of(context).pop();

                                submit(context, widget.title, widget.time, _controllerNote.text);

                              }else{
                                MyToast.showToast("Empty note");
                              }
                            },
                            child: Container(
                              alignment: Alignment.center,
                              height: 50,
                              width: double.infinity,
                              child: Text("Save",style: TextStyle(
                                  fontSize: 18
                              ),),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
  bool _inProcess = false;
  void submit(BuildContext context, String mNoteTitle, DateTime mNoteDateTime,
      String mNoteDescription) async {
    String uid = await Auth().currentUser();
    String snackbarErrorMessage;
    setState(() {
      _inProcess = true;
    });
    try {
      /*DatabaseReference ref = FirebaseDatabase.instance.reference();
      String noteId = "${mNoteDateTime.toUtc().millisecondsSinceEpoch}";
      await ref.child(prefix0.pregame_folder).child(noteId).set({
        prefix0.userId:uid,
        prefix0.title: mNoteTitle,
        prefix0.timestamp: mNoteDateTime.toUtc().millisecondsSinceEpoch,
        prefix0.description: mNoteDescription,
      });*/


      Map<String,dynamic> mapNote = Map();
      mapNote[prefix0.title] =  mNoteTitle;
      mapNote[prefix0.timestamp] =  mNoteDateTime.toUtc().millisecondsSinceEpoch;
      mapNote[prefix0.description] =  mNoteDescription;

      // widget.listNotes.add(mapNote);
      List<Map<dynamic, dynamic>> listNewNotes = List();
      listNewNotes.add(mapNote);
      listNewNotes.addAll(widget.listNotes);

      
      DatabaseReference ref = FirebaseDatabase.instance.reference();
      String uid = await Auth().currentUser();
      await ref
          .child(users)
          .child(uid)
          .update({prefix0.pregame_folder: listNewNotes});

      Scaffold.of(context).showSnackBar(SnackBar(content: Text("Note Added")));

      Future.delayed(
        const Duration(milliseconds: 1000),
            () {
          setState(() {
            _inProcess = false;
          });
          Navigator.of(context).pop(true);
        },
      );
    } catch (e) {
      print(e);
      snackbarErrorMessage = e.toString();

      displaySnackbar(snackbarErrorMessage,context);
      setState(() {
        _inProcess = false;
      });
    }
  }
  void displaySnackbar(String authError,BuildContext context) {
    final snackBar = SnackBar(
      content: Text(
        authError,
        style: TextStyle(color: Colors.white),
      ),
      backgroundColor: Theme.of(context).primaryColor,
    );

    Scaffold.of(context).showSnackBar(snackBar);
  }
  _getCurrentUser() async {
    DatabaseReference ref = FirebaseDatabase.instance.reference();
    String uid = await Auth().currentUser();
    var users = await ref.child(prefix0.users).child(uid).once();
    currentUser = UserModel.fromSnapshot(uid, Map<String, dynamic>.from(users.value));

  }


}

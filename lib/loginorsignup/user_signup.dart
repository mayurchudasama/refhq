import 'dart:convert';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:keyboard_actions/keyboard_actions_config.dart';
import 'package:keyboard_actions/keyboard_actions_item.dart';
import 'package:ref_hq/home/home_screen.dart';
import 'package:ref_hq/misc_widgets.dart';
import 'package:ref_hq/state_container/state_container.dart';
import 'package:ref_hq/utils/app_preferences.dart';
import 'package:ref_hq/utils/common_constant.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import '../auth.dart';
import '../constants.dart';
import '../validators.dart';

class UserSignUp extends StatefulWidget {
  static final _formKey = GlobalKey<FormState>();


  @override
  _UserSignUpState createState() => _UserSignUpState();
}

class _UserSignUpState extends State<UserSignUp> {
  bool _inProcess = false;
  final FocusNode _nodeText1 = FocusNode();
  final FocusNode _nodeText2 = FocusNode();
  final FocusNode _nodeText3 = FocusNode();
  final FocusNode _nodeText4 = FocusNode();
  final FocusNode _nodeText5 = FocusNode();


  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.black,

      // nextFocus: true,
      actions: [
        KeyboardActionsItem(
          focusNode: _nodeText1,
        ),
        KeyboardActionsItem(
          focusNode: _nodeText2,
        ),
        KeyboardActionsItem(
          focusNode: _nodeText3,
        ),
        KeyboardActionsItem(
          focusNode: _nodeText4,
        ),
        KeyboardActionsItem(
          focusNode: _nodeText5,
        ),
        /*KeyboardActionsItem(focusNode: _nodeText2, toolbarButtons: [
              (node) {
            return GestureDetector(
              onTap: () => node.unfocus(),
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Icon(Icons.close),
              ),
            );
          }
        ]),*/
        /*KeyboardActionsItem(
          focusNode: _nodeText3,
          onTapAction: () {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    content: Text("Custom Action"),
                    actions: <Widget>[
                      FlatButton(
                        child: Text("OK"),
                        onPressed: () => Navigator.of(context).pop(),
                      )
                    ],
                  );
                });
          },
        ),*/
        /*KeyboardActionsItem(
          focusNode: _nodeText4,
          displayCloseWidget: false,
        ),*/
        /*KeyboardActionsItem(
          focusNode: _nodeText5,
          toolbarButtons: [
            //button 1
                (node) {
              return GestureDetector(
                onTap: () => node.unfocus(),
                child: Container(
                  color: Colors.white,
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "CLOSE",
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              );
            },
            //button 2
                (node) {
              return GestureDetector(
                onTap: () => node.unfocus(),
                child: Container(
                  color: Colors.black,
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "DONE",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              );
            }
          ],
        ),*/
        /*KeyboardActionsItem(
          focusNode: _nodeText6,
          footerBuilder: (_) => PreferredSize(
              child: SizedBox(
                  height: 40,
                  child: Center(
                    child: Text('Custom Footer'),
                  )),
              preferredSize: Size.fromHeight(40)),
        ),*/
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    double heightProgress = 4.0;
    String _firstname, _lastname, _hometown, _email, _password;
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
          elevation: 0,
          color: Colors.transparent,
          child: Container(
            child: bottomLogo(),
          )
      ),
      // resizeToAvoidBottomInset: false,
      extendBody: true,
      extendBodyBehindAppBar: true,
      // type: MaterialType.transparency,
      // resizeToAvoidBottomInset: false,
      body: KeyboardActions(
        config: _buildConfig(context),
        child: Builder(builder: (BuildContext context) {
          return Stack(
            children: <Widget>[
              Container(
                constraints: BoxConstraints(
                  minHeight: MediaQuery.of(context).size.height,
                  minWidth: MediaQuery.of(context).size.width
                ),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                      'assets/images/background.jpg',
                    ),
                    fit: BoxFit.fill,
                  ),
                ),
                // height: MediaQuery.of(context).size.height,
              ),
              /*Image.asset(
                'assets/images/hoop.png',
                height: MediaQuery.of(context).size.height * 0.25,
              ),*/
              _inProcess
                  ? SafeArea(child: Container(height: heightProgress, child: LinearProgressIndicator()))
                  : SafeArea(
                child: Container(
                  height: heightProgress,
                ),
              ),
              Container(
                constraints: BoxConstraints(
                    minHeight: MediaQuery.of(context).size.height,
                    minWidth: MediaQuery.of(context).size.width
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Form(
                      key: UserSignUp._formKey,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 48,right: 48),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[

                            // Email
                            TextFormField(
                              autocorrect: false,
                              focusNode: _nodeText1,
                              validator: EmailValidator.validate,
                              textCapitalization: TextCapitalization.none,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(

                                  border: CommonConstant.signupTextfieldOutlineBorder,
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 4.0, horizontal: 12),
                                  // labelText: 'email',
                                  hintText: 'email',
                                  hintStyle: TextStyle(color: Colors.grey[500]),
                                  fillColor: Colors.white,
                                  filled: true),
                              onSaved: (value) => _email = value,
                            ),
                            CommonConstant.heightWidget,

                            // Password
                            TextFormField(
                              autocorrect: false,
                              focusNode: _nodeText2,
                              validator: PasswordValidator.validate,
                              textCapitalization: TextCapitalization.none,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 4.0, horizontal: 12),
                                  border: CommonConstant.signupTextfieldOutlineBorder,
                                  hintText: 'password',
                                  hintStyle: TextStyle(color: Colors.grey[500]),
                                  fillColor: Colors.white,
                                  filled: true),
                              obscureText: true,
                              onSaved: (value) => _password = value,
                            ),
                            SizedBox(
                              height: 16,
                            ),

                            Text("(Optional for Leaderboard Display)",style: TextStyle(
                              fontStyle: FontStyle.italic
                            ),),
                            CommonConstant.heightWidget,
                            // Firstname
                            TextFormField(
                              autocorrect: false,
                              focusNode: _nodeText3,
//                              validator: FirstNameValidator.validate,
                              textCapitalization: TextCapitalization.words,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 4.0, horizontal: 12),
                                  border: CommonConstant.signupTextfieldOutlineBorder,
                                  hintText: 'first name',
                                  hintStyle: TextStyle(color: Colors.grey[500]),
                                  fillColor: Colors.white,
                                  filled: true),
                              onSaved: (value) => _firstname = value,
                            ),
                            CommonConstant.heightWidget,

                            // Lastname
                            TextFormField(
                              autocorrect: false,
                              focusNode: _nodeText4,
//                              validator: LastNameValidator.validate,
                              textCapitalization: TextCapitalization.words,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 4.0, horizontal: 12),
                                  border: CommonConstant.signupTextfieldOutlineBorder,
                                  hintText: 'last name',
                                  hintStyle: TextStyle(color: Colors.grey[500]),
                                  fillColor: Colors.white,
                                  filled: true),
                              onSaved: (value) => _lastname = value,
                            ),
                            CommonConstant.heightWidget,

                            // Hometown
                            TextFormField(
                              autocorrect: false,
                              focusNode: _nodeText5,
                              textCapitalization: TextCapitalization.words,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 4.0, horizontal: 12),
                                  border: CommonConstant.signupTextfieldOutlineBorder,
                                  hintText: 'hometown',
                                  hintStyle: TextStyle(color: Colors.grey[500]),
                                  fillColor: Colors.white,
                                  filled: true),
                              onSaved: (value) => _hometown = value,
                            ),
                            SizedBox(height: 50),

                            // Submit
                            ConstrainedBox(
                              constraints: BoxConstraints(
                                  minWidth: MediaQuery.of(context).size.width),
                              child: RaisedButton(
                                shape: CommonConstant.roundedRectangleBorder,
                                color: Theme.of(context).accentColor,
                                padding: EdgeInsets.all(16),
                                child: (_inProcess)
                                    ? CircularProgressIndicator()
                                    : Text('Submit'),
                                onPressed: (_inProcess)
                                    ? null
                                    : () {

                                        // Validate returns true if the form is valid, otherwise false.
                                        if (UserSignUp._formKey.currentState
                                            .validate()) {
                                          UserSignUp._formKey.currentState
                                              .save(); //Saves the form
                                          SystemChannels.textInput
                                              .invokeMethod('TextInput.hide');
                                          submit(
                                              context,
                                              _email.trim(),
                                              _password,
                                              _firstname.trim(),
                                              _lastname.trim(),
                                              _hometown.trim());
                                        }
                                      },
                              ),
                            ),
                            CommonConstant.heightWidget,

                            // Already have an account
                            ConstrainedBox(
                              constraints: BoxConstraints(
                                  minWidth: MediaQuery.of(context).size.width),
                              child: RaisedButton(
                                shape: CommonConstant.roundedRectangleBorder,
                                padding: EdgeInsets.all(16),
                                child: Text('Already have an account? Sign In'),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            SizedBox(height: 50,)
                          ],
                        ),
                      ),
                    ),

                  ],
                ),
              ),
            ],
          );
        }),
      ),
    );
  }

  void submit(BuildContext context, String email, String password,
      String firstname, String lastname, String hometown) async {
    String snackbarErrorMessage;
    setState(() {
      _inProcess = true;
    });
    try {

      final sports = StateContainer.of(context).selSport;//json.decode(prefs.getString(AppPreferences.pref_sports));
      final ruleset = StateContainer.of(context).selRuleset;//json.decode(prefs.getString(AppPreferences.pref_rule_set));
      final mSubscriptionType = StateContainer.of(context).subscriptionType;
      String uid = await Auth().createUserWithEmailAndPassword(email, password);
      debugPrint("user id : $uid");
      DatabaseReference ref = FirebaseDatabase.instance.reference();
      await ref.child(users).child(uid).set({
        firstName: firstname,
        lastName: lastname,
        homeTown: hometown,
        currentStreak: "0",
        streak: "0",
        // totalQuestionAttended:0,
        prefix0.sports: sports,
        prefix0.ruleset: ruleset,
        prefix0.subscriptionType:mSubscriptionType
      });
      displaySnackbar("New user created. Logging in", context);
      Future.delayed(Duration(milliseconds: 1200), () {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => HomeScreen()),
            (Route<dynamic> route) => false);
      });
    } catch (e) {
      print(e);

      switch (e.code) {
        case 'ERROR_EMAIL_ALREADY_IN_USE':
          snackbarErrorMessage = 'User already exists';
          break;
        default:
          snackbarErrorMessage = e.message;
      }
      displaySnackbar(snackbarErrorMessage, context);
      setState(() {
        _inProcess = false;
      });
    }
  }

  void displaySnackbar(String authError, BuildContext context) {
    final snackBar = SnackBar(
      content: Text(
        authError,
        style: TextStyle(color: Colors.white),
      ),
      backgroundColor: Theme.of(context).primaryColor,
    );

    Scaffold.of(context).showSnackBar(snackBar);
  }
}

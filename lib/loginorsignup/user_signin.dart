import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:keyboard_actions/keyboard_actions_config.dart';
import 'package:ref_hq/home/home_screen.dart';
import 'package:ref_hq/state_container/state_container.dart';
import 'package:ref_hq/utils/app_preferences.dart';
import 'package:ref_hq/utils/common_constant.dart';
import 'package:ref_hq/constants.dart' as prefix0;
import '../auth.dart';
import '../misc_widgets.dart';
import '../home/settings.dart';
import 'user_signup.dart';
import '../validators.dart';

class UserSignIn extends StatefulWidget {
  bool hasSubscription;
  UserSignIn({this.onSignedIn,this.hasSubscription});

  final VoidCallback onSignedIn;

  @override
  _UserSignInState createState() => _UserSignInState();
}

class _UserSignInState extends State<UserSignIn> {
  final _formKey = GlobalKey<FormState>();
  final FocusNode _nodeText1 = FocusNode();
  final FocusNode _nodeText2 = FocusNode();
  @override
  void initState() {
    FirebaseAnalytics().setAnalyticsCollectionEnabled(true);
    super.initState();
  }

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.black,

      // nextFocus: true,
      actions: [
        KeyboardActionsItem(
          focusNode: _nodeText1,
        ),
        KeyboardActionsItem(
          focusNode: _nodeText2,
        ),

      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    double heightProgress = 4.0;
    String _email, _password;
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        color: Colors.transparent,
        child: Container(
          child: bottomLogo(),
        )
      ),
      resizeToAvoidBottomInset: false,
      extendBodyBehindAppBar: true,
      extendBody: true,
      // resizeToAvoidBottomInset: false,
      // type: MaterialType.transparency,
      body: KeyboardActions(
        config: _buildConfig(context),
        child: Builder(builder: (BuildContext context) {
          return Stack(
            children: <Widget>[
              Container(
                constraints: BoxConstraints(
                    minHeight: MediaQuery.of(context).size.height,
                    minWidth: MediaQuery.of(context).size.width
                ),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                      'assets/images/background.jpg',
                    ),
                    fit: BoxFit.fill,
                  ),
                ),
                height: MediaQuery
                    .of(context)
                    .size
                    .height,
                child: Column(
                  children: [

                    isUserLoggingIn
                        ? SafeArea(child: Container(height: heightProgress, child: LinearProgressIndicator()))
                        : SafeArea(
                      child: Container(
                        height: heightProgress,
                      ),
                    ),
                    Expanded(
                      child: Form(
                        key: _formKey,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 48,right: 48),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[

                              // Flexible(child: FractionallySizedBox(heightFactor: 0.25)),
                              TextFormField(
                                 autocorrect: false,
                                // enableSuggestions: false,
                                focusNode: _nodeText1,
                                validator: EmailValidator.validate,
                                textCapitalization: TextCapitalization.none,
                                keyboardType: TextInputType.visiblePassword,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    contentPadding: CommonConstant.textFieldContentPadding,
                                    border: CommonConstant.signupTextfieldOutlineBorder,
                                    hintText: 'email',
                                    hintStyle: TextStyle(color: Colors.grey[500]),
                                    fillColor: Colors.white,
                                    filled: true),
                                onSaved: (value) => _email = value,
                              ),
                              CommonConstant.heightWidget,
                              TextFormField(
                                autocorrect: false,
                                focusNode: _nodeText2,
                                validator: PasswordValidator.validate,
                                textCapitalization: TextCapitalization.none,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    border: CommonConstant.signupTextfieldOutlineBorder,
                                    hintText: 'password',
                                    contentPadding: CommonConstant.textFieldContentPadding,
                                    hintStyle: TextStyle(color: Colors.grey[500]),
                                    fillColor: Colors.white,
                                    filled: true),
                                obscureText: true,
                                onSaved: (value) => _password = value,
                              ),
                              SizedBox(
                                height: 32,
                              ),
                              ConstrainedBox(
                                constraints: BoxConstraints(
                                    minWidth: MediaQuery
                                        .of(context)
                                        .size
                                        .width),
                                child: RaisedButton(
                                    shape: CommonConstant.roundedRectangleBorder,
                                    color: Theme
                                        .of(context)
                                        .accentColor,
                                    padding: EdgeInsets.all(16),
                                    child: Text('Log In'),
                                    onPressed: () {
                                      // Validate returns true if the form is valid, otherwise false.
                                      if (_formKey.currentState.validate()) {
                                        _formKey.currentState.save(); //Saves the form
                                        SystemChannels.textInput
                                            .invokeMethod('TextInput.hide');
                                        submit(context, _email, _password);
                                      }
                                    }),
                              ),
                              ConstrainedBox(
                                constraints: BoxConstraints(
                                    minWidth: MediaQuery
                                        .of(context)
                                        .size
                                        .width),
                                child: FlatButton(
                                  padding: EdgeInsets.all(16),
                                  child: Text('Reset Password'),
                                  onPressed: () {
                                    SystemChannels.textInput
                                        .invokeMethod('TextInput.hide');
                                    _formKey.currentState.save();
                                    if (EmailValidator.validate(_email) == null) {
                                      resetPassword(email: _email, context: context);
                                    } else {
                                      displaySnackbar('Email Required', context);
                                    }
                                  },
                                ),
                              ),
                              ConstrainedBox(
                                constraints: BoxConstraints(
                                    minWidth: MediaQuery
                                        .of(context)
                                        .size
                                        .width),
                                child: RaisedButton(
                                  shape: CommonConstant.roundedRectangleBorder,
                                  padding: EdgeInsets.all(16),
                                  child: Text('Sign Up'),
                                  onPressed: () {
                                    Navigator.of(context).push(MaterialPageRoute(
                                        builder: (context) => UserSignUp()));
                                  },
                                ),
                              ),
                              SizedBox(
                                height: 150,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    /*KeyboardVisibilityProvider.isKeyboardVisible(context)?Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        ConstrainedBox(
                          constraints: BoxConstraints(
                            minHeight: 20,
                            minWidth: 25,
                          ),
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50),
                                side: BorderSide(width: 0.70,color: Colors.white)
                            ),
                            color: Colors.transparent,
                            splashColor: Colors.white24,
                            onPressed: () async{
                              FocusScope.of(context).unfocus();
                            },
                            child: Container(
                              alignment: Alignment.center,
                              height: 20,
                              width: 50,
                              child: Text("Close",style: TextStyle(
                                  fontSize: 14
                              ),),
                            ),
                          ),
                        ),
                      ],
                    ):Container(),*/
                  ],
                ),
              ),

              /*Positioned(
                bottom: 0,
                right: 0,
                left: 0,
                child: bottomLogo(),
              )*/
            ],
          );
        }),
      ),
    );
  }
  void resetPassword({String email, @required BuildContext context}) async {
    try {
      await Auth().resetPassword(email: email);
      displaySnackBar('Password reset email sent.', context);
    } catch (e) {
      print(e);
      var connectivityResult = await (Connectivity().checkConnectivity());
      String snackbarErrorMessage;
      if (connectivityResult == ConnectivityResult.none) {
        snackbarErrorMessage = 'No Internet. Try again';
      } else if (e == NoSuchMethodError) {
        snackbarErrorMessage = 'Unknown Error';
      } else {
        switch (e.code) {
          case 'ERROR_INVALID_EMAIL':
            snackbarErrorMessage = 'Invalid email';
            break;
          case 'ERROR_USER_NOT_FOUND':
            snackbarErrorMessage = 'User not found';
        }
      }
      displaySnackBar(snackbarErrorMessage, context);
    }
  }
  bool isUserLoggingIn = false;
  void submit(BuildContext context, String email, String password) async {

      String authError;
      setState(() {

        isUserLoggingIn = true;
      });

      try {
        String userId = await Auth().signInWithEmailAndPassword(email, password);
        if(!widget.hasSubscription){
          debugPrint("user id : $userId");
          DatabaseReference dbRef = FirebaseDatabase.instance.reference();

          final sports = StateContainer.of(context).selSport;//json.decode(prefs.getString(AppPreferences.pref_sports));
          final ruleset = StateContainer.of(context).selRuleset;//json.decode(prefs.getString(AppPreferences.pref_rule_set));
          final mSubscriptionType = StateContainer.of(context).subscriptionType;
          await dbRef.child(prefix0.users).child(userId).update({
            prefix0.sports: sports,
            prefix0.ruleset: ruleset,
            prefix0.subscriptionType:mSubscriptionType
          });

          isUserLoggingIn = false;
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomeScreen()));
        }
        else{
          isUserLoggingIn = false;
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomeScreen()));
        }
        setState(() {
        });

      } catch (e) {
        isUserLoggingIn = false;
        print(e);
        print(e.code);
        switch (e.code) {
          case 'ERROR_INVALID_EMAIL':
            authError = 'Invalid Email';
            break;
          case 'ERROR_USER_NOT_FOUND':
            authError = 'User Not Found';
            break;
          case 'ERROR_WRONG_PASSWORD':
            authError = 'Wrong Password';
            break;
          default:
            authError = 'Error';
            break;
        }
        setState(() {
        });
      }
      displaySnackbar(authError, context);
  }

  void displaySnackbar(String authError, BuildContext context) {
    final snackBar = SnackBar(
      content: Text(
        authError??"",
        style: TextStyle(color: Colors.white),
      ),
      backgroundColor: Theme
          .of(context)
          .primaryColor,
    );

    Scaffold.of(context).showSnackBar(snackBar);
  }
}
